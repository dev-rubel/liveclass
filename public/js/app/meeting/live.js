(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/app/meeting/live"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/FileSharer.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/FileSharer.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _js_vars__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @js/vars */ "./resources/js/vars.js");
/* harmony import */ var bootstrap_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! bootstrap-vue */ "./node_modules/bootstrap-vue/esm/index.js");
/* harmony import */ var _core_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @core/utils */ "./resources/js/core/utils/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: "file-sharer",
  components: {
    BProgress: bootstrap_vue__WEBPACK_IMPORTED_MODULE_2__["BProgress"]
  },
  props: {
    connection: {
      "default": null
    },
    actionLabel: {
      type: String,
      "default": 'global.choose'
    },
    nameLabel: {
      type: String,
      "default": 'upload.file'
    },
    namesLabel: {
      type: String,
      "default": 'upload.files'
    },
    closeButtonLabel: {
      type: String,
      "default": 'upload.close'
    },
    buttonWrapperClasses: {
      type: String,
      "default": 'justify-content-between'
    },
    buttonClasses: {
      type: String,
      "default": ''
    },
    closeButtonClasses: {
      type: String,
      "default": 'btn-light'
    },
    buttonIcon: {
      type: String,
      "default": 'fas fa-upload'
    },
    buttonDesign: {
      type: String,
      "default": 'primary'
    },
    options: {
      type: Object,
      "default": function _default() {
        return {
          allowedExtensions: ["jpg", "png", "jpeg", "pdf", "doc", "docx", "xls", "xlsx", "txt"],
          maxNoOfFiles: 20,
          allowedMaxFileSize: null
        };
      }
    },
    multiple: {
      type: Boolean,
      "default": false
    },
    hideButtonLabel: {
      type: Boolean,
      "default": false
    },
    showCloseButton: {
      type: Boolean,
      "default": true
    },
    autoSend: {
      type: Boolean,
      "default": true
    }
  },
  data: function data() {
    return {
      icons: _js_vars__WEBPACK_IMPORTED_MODULE_1__["fileIcons"],
      mimeTypes: _js_vars__WEBPACK_IMPORTED_MODULE_1__["fileMimeTypes"],
      sharingFiles: [],
      lastSelectedFile: null,
      incomingRequest: {
        show: false,
        sender: null,
        fileId: null
      }
    };
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('config', ['configs', 'vars'])),
  watch: {
    connection: function connection(newVal, oldVal) {
      if (newVal && oldVal === null) {
        this.setupConnection();
      }
    }
  },
  methods: {
    getFileIcon: function getFileIcon(type) {
      return this.icons[type] || 'fa-file-alt';
    },
    getFileNameIcon: function getFileNameIcon(type) {
      return this.icons[type] || 'fa-file-alt';
    },
    generateFilesList: function generateFilesList(e) {
      var _this = this;

      var allowedExtensions = null;
      var selectedFiles = this.$refs.filesInput.files;

      if (this.options.allowedExtensions) {
        allowedExtensions = new RegExp('(\.' + this.options.allowedExtensions.join('|\.') + ')$');
      }

      if (this.options.maxNoOfFiles && selectedFiles.length > this.options.maxNoOfFiles) {
        this.$toasted.error(this.$t('upload.max_file_limit_crossed', {
          number: this.options.maxNoOfFiles
        }), this.$toastConfig.error);
        return;
      }

      var _loop = function _loop(i) {
        var fileId = "".concat(selectedFiles[i].size, "-").concat(selectedFiles[i].name);

        var existingFileIndex = _this.sharingFiles.findIndex(function (f) {
          return f.fileId === fileId;
        });

        if (allowedExtensions && !allowedExtensions.test(selectedFiles[i].name)) {
          _this.$toasted.error(_this.$t('global.file_not_supported', {
            attribute: selectedFiles[i].name.split('.').pop()
          }), _this.$toastConfig.error);
        } else if (_this.options.allowedMaxFileSize && selectedFiles[i].size > _this.options.allowedMaxFileSize) {
          _this.$toasted.error(_this.$t('global.file_too_large', {
            attribute: selectedFiles[i].name
          }), _this.$toastConfig.error);
        } else if (existingFileIndex === -1) {
          var fileObj = {
            uuid: '',
            name: selectedFiles[i].name,
            size: selectedFiles[i].size,
            type: selectedFiles[i].type,
            humanSize: Object(_core_utils__WEBPACK_IMPORTED_MODULE_3__["bytesToSize"])(selectedFiles[i].size),
            fileId: fileId,
            url: '',
            sendingTo: [],
            progress: {
              value: 0,
              max: 0,
              percent: 0,
              status: 'starting'
            }
          };

          _this.sharingFiles.unshift(fileObj);

          _this.lastSelectedFile = selectedFiles[i];
        }
      };

      for (var i = 0; i < selectedFiles.length; i++) {
        _loop(i);
      }

      if (this.autoSend && this.connection) {
        this.connection.send({
          doYouWannaReceiveThisFile: true,
          sender: this.connection.extra,
          fileId: "".concat(this.lastSelectedFile.size, "-").concat(this.lastSelectedFile.name)
        });
      }
    },
    rejectIncomingFile: function rejectIncomingFile() {
      this.incomingRequest.show = false;
      this.connection.send({
        noIDoNotWannaReceive: true,
        sender: this.connection.extra,
        fileId: this.incomingRequest.fileId
      });
      this.incomingRequest.sender = null;
      this.incomingRequest.fileId = null;
    },
    acceptIncomingFile: function acceptIncomingFile() {
      this.incomingRequest.show = false;
      this.connection.send({
        yesIWannaReceive: true,
        sender: this.connection.extra,
        fileId: this.incomingRequest.fileId
      });
      this.incomingRequest.sender = null;
      this.incomingRequest.fileId = null;
    },
    rtcmOnOpen: function rtcmOnOpen(e) {
      var _this2 = this;

      try {
        chrome.power.requestKeepAwake('display');
      } catch (e) {}

      if (this.connection.connectedWith[e.userid]) {
        return;
      }

      this.connection.connectedWith[e.userid] = true;

      if (!this.lastSelectedFile) {
        return;
      }

      window.setTimeout(function () {
        if (_this2.autoSend && _this2.connection) {
          _this2.connection.send({
            doYouWannaReceiveThisFile: true,
            sender: _this2.connection.extra,
            fileId: "".concat(_this2.lastSelectedFile.size, "-").concat(_this2.lastSelectedFile.name)
          });
        }
      }, 500);
    },
    rtcmOnClose: function rtcmOnClose(e) {// console.log('rtcmOnClose')
    },
    rtcmOnLeave: function rtcmOnLeave(e) {// console.log('rtcmOnLeave')
    },
    rtcmOnMessage: function rtcmOnMessage(event) {
      if (event.data.doYouWannaReceiveThisFile) {
        if (!this.connection.fileReceived[event.data.fileId]) {
          this.incomingRequest.fileId = event.data.fileId;
          this.incomingRequest.sender = event.data.sender;
          this.incomingRequest.show = true;
          this.$emit('show');
        }
      } else if (event.data.yesIWannaReceive && !!this.lastSelectedFile) {
        this.connection.shareFile(this.lastSelectedFile, event.userid);
      } else if (event.data.noIDoNotWannaReceive && !!this.lastSelectedFile) {//
      }
    },
    rtcmOnFileStart: function rtcmOnFileStart(file) {
      var fileId = "".concat(file.size, "-").concat(file.name);

      if (this.connection.fileReceived[fileId]) {
        return;
      } //receiving new file


      if (file.userid !== this.connection.userid) {
        this.sharingFiles.unshift({
          uuid: '',
          name: file.name,
          size: file.size,
          type: file.type,
          humanSize: Object(_core_utils__WEBPACK_IMPORTED_MODULE_3__["bytesToSize"])(file.size),
          fileId: fileId,
          url: '',
          receivingFrom: null,
          progress: {
            value: 0,
            max: 0,
            percent: 0,
            status: 'starting'
          }
        });
        this.$emit('show');
      }

      var selectedFileIndex = this.sharingFiles.findIndex(function (f) {
        return f.fileId === fileId;
      });

      if (selectedFileIndex === -1) {
        return;
      }

      var selectedFile = this.sharingFiles[selectedFileIndex];
      selectedFile.uuid = file.uuid;
      selectedFile.userid = file.userid;

      if (file.userid === this.connection.userid) {
        if (!selectedFile.sendingTo) {
          selectedFile.sendingTo = [];
        }

        if (selectedFile.sendingTo.length > 0) {
          if (selectedFile.sendingTo.findIndex(function (u) {
            return u.userid === file.remoteUserId;
          }) !== -1) {
            return;
          }
        }

        selectedFile.sendingTo.unshift({
          userid: file.remoteUserId,
          progress: {
            value: 0,
            percent: 0,
            max: file.maxChunks,
            status: 'starting'
          }
        }); // this.sharingFiles[selectedFileIndex] = selectedFile
      } else {
        selectedFile.receiveAgain = true;
        selectedFile.receivingFrom = file.userid;
        selectedFile.progress.max = file.maxChunks;
        selectedFile.progress.status = 'starting'; // this.sharingFiles[selectedFileIndex] = selectedFile
      }
    },
    rtcmOnFileProgress: function rtcmOnFileProgress(chunk) {
      var fileId = "".concat(chunk.size, "-").concat(chunk.name); // console.log(`name-${chunk.name}  to-${chunk.remoteUserId}  maxChunks-${chunk.maxChunks}  position-${chunk.currentPosition}`)

      if (this.connection.fileReceived[fileId]) {
        return;
      }

      var selectedFileIndex = this.sharingFiles.findIndex(function (f) {
        return f.uuid === chunk.uuid;
      });

      if (selectedFileIndex === -1) {
        return;
      }

      var selectedFile = this.sharingFiles[selectedFileIndex];

      if (chunk.remoteUserId && selectedFile.sendingTo) {
        var remoteUserIndex = selectedFile.sendingTo.findIndex(function (u) {
          return u.userid === chunk.remoteUserId;
        });
        var helperInnerObj = selectedFile.sendingTo[remoteUserIndex];

        if (!helperInnerObj) {
          return;
        }

        helperInnerObj.progress.value = chunk.currentPosition;
        helperInnerObj.progress = this.updateProgress(helperInnerObj.progress, true);
      } else {
        selectedFile.progress.value = chunk.currentPosition;
        selectedFile.progress = this.updateProgress(selectedFile.progress, false);
      }
    },
    rtcmOnFileEnd: function rtcmOnFileEnd(file) {
      var fileId = "".concat(file.size, "-").concat(file.name);

      if (this.connection.fileReceived[fileId]) {
        return;
      } //received file ?


      if (file.remoteUserId === this.connection.userid) {
        this.connection.fileReceived[fileId] = file;
      }

      var selectedFileIndex = this.sharingFiles.findIndex(function (f) {
        return f.fileId === fileId;
      });

      if (selectedFileIndex === -1) {
        return;
      }

      var selectedFile = this.sharingFiles[selectedFileIndex];
      selectedFile.url = file.url;

      if (file.remoteUserId && selectedFile.sendingTo) {
        var remoteUserIndex = selectedFile.sendingTo.findIndex(function (u) {
          return u.userid === file.remoteUserId;
        }); // const helperInnerObj = selectedFile.sendingTo[remoteUserIndex]

        selectedFile.sendingTo[remoteUserIndex].progress.status = 'sent'; // selectedFile.sendingTo[remoteUserIndex] = helperInnerObj
        // this.$nextTick(() => {
        //     this.sharingFiles[selectedFileIndex] = selectedFile
        // })
      } else {
        selectedFile.progress.status = 'received';
      }
    },
    rtcmOnError: function rtcmOnError(e) {// console.log('Some error: ', e)
    },
    updateProgress: function updateProgress(progress) {
      var sending = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

      if (!progress.value || progress.value === -1) {
        progress.percent = 0;
        return progress;
      }

      var percent = parseInt((progress.value / progress.max * 100).toFixed(0));

      if (sending) {
        progress.status = percent > 100 ? 'sent' : 'sending - ' + percent + '%';
      } else {
        progress.status = percent > 100 ? 'received' : 'receiving - ' + percent + '%';
      }

      progress.percent = percent > 100 ? 100 : percent;
      return progress;
    },
    setupConnection: function setupConnection() {
      this.connection.enableFileSharing = true;
      this.connection.fileReceived = {};
      this.connection.connectedWith = {};
      this.connection.chunkSize = 15 * 1000;
      this.connection.onopen = this.rtcmOnOpen;
      this.connection.onclose = this.rtcmOnClose;
      this.connection.onleave = this.rtcmOnLeave;
      this.connection.onmessage = this.rtcmOnMessage;
      this.connection.onerror = this.rtcmOnError;
      this.connection.onFileStart = this.rtcmOnFileStart;
      this.connection.onFileProgress = this.rtcmOnFileProgress;
      this.connection.onFileEnd = this.rtcmOnFileEnd;
    }
  },
  mounted: function mounted() {
    if (this.connection) {
      this.setupConnection();
    }
  },
  destroyed: function destroyed() {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/core/components/AnimatedNumber.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/core/components/AnimatedNumber.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "animated-number",
  props: {
    number: {
      type: Number,
      "default": 0
    },
    delay: {
      type: Number,
      "default": 20
    }
  },
  data: function data() {
    return {
      displayNumber: 0,
      interval: false,
      initialInterval: false
    };
  },
  methods: {
    changeNumberFn: function changeNumberFn() {
      if (this.displayNumber != this.number) {
        var change = (this.number - this.displayNumber) / 10;
        change = change >= 0 ? Math.ceil(change) : Math.floor(change);
        this.displayNumber = this.displayNumber + change;
      }
    }
  },
  watch: {
    number: function number() {
      clearInterval(this.interval);

      if (this.number == this.displayNumber) {
        return;
      }

      this.interval = window.setInterval(this.changeNumberFn, this.delay);
    }
  },
  mounted: function mounted() {
    // this.displayNumber = this.number ? this.number : 0
    this.displayNumber = 0;
    clearInterval(this.initialInterval);

    if (this.number == this.displayNumber) {
      return;
    }

    this.initialInterval = window.setInterval(this.changeNumberFn, this.delay);
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/meeting/live.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/app/meeting/live.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _mixins_user_dropdown__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @mixins/user-dropdown */ "./resources/js/mixins/user-dropdown.js");
/* harmony import */ var _mixins_meeting__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @mixins/meeting */ "./resources/js/mixins/meeting.js");
/* harmony import */ var _mixins_live__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @mixins/live */ "./resources/js/mixins/live.js");
/* harmony import */ var _components_AppLogo__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @components/AppLogo */ "./resources/js/components/AppLogo.vue");
/* harmony import */ var _core_components_AnimatedNumber__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @core/components/AnimatedNumber */ "./resources/js/core/components/AnimatedNumber.vue");
/* harmony import */ var vue2_flip_countdown__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vue2-flip-countdown */ "./node_modules/vue2-flip-countdown/dist/vue2-flip-countdown.js");
/* harmony import */ var vue2_flip_countdown__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue2_flip_countdown__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _components_FileSharer__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @components/FileSharer */ "./resources/js/components/FileSharer.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







/* harmony default export */ __webpack_exports__["default"] = ({
  mixins: [_mixins_live__WEBPACK_IMPORTED_MODULE_2__["default"], _mixins_meeting__WEBPACK_IMPORTED_MODULE_1__["default"], _mixins_user_dropdown__WEBPACK_IMPORTED_MODULE_0__["default"]],
  components: {
    AppLogo: _components_AppLogo__WEBPACK_IMPORTED_MODULE_3__["default"],
    AnimatedNumber: _core_components_AnimatedNumber__WEBPACK_IMPORTED_MODULE_4__["default"],
    FlipCountdown: vue2_flip_countdown__WEBPACK_IMPORTED_MODULE_5___default.a,
    FileSharer: _components_FileSharer__WEBPACK_IMPORTED_MODULE_6__["default"]
  },
  watch: {
    locked: function locked(newVal, oldVal) {
      if (newVal) {
        var toPath = this.$route.fullPath;
        this.$router.push({
          name: 'authLock',
          query: {
            ref: toPath
          }
        });
      }
    }
  },
  methods: {}
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/meeting/live.vue?vue&type=style&index=0&lang=scss&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--9-2!./node_modules/sass-loader/dist/cjs.js??ref--9-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/app/meeting/live.vue?vue&type=style&index=0&lang=scss& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".files-sharing-box {\n  position: fixed;\n  top: 0;\n  left: 0;\n  width: 35vw;\n  height: 100vh;\n  z-index: 1;\n}\n.files-sharing-box .files-container {\n  width: 100%;\n  height: 100%;\n  background-color: rgba(255, 255, 255, 0.85);\n  padding-bottom: 60px;\n  opacity: 0.85;\n}\n.files-sharing-box .files-container .files-wrapper {\n  display: flex;\n  flex-direction: column;\n  justify-content: space-between;\n}\n.files-sharing-box:hover .files-container {\n  opacity: 1;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/meeting/live.vue?vue&type=style&index=0&lang=scss&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--9-2!./node_modules/sass-loader/dist/cjs.js??ref--9-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/app/meeting/live.vue?vue&type=style&index=0&lang=scss& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--9-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--9-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./live.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/meeting/live.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/FileSharer.vue?vue&type=template&id=68270e3a&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/FileSharer.vue?vue&type=template&id=68270e3a& ***!
  \*************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "sm-uploader file-uploader file-sharer" }, [
    _c("div", { class: ["files-list-wrapper"] }, [
      _vm.incomingRequest.show
        ? _c("div", { staticClass: "file-incoming-request" }, [
            _c("div", { staticClass: "alert fade show alert-white" }, [
              _c("div", { staticClass: "alert-content" }, [
                _c("h6", [
                  _c("strong", [
                    _vm._v(_vm._s(_vm.incomingRequest.sender.name))
                  ]),
                  _c("span", { staticClass: "text-muted mx-1" }, [
                    _vm._v(_vm._s(_vm.$t("upload.wants_to_share_file")))
                  ]),
                  _c("strong", [_vm._v(_vm._s(_vm.incomingRequest.fileId))])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "d-flex justify-content-start mt-3" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-sm btn-danger flex-grow",
                    attrs: { type: "button" },
                    on: { click: _vm.rejectIncomingFile }
                  },
                  [_vm._v(_vm._s(_vm.$t("upload.reject")))]
                ),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "btn btn-sm btn-success flex-grow",
                    attrs: { type: "button" },
                    on: { click: _vm.acceptIncomingFile }
                  },
                  [_vm._v(_vm._s(_vm.$t("upload.accept")))]
                )
              ])
            ])
          ])
        : _vm._e(),
      _vm._v(" "),
      _vm.sharingFiles.length
        ? _c(
            "ul",
            { staticClass: "files-list" },
            _vm._l(_vm.sharingFiles, function(fileObj, index) {
              return _c(
                "li",
                { key: fileObj.fileId, staticClass: "file-details-row" },
                [
                  _c("div", { staticClass: "file-details-wrapper" }, [
                    _c("div", { staticClass: "file-icon" }, [
                      _c("span", [
                        _c("i", {
                          class: ["far", _vm.getFileIcon(fileObj.type)]
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "file-details" }, [
                      _c("h6", [_vm._v(_vm._s(fileObj.name))]),
                      _vm._v(" "),
                      _c("p", [
                        _c("span", [
                          _c("span", { staticClass: "font-weight-700" }, [
                            _vm._v(_vm._s(_vm.$t("upload.type")))
                          ]),
                          _vm._v(
                            ": " +
                              _vm._s(_vm.mimeTypes[fileObj.type] || "Unknown")
                          )
                        ]),
                        _vm._v(" "),
                        _c("span", { staticClass: "ml-1" }, [
                          _c("span", { staticClass: "font-weight-700" }, [
                            _vm._v(_vm._s(_vm.$t("upload.size")))
                          ]),
                          _vm._v(": " + _vm._s(fileObj.humanSize))
                        ])
                      ]),
                      _vm._v(" "),
                      fileObj.sendingTo
                        ? _c(
                            "div",
                            { staticClass: "sharing-details-wrapper" },
                            _vm._l(fileObj.sendingTo, function(to) {
                              return _c(
                                "div",
                                {
                                  key: to.userid,
                                  staticClass: "sharing-row mt-2"
                                },
                                [
                                  _c(
                                    "div",
                                    { staticClass: "sharing-details small" },
                                    [
                                      _c(
                                        "div",
                                        { staticClass: "flex-grow text-muted" },
                                        [
                                          _vm._v(
                                            "\n                                        Sending to: " +
                                              _vm._s(to.userid) +
                                              "\n                                    "
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c("div", [
                                        _c(
                                          "span",
                                          {
                                            class: [
                                              "badge badge-sm badge-pill",
                                              {
                                                "badge-success":
                                                  to.progress.status === "sent"
                                              },
                                              {
                                                "badge-info":
                                                  to.progress.status !== "sent"
                                              }
                                            ]
                                          },
                                          [_vm._v(_vm._s(to.progress.status))]
                                        )
                                      ])
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("b-progress", {
                                    staticClass: "mt-2 mb-0",
                                    attrs: {
                                      value: to.progress.percent,
                                      max: 100
                                    }
                                  })
                                ],
                                1
                              )
                            }),
                            0
                          )
                        : fileObj.receivingFrom
                        ? _c(
                            "div",
                            { staticClass: "sharing-row mt-2" },
                            [
                              _c(
                                "div",
                                { staticClass: "sharing-details small" },
                                [
                                  _c(
                                    "div",
                                    { staticClass: "flex-grow text-muted" },
                                    [
                                      _vm._v(
                                        "\n                                    Receiving From: " +
                                          _vm._s(fileObj.receivingFrom) +
                                          "\n                                "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("div", [
                                    _c(
                                      "span",
                                      {
                                        class: [
                                          "badge badge-sm badge-pill",
                                          {
                                            "badge-success":
                                              fileObj.progress.status ===
                                              "received"
                                          },
                                          {
                                            "badge-info":
                                              fileObj.progress.status !==
                                              "received"
                                          }
                                        ]
                                      },
                                      [_vm._v(_vm._s(fileObj.progress.status))]
                                    )
                                  ])
                                ]
                              ),
                              _vm._v(" "),
                              _c("b-progress", {
                                staticClass: "mt-2 mb-0",
                                attrs: {
                                  value: fileObj.progress.percent,
                                  max: 100
                                }
                              })
                            ],
                            1
                          )
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "status-wrapper" }, [
                      !fileObj.receivingFrom ||
                      fileObj.progress.status === "received"
                        ? _c(
                            "a",
                            {
                              staticClass: "status-action",
                              attrs: {
                                href: fileObj.url,
                                target: "_blank",
                                download: fileObj.name
                              }
                            },
                            [_vm._m(0, true)]
                          )
                        : _vm._e()
                    ])
                  ])
                ]
              )
            }),
            0
          )
        : _vm._e()
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "file-uploader-form" }, [
      _c("div", { class: ["file-selector d-flex", _vm.buttonWrapperClasses] }, [
        _vm.showCloseButton
          ? _c(
              "button",
              {
                class: ["btn", _vm.closeButtonClasses],
                attrs: { type: "button" },
                on: {
                  click: function($event) {
                    return _vm.$emit("hide")
                  }
                }
              },
              [
                _vm._m(1),
                _vm._v(" "),
                _c("span", { staticClass: "button-label" }, [
                  _vm._v(_vm._s(_vm.$t(_vm.closeButtonLabel)))
                ])
              ]
            )
          : _vm._e(),
        _vm._v(" "),
        _c(
          "label",
          { class: ["btn", "btn-" + _vm.buttonDesign, _vm.buttonClasses] },
          [
            _c("input", {
              ref: "filesInput",
              staticClass: "selector-input",
              attrs: {
                name: "filesInput",
                type: "file",
                id: "filesInput",
                multiple: _vm.multiple
              },
              on: { change: _vm.generateFilesList }
            }),
            _vm._v(" "),
            _c("span", { staticClass: "button-icon" }, [
              _c("i", { class: _vm.buttonIcon })
            ]),
            _vm._v(" "),
            !_vm.hideButtonLabel
              ? _c("span", { staticClass: "button-label" }, [
                  _vm._v(
                    _vm._s(
                      _vm.$t(_vm.actionLabel, {
                        attribute: _vm.$t(_vm.namesLabel)
                      })
                    )
                  )
                ])
              : _vm._e()
          ]
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", [_c("i", { staticClass: "fas fa-download" })])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "button-icon" }, [
      _c("i", { staticClass: "fas fa-times" })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/core/components/AnimatedNumber.vue?vue&type=template&id=96304fda&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/core/components/AnimatedNumber.vue?vue&type=template&id=96304fda& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("span", [_vm._v(_vm._s(_vm.displayNumber))])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/meeting/live.vue?vue&type=template&id=12b2cb6d&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/app/meeting/live.vue?vue&type=template&id=12b2cb6d& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "base-container",
    {
      class: [
        "meeting-page",
        { "auto-hide-footer": _vm.pageConfigs.footerAutoHide && _vm.hasVideos },
        { pulse: _vm.recording }
      ],
      attrs: {
        "with-loader": "",
        "is-loading": _vm.isLoading,
        "loader-color": _vm.vars.loaderColor,
        "overlay-color": _vm.uiConfigs.pageColorSchemeType,
        "loader-size": "screen",
        "data-page-background-color": _vm.uiConfigs.pageBackgroundColor,
        "data-card-background-color": _vm.uiConfigs.pageContainerBackgroundColor
      }
    },
    [
      !_vm.duplicateTab
        ? [
            _c(
              "div",
              {
                class: ["meeting-footer", { "has-videos": _vm.hasVideos }],
                attrs: {
                  "data-footer-background-color": _vm.uiConfigs.leftSidebarColor
                }
              },
              [
                _c(
                  "div",
                  { staticClass: "logo-wrapper" },
                  [_c("app-logo", { attrs: { place: "sidebar", size: "sm" } })],
                  1
                ),
                _vm._v(" "),
                _vm.entity && _vm.entity.status && _vm.videoList.length
                  ? _c(
                      "div",
                      { staticClass: "meeting-actions meeting-actions-center" },
                      [
                        _vm.localVideo && _vm.pageConfigs.showEnableAudioBtn
                          ? [
                              _vm.pageConfigs.enableAudio
                                ? _c(
                                    "button",
                                    {
                                      directives: [
                                        {
                                          name: "b-tooltip",
                                          rawName: "v-b-tooltip.hover.d500",
                                          modifiers: { hover: true, d500: true }
                                        }
                                      ],
                                      staticClass: "btn action",
                                      attrs: {
                                        type: "button",
                                        title: _vm.$t("meeting.mute_audio")
                                      },
                                      on: { click: _vm.toggleAudio }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fas fa-microphone"
                                      })
                                    ]
                                  )
                                : _c(
                                    "button",
                                    {
                                      directives: [
                                        {
                                          name: "b-tooltip",
                                          rawName: "v-b-tooltip.hover.d500",
                                          modifiers: { hover: true, d500: true }
                                        }
                                      ],
                                      staticClass: "btn action disabled-text",
                                      attrs: {
                                        type: "button",
                                        title: _vm.$t("meeting.unmute_audio")
                                      },
                                      on: { click: _vm.toggleAudio }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fas fa-microphone-slash"
                                      })
                                    ]
                                  )
                            ]
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.localVideo && _vm.pageConfigs.showEnableVideoBtn
                          ? [
                              _vm.pageConfigs.enableVideo
                                ? _c(
                                    "button",
                                    {
                                      directives: [
                                        {
                                          name: "b-tooltip",
                                          rawName: "v-b-tooltip.hover.d500",
                                          modifiers: { hover: true, d500: true }
                                        }
                                      ],
                                      staticClass: "btn action",
                                      attrs: {
                                        type: "button",
                                        title: _vm.$t("meeting.mute_video")
                                      },
                                      on: { click: _vm.toggleVideo }
                                    },
                                    [_c("i", { staticClass: "fas fa-video" })]
                                  )
                                : _c(
                                    "button",
                                    {
                                      directives: [
                                        {
                                          name: "b-tooltip",
                                          rawName: "v-b-tooltip.hover.d500",
                                          modifiers: { hover: true, d500: true }
                                        }
                                      ],
                                      staticClass: "btn action disabled-text",
                                      attrs: {
                                        type: "button",
                                        title: _vm.$t("meeting.unmute_video")
                                      },
                                      on: { click: _vm.toggleVideo }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fas fa-video-slash"
                                      })
                                    ]
                                  )
                            ]
                          : _vm._e(),
                        _vm._v(" "),
                        _c(
                          "button",
                          {
                            directives: [
                              {
                                name: "b-tooltip",
                                rawName: "v-b-tooltip.hover.d500",
                                modifiers: { hover: true, d500: true }
                              }
                            ],
                            staticClass: "btn action",
                            attrs: {
                              type: "button",
                              title: _vm.$t("meeting.media_devices.setup")
                            },
                            on: { click: _vm.toggleDevicesModal }
                          },
                          [_c("i", { staticClass: "fas fa-cogs" })]
                        ),
                        _vm._v(" "),
                        _c(
                          "button",
                          {
                            directives: [
                              {
                                name: "b-tooltip",
                                rawName: "v-b-tooltip.hover.d500",
                                modifiers: { hover: true, d500: true }
                              }
                            ],
                            staticClass: "btn action danger-text",
                            attrs: {
                              type: "button",
                              title: _vm.$t("meeting.leave_meeting")
                            },
                            on: { click: _vm.getOffline }
                          },
                          [_c("i", { staticClass: "fas fa-phone-slash" })]
                        ),
                        _vm._v(" "),
                        _vm.pageConfigs.enableRecording
                          ? [
                              _vm.recording
                                ? _c(
                                    "button",
                                    {
                                      directives: [
                                        {
                                          name: "b-tooltip",
                                          rawName: "v-b-tooltip.hover.d500",
                                          modifiers: { hover: true, d500: true }
                                        }
                                      ],
                                      staticClass:
                                        "btn action danger-text d-none d-md-inline-block",
                                      attrs: {
                                        type: "button",
                                        title: _vm.$t("meeting.stop_recording")
                                      },
                                      on: { click: _vm.stopRecording }
                                    },
                                    [
                                      _c("i", { staticClass: "fas fa-stop" }),
                                      _vm._v(" "),
                                      _c(
                                        "span",
                                        { staticClass: "recording_time" },
                                        [_vm._v(_vm._s(_vm.recordingDuration))]
                                      )
                                    ]
                                  )
                                : _c(
                                    "button",
                                    {
                                      directives: [
                                        {
                                          name: "b-tooltip",
                                          rawName: "v-b-tooltip.hover.d500",
                                          modifiers: { hover: true, d500: true }
                                        }
                                      ],
                                      staticClass:
                                        "btn action d-none d-md-inline-block",
                                      attrs: {
                                        type: "button",
                                        title: _vm.$t("meeting.start_recording")
                                      },
                                      on: { click: _vm.startRecording }
                                    },
                                    [_c("i", { staticClass: "fas fa-circle" })]
                                  )
                            ]
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.pageConfigs.enableScreenSharing
                          ? [
                              _vm.localScreenStreamid
                                ? _c(
                                    "button",
                                    {
                                      directives: [
                                        {
                                          name: "b-tooltip",
                                          rawName: "v-b-tooltip.hover.d500",
                                          modifiers: { hover: true, d500: true }
                                        }
                                      ],
                                      staticClass:
                                        "btn action disabled-text d-none d-md-inline-block",
                                      attrs: {
                                        type: "button",
                                        title: _vm.$t(
                                          "meeting.stop_sharing_screen"
                                        )
                                      },
                                      on: { click: _vm.stopSharingScreen }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fas fa-minus-square"
                                      })
                                    ]
                                  )
                                : _c(
                                    "button",
                                    {
                                      directives: [
                                        {
                                          name: "b-tooltip",
                                          rawName: "v-b-tooltip.hover.d500",
                                          modifiers: { hover: true, d500: true }
                                        }
                                      ],
                                      staticClass:
                                        "btn action d-none d-md-inline-block",
                                      attrs: {
                                        type: "button",
                                        title: _vm.$t("meeting.share_screen")
                                      },
                                      on: { click: _vm.shareScreen }
                                    },
                                    [_c("i", { staticClass: "fas fa-desktop" })]
                                  )
                            ]
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.pageConfigs.enableHandGesture
                          ? [
                              _vm.isHandUp
                                ? _c(
                                    "button",
                                    {
                                      directives: [
                                        {
                                          name: "b-tooltip",
                                          rawName: "v-b-tooltip.hover.d500",
                                          modifiers: { hover: true, d500: true }
                                        }
                                      ],
                                      staticClass:
                                        "btn action success-text d-none d-md-inline-block",
                                      attrs: {
                                        type: "button",
                                        title: _vm.$t("meeting.lower_hand")
                                      },
                                      on: { click: _vm.toggleHandUp }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "far fa-hand-paper"
                                      })
                                    ]
                                  )
                                : _c(
                                    "button",
                                    {
                                      directives: [
                                        {
                                          name: "b-tooltip",
                                          rawName: "v-b-tooltip.hover.d500",
                                          modifiers: { hover: true, d500: true }
                                        }
                                      ],
                                      staticClass:
                                        "btn action d-none d-md-inline-block",
                                      attrs: {
                                        type: "button",
                                        title: _vm.$t("meeting.raise_hand")
                                      },
                                      on: { click: _vm.toggleHandUp }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fas fa-hand-paper"
                                      })
                                    ]
                                  )
                            ]
                          : _vm._e()
                      ],
                      2
                    )
                  : _vm._e(),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "meeting-actions meeting-actions-right" },
                  [
                    _vm.entity
                      ? [
                          _vm.entity.status && _vm.videoList.length
                            ? _c(
                                "base-dropdown",
                                {
                                  staticClass: "more-actions",
                                  attrs: {
                                    tag: "div",
                                    direction: "up",
                                    "menu-classes": "show-dropdown-up",
                                    position: "right"
                                  }
                                },
                                [
                                  _c(
                                    "button",
                                    {
                                      directives: [
                                        {
                                          name: "b-tooltip",
                                          rawName: "v-b-tooltip.hover.d500",
                                          modifiers: { hover: true, d500: true }
                                        }
                                      ],
                                      staticClass:
                                        "btn action grid-toggle-btn d-none d-md-inline-block",
                                      attrs: {
                                        slot: "title",
                                        type: "button",
                                        title: _vm.$t("meeting.change_layout")
                                      },
                                      slot: "title"
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fas fa-th-large"
                                      })
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "button",
                                    {
                                      staticClass:
                                        "dropdown-item d-flex justify-content-between align-items-center",
                                      attrs: { type: "button" },
                                      on: {
                                        click: function($event) {
                                          return _vm.toggleLayout("grid")
                                        }
                                      }
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(_vm.$t("meeting.show_as_grid")) +
                                          " "
                                      ),
                                      _c("i", {
                                        staticClass:
                                          "fas fa-check min-width-auto text-light",
                                        class: {
                                          "text-dark":
                                            _vm.pageConfigs.layout === "grid"
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "button",
                                    {
                                      staticClass:
                                        "dropdown-item d-flex justify-content-between align-items-center",
                                      attrs: { type: "button" },
                                      on: {
                                        click: function($event) {
                                          return _vm.toggleLayout("gallery")
                                        }
                                      }
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm.$t("meeting.show_as_gallery")
                                        ) + " "
                                      ),
                                      _c("i", {
                                        staticClass:
                                          "fas fa-check min-width-auto text-light",
                                        class: {
                                          "text-dark":
                                            _vm.pageConfigs.layout === "gallery"
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "button",
                                    {
                                      staticClass:
                                        "dropdown-item d-flex justify-content-between align-items-center",
                                      attrs: { type: "button" },
                                      on: {
                                        click: function($event) {
                                          return _vm.toggleLayout("fullpage")
                                        }
                                      }
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm.$t("meeting.show_fullpage")
                                        ) + " "
                                      ),
                                      _vm.pageConfigs.layout === "fullpage"
                                        ? _c("i", {
                                            staticClass:
                                              "fas fa-check min-width-auto text-light",
                                            class: {
                                              "text-dark":
                                                _vm.pageConfigs.layout ===
                                                "fullpage"
                                            }
                                          })
                                        : _vm._e()
                                    ]
                                  )
                                ]
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _c(
                            "button",
                            {
                              directives: [
                                {
                                  name: "b-tooltip",
                                  rawName: "v-b-tooltip.hover.d500",
                                  modifiers: { hover: true, d500: true }
                                }
                              ],
                              staticClass: "btn action",
                              attrs: {
                                type: "button",
                                title: _vm.$t("general.share")
                              },
                              on: { click: _vm.shareURL }
                            },
                            [_c("i", { staticClass: "fas fa-share-alt" })]
                          ),
                          _vm._v(" "),
                          _vm.pageConfigs.enableChat &&
                          _vm.configs.chat &&
                          _vm.configs.chat.enabled &&
                          _vm.subscriptions &&
                          _vm.subscriptions.userPrivate &&
                          !_vm.entity.isBlocked
                            ? _c(
                                "button",
                                {
                                  directives: [
                                    {
                                      name: "b-tooltip",
                                      rawName: "v-b-tooltip.hover.d500",
                                      modifiers: { hover: true, d500: true }
                                    }
                                  ],
                                  class: [
                                    "btn action chats-toggle-btn",
                                    {
                                      pulse:
                                        _vm.totalUnreadMessages &&
                                        _vm.totalUnreadMessages > 0 &&
                                        !_vm.isChatBoxShown
                                    }
                                  ],
                                  attrs: {
                                    type: "button",
                                    title: _vm.$t(
                                      _vm.isChatBoxShown
                                        ? "global.hide"
                                        : "global.show",
                                      { attribute: _vm.$t("chat.chats") }
                                    )
                                  },
                                  on: { click: _vm.toggleChatBox }
                                },
                                [
                                  _c("i", {
                                    class: [
                                      _vm.isChatBoxShown
                                        ? "fas fa-comment-slash"
                                        : "far fa-comment"
                                    ]
                                  }),
                                  _vm._v(" "),
                                  _vm.totalUnreadMessages &&
                                  _vm.totalUnreadMessages > 0
                                    ? _c(
                                        "span",
                                        {
                                          staticClass:
                                            "text-success unread-count"
                                        },
                                        [
                                          _vm._v(
                                            _vm._s(_vm.totalUnreadMessages)
                                          )
                                        ]
                                      )
                                    : _vm._e()
                                ]
                              )
                            : _vm._e()
                        ]
                      : _vm._e(),
                    _vm._v(" "),
                    _c(
                      "base-dropdown",
                      {
                        staticClass: "more-actions",
                        attrs: {
                          tag: "div",
                          direction: "up",
                          "menu-classes": "show-dropdown-up",
                          position: "right"
                        }
                      },
                      [
                        _c(
                          "button",
                          {
                            staticClass: "btn action more-actions-btn",
                            attrs: { slot: "title", type: "button" },
                            slot: "title"
                          },
                          [_c("i", { staticClass: "fas fa-ellipsis-v" })]
                        ),
                        _vm._v(" "),
                        !(
                          _vm.entity &&
                          _vm.entity.status &&
                          _vm.videoList.length
                        )
                          ? _c(
                              "button",
                              {
                                staticClass: "dropdown-item",
                                attrs: { type: "button" },
                                on: {
                                  click: function($event) {
                                    return _vm.$router.push({
                                      name: "appMeetingList"
                                    })
                                  }
                                }
                              },
                              [
                                _c("i", { staticClass: "fas fa-chevron-left" }),
                                _vm._v(" " + _vm._s(_vm.$t("general.back")))
                              ]
                            )
                          : _c(
                              "button",
                              {
                                staticClass: "dropdown-item",
                                attrs: { type: "button" },
                                on: { click: _vm.getOffline }
                              },
                              [
                                _c("i", { staticClass: "fas fa-phone-slash" }),
                                _vm._v(
                                  " " + _vm._s(_vm.$t("meeting.leave_meeting"))
                                )
                              ]
                            ),
                        _vm._v(" "),
                        _vm.entity &&
                        _vm.entity.status &&
                        _vm.entity.canModerate &&
                        _vm.entity.status === "live"
                          ? _c(
                              "button",
                              {
                                staticClass: "dropdown-item",
                                attrs: { type: "button" },
                                on: { click: _vm.endMeeting }
                              },
                              [
                                _c("i", {
                                  staticClass: "fas fa-calendar-times"
                                }),
                                _vm._v(
                                  " " + _vm._s(_vm.$t("meeting.end_meeting"))
                                )
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.pageConfigs.hasAgenda
                          ? _c(
                              "button",
                              {
                                staticClass: "dropdown-item agenda-toggle-btn",
                                attrs: { type: "button" },
                                on: { click: _vm.toggleAgenda }
                              },
                              [
                                _c("i", { staticClass: "fas fa-quote-left" }),
                                _vm._v(
                                  " " +
                                    _vm._s(
                                      _vm.$t(
                                        _vm.showAgenda
                                          ? "global.hide"
                                          : "global.show",
                                        { attribute: _vm.$t("general.agenda") }
                                      )
                                    )
                                )
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _c(
                          "button",
                          {
                            staticClass: "dropdown-item show-devices-btn",
                            attrs: { type: "button" },
                            on: { click: _vm.toggleDevicesModal }
                          },
                          [
                            _c("i", { staticClass: "fas fa-cogs" }),
                            _vm._v(
                              " " +
                                _vm._s(_vm.$t("meeting.media_devices.setup"))
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _vm.pageConfigs.enableFileSharing &&
                        _vm.entity &&
                        _vm.entity.status &&
                        _vm.videoList.length
                          ? _c(
                              "button",
                              {
                                staticClass: "dropdown-item file-sharing-btn",
                                attrs: { type: "button" },
                                on: { click: _vm.toggleFileSharing }
                              },
                              [
                                _c("i", { staticClass: "fas fa-upload" }),
                                _vm._v(
                                  " " + _vm._s(_vm.$t("upload.share_files"))
                                )
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _c(
                          "button",
                          {
                            staticClass: "dropdown-item d-none d-md-block",
                            attrs: { type: "button" },
                            on: { click: _vm.toggleFooterAutoHide }
                          },
                          [
                            _c("i", { staticClass: "fas fa-hourglass-start" }),
                            _vm._v(
                              " " +
                                _vm._s(
                                  _vm.$t(
                                    _vm.pageConfigs.footerAutoHide
                                      ? "meeting.toggle_autohide_off"
                                      : "meeting.toggle_autohide_on"
                                  )
                                )
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _vm.uiConfigs.fullScreen
                          ? _c(
                              "button",
                              {
                                staticClass: "dropdown-item d-none d-md-block",
                                attrs: { type: "button" },
                                on: {
                                  click: function($event) {
                                    return _vm.toggleFullScreen(false)
                                  }
                                }
                              },
                              [
                                _c("i", { staticClass: "fas fa-compress" }),
                                _vm._v(
                                  " " +
                                    _vm._s(
                                      _vm.$t("config.ui.toggle_fullscreen")
                                    )
                                )
                              ]
                            )
                          : _c(
                              "button",
                              {
                                staticClass: "dropdown-item d-none d-md-block",
                                attrs: { type: "button" },
                                on: {
                                  click: function($event) {
                                    return _vm.toggleFullScreen(true)
                                  }
                                }
                              },
                              [
                                _c("i", { staticClass: "fas fa-expand" }),
                                _vm._v(
                                  " " +
                                    _vm._s(
                                      _vm.$t("config.ui.toggle_fullscreen")
                                    )
                                )
                              ]
                            )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "base-dropdown",
                      {
                        staticClass: "more-actions",
                        attrs: {
                          tag: "div",
                          direction: "up",
                          "menu-classes": "show-dropdown-up",
                          position: "right"
                        }
                      },
                      [
                        _c(
                          "button",
                          {
                            staticClass:
                              "btn action grid-toggle-btn d-none d-md-inline-block px-1",
                            attrs: { slot: "title", type: "button" },
                            slot: "title"
                          },
                          [
                            _vm.loggedInUser
                              ? _c("user-avatar", {
                                  attrs: {
                                    user: _vm.loggedInUser,
                                    size: 40,
                                    color:
                                      _vm.vars.colors[
                                        _vm.uiConfigs.topNavbarColor
                                      ]
                                  }
                                })
                              : _vm._e()
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c("h6", { staticClass: "dropdown-header" }, [
                          _vm._v(
                            _vm._s(
                              _vm.$t("general.greeting") +
                                ", " +
                                _vm.loggedInUser.name
                            )
                          )
                        ]),
                        _vm._v(" "),
                        _vm.loggedInUser
                          ? _c(
                              "router-link",
                              {
                                staticClass: "dropdown-item",
                                attrs: { to: { name: "appProfileView" } }
                              },
                              [
                                _c("i", { staticClass: "fas fa-user" }),
                                _vm._v(
                                  " " +
                                    _vm._s(_vm.$t("user.your_profile")) +
                                    "\n                    "
                                )
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        !_vm.hasPermission("access-config")
                          ? _c(
                              "router-link",
                              {
                                staticClass: "dropdown-item",
                                attrs: { to: { name: "appUserPreference" } }
                              },
                              [
                                _c("i", { staticClass: "fas fa-user-cog" }),
                                _vm._v(
                                  " " +
                                    _vm._s(_vm.$t("user.user_preference")) +
                                    "\n                    "
                                )
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.configs.auth.lockScreen
                          ? _c(
                              "button",
                              {
                                staticClass: "dropdown-item",
                                attrs: { type: "button" },
                                on: { click: _vm.lock }
                              },
                              [
                                _c("i", { staticClass: "fas fa-lock" }),
                                _vm._v(" " + _vm._s(_vm.$t("auth.lock.lock")))
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _c(
                          "router-link",
                          {
                            staticClass: "dropdown-item",
                            attrs: { to: { name: "appUserChangePassword" } }
                          },
                          [
                            _c("i", { staticClass: "fas fa-key" }),
                            _vm._v(
                              " " +
                                _vm._s(
                                  _vm.$t("auth.password.change_password")
                                ) +
                                "\n                    "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c("div", {
                          staticClass: "dropdown-divider",
                          attrs: { role: "separator" }
                        }),
                        _vm._v(" "),
                        _c(
                          "button",
                          {
                            staticClass: "dropdown-item",
                            attrs: { type: "button" },
                            on: { click: _vm.logout }
                          },
                          [
                            _c("i", { staticClass: "fas fa-power-off" }),
                            _vm._v(" " + _vm._s(_vm.$t("auth.logout")))
                          ]
                        )
                      ],
                      1
                    )
                  ],
                  2
                )
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                class: [
                  "agenda-streams-container",
                  { "has-agenda": _vm.pageConfigs.hasAgenda },
                  { "agenda-visible": _vm.showAgenda }
                ]
              },
              [
                _vm.pageConfigs.hasAgenda && _vm.entity && !_vm.entity.isBlocked
                  ? [
                      _c(
                        "card",
                        {
                          staticClass: "agenda-container",
                          attrs: {
                            shadow: "",
                            "body-classes": "agenda-wrapper"
                          }
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "meeting-agenda-container" },
                            [
                              _c(
                                "vue-scroll",
                                {
                                  ref: "vue-scroll-agenda",
                                  attrs: { id: "vue-scroll-agenda" }
                                },
                                [
                                  _vm.entity
                                    ? [
                                        _vm._m(0),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "text-muted mt-3" },
                                          [_vm._m(1), _vm._v(" "), _vm._m(2)]
                                        ),
                                        _vm._v(" "),
                                        _c("view-paragraph", {
                                          staticClass: "mt-3",
                                          attrs: {
                                            label: _vm.$t(
                                              "meeting.props.agenda"
                                            ),
                                            value: _vm.entity.agenda
                                          }
                                        }),
                                        _vm._v(" "),
                                        _vm.entity.description
                                          ? _c("view-paragraph", {
                                              staticClass: "mt-3",
                                              attrs: {
                                                label: _vm.$t(
                                                  "meeting.props.description"
                                                ),
                                                value: _vm.entity.description,
                                                html: ""
                                              }
                                            })
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.entity.attachments
                                          ? _c("view-uploads", {
                                              class: [
                                                "mt-3",
                                                _vm.uiConfigs.colorSchemeType
                                              ],
                                              attrs: {
                                                label: _vm.$t(
                                                  "upload.attachments"
                                                ),
                                                "name-label": _vm.$t(
                                                  "upload.attachment"
                                                ),
                                                value: _vm.entity.attachments,
                                                "download-button-classes":
                                                  "text-gray",
                                                "url-prefix":
                                                  "/meetings/" +
                                                  _vm.uuid +
                                                  "/downloads"
                                              }
                                            })
                                          : _vm._e()
                                      ]
                                    : _vm._e()
                                ],
                                2
                              )
                            ],
                            1
                          )
                        ]
                      )
                    ]
                  : _vm._e(),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    class: [
                      "streams-container",
                      _vm.hasVideos
                        ? "stream-" + _vm.pageConfigs.layout + "-layout"
                        : ""
                    ]
                  },
                  [
                    _c(
                      "div",
                      {
                        ref: "videoListEle",
                        class: ["video-list", _vm.hasVideosClasses]
                      },
                      [
                        _vm.hasVideos
                          ? _vm._l(_vm.videoList, function(item, itemIndex) {
                              return _c(
                                "div",
                                {
                                  key: "" + item.uuid + item.id,
                                  class: [
                                    "video-item video-card",
                                    { maximized: item.maximized },
                                    { local: item.local },
                                    { screen: item.screen },
                                    {
                                      "fullscreen-item":
                                        item.id === _vm.fullScreenItemId
                                    },
                                    { "hand-up": item.isHandUp },
                                    { "audio-muted": item.audioMuted },
                                    {
                                      "video-muted":
                                        item.videoMuted || !item.hasVideo
                                    },
                                    { "no-stream": !item.status }
                                  ],
                                  on: {
                                    dblclick: function($event) {
                                      return _vm.changeFocus(item)
                                    }
                                  }
                                },
                                [
                                  _c("div", { staticClass: "video-wrapper" }, [
                                    _c("video", {
                                      ref: "videos",
                                      refInFor: true,
                                      class: [
                                        {
                                          "no-poster":
                                            item.status && !item.videoMuted
                                        }
                                      ],
                                      attrs: {
                                        autoplay: "",
                                        playsinline: "",
                                        id: item.id,
                                        poster: item.status
                                          ? null
                                          : "/images/video/no-stream.jpg"
                                      },
                                      domProps: { muted: item.muted }
                                    })
                                  ]),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "caption" }, [
                                    _c(
                                      "h6",
                                      [
                                        _c(
                                          "span",
                                          {
                                            class: [
                                              "text-xs mr-1",
                                              { "text-success": item.status },
                                              { "text-danger": !item.status }
                                            ]
                                          },
                                          [
                                            _c("i", {
                                              staticClass: "fas fa-circle"
                                            })
                                          ]
                                        ),
                                        _vm._v(" "),
                                        item.local
                                          ? [
                                              _vm._v(
                                                "\n                                        " +
                                                  _vm._s(
                                                    item.screen
                                                      ? _vm.$t(
                                                          "meeting.your_screen"
                                                        )
                                                      : _vm.$t("meeting.you")
                                                  ) +
                                                  "\n                                    "
                                              )
                                            ]
                                          : [
                                              _vm._v(
                                                "\n                                        " +
                                                  _vm._s(
                                                    item.screen
                                                      ? "" +
                                                          item.name +
                                                          _vm.$t(
                                                            "meeting.screen"
                                                          )
                                                      : item.name
                                                  ) +
                                                  "\n                                    "
                                              )
                                            ],
                                        _vm._v(" "),
                                        !item.status
                                          ? _c(
                                              "span",
                                              {
                                                staticClass:
                                                  "bracketed text-danger"
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(_vm.$t("meeting.left"))
                                                )
                                              ]
                                            )
                                          : _vm._e()
                                      ],
                                      2
                                    )
                                  ]),
                                  _vm._v(" "),
                                  !item.local
                                    ? _c(
                                        "div",
                                        {
                                          staticClass: "custom-controls-wrapper"
                                        },
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "custom-controls" },
                                            [
                                              item.isHandUp
                                                ? _c(
                                                    "div",
                                                    {
                                                      staticClass: "status-icon"
                                                    },
                                                    [
                                                      _c("i", {
                                                        staticClass:
                                                          "fas fa-hand-paper enabled-text"
                                                      })
                                                    ]
                                                  )
                                                : _vm._e(),
                                              _vm._v(" "),
                                              item.audioMuted &&
                                              !item.videoMuted
                                                ? _c(
                                                    "div",
                                                    {
                                                      staticClass: "status-icon"
                                                    },
                                                    [
                                                      _c("i", {
                                                        staticClass:
                                                          "fas fa-microphone-slash disabled-text"
                                                      })
                                                    ]
                                                  )
                                                : _vm._e(),
                                              _vm._v(" "),
                                              _vm.entity.canModerate
                                                ? _c(
                                                    "button",
                                                    {
                                                      on: {
                                                        click: function(
                                                          $event
                                                        ) {
                                                          return _vm.kickRemoteUser(
                                                            item,
                                                            itemIndex
                                                          )
                                                        }
                                                      }
                                                    },
                                                    [
                                                      _c("i", {
                                                        staticClass:
                                                          "fas fa-ban enabled-text"
                                                      })
                                                    ]
                                                  )
                                                : _vm._e(),
                                              _vm._v(" "),
                                              _c(
                                                "button",
                                                {
                                                  on: {
                                                    click: function($event) {
                                                      return _vm.toggleRemoteAudio(
                                                        item,
                                                        itemIndex
                                                      )
                                                    }
                                                  }
                                                },
                                                [
                                                  _c("i", {
                                                    class: [
                                                      "fas",
                                                      {
                                                        "fa-volume-up enabled-text": !item.muted
                                                      },
                                                      {
                                                        "fa-volume-off disabled-text":
                                                          item.muted
                                                      }
                                                    ]
                                                  })
                                                ]
                                              )
                                            ]
                                          )
                                        ]
                                      )
                                    : item.local && item.isHandUp
                                    ? _c(
                                        "div",
                                        {
                                          staticClass: "custom-controls-wrapper"
                                        },
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "custom-controls" },
                                            [
                                              _c(
                                                "div",
                                                { staticClass: "status-icon" },
                                                [
                                                  _c("i", {
                                                    staticClass:
                                                      "fas fa-hand-paper enabled-text"
                                                  })
                                                ]
                                              )
                                            ]
                                          )
                                        ]
                                      )
                                    : _vm._e()
                                ]
                              )
                            })
                          : _c(
                              "card",
                              {
                                staticClass: "video-item wait-box maximized",
                                attrs: { shadow: "" }
                              },
                              [
                                _vm.entity && !_vm.entity.isBlocked
                                  ? [
                                      _vm.entity.status === "scheduled"
                                        ? [
                                            _vm.startDateTimeIsFuture
                                              ? _c("flip-countdown", {
                                                  staticClass: "my-3",
                                                  attrs: {
                                                    deadline:
                                                      _vm.entity.startDateTime
                                                  }
                                                })
                                              : [
                                                  _vm.entity.canModerate
                                                    ? [
                                                        _c(
                                                          "h5",
                                                          {
                                                            staticClass:
                                                              "text-muted text-center my-3"
                                                          },
                                                          [
                                                            _vm._v(
                                                              "\n                                            " +
                                                                _vm._s(
                                                                  _vm.$t(
                                                                    "meeting.waiting_for_you_to_start"
                                                                  )
                                                                ) +
                                                                "\n                                        "
                                                            )
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "d-flex justify-content-center my-3"
                                                          },
                                                          [
                                                            _c(
                                                              "base-button",
                                                              {
                                                                attrs: {
                                                                  type:
                                                                    "button",
                                                                  design:
                                                                    "primary",
                                                                  size: "lg"
                                                                },
                                                                on: {
                                                                  click:
                                                                    _vm.getOnline
                                                                }
                                                              },
                                                              [
                                                                _vm._v(
                                                                  _vm._s(
                                                                    _vm.$t(
                                                                      "global.click_to",
                                                                      {
                                                                        attribute: _vm.$t(
                                                                          "meeting.get_live"
                                                                        )
                                                                      }
                                                                    )
                                                                  )
                                                                )
                                                              ]
                                                            )
                                                          ],
                                                          1
                                                        )
                                                      ]
                                                    : _c(
                                                        "h5",
                                                        {
                                                          staticClass:
                                                            "text-muted text-center my-3"
                                                        },
                                                        [
                                                          _vm._v(
                                                            "\n                                        " +
                                                              _vm._s(
                                                                _vm.$t(
                                                                  "meeting.starting_any_time_now"
                                                                )
                                                              ) +
                                                              "\n                                    "
                                                          )
                                                        ]
                                                      )
                                                ],
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              {
                                                staticClass: "text-center my-3"
                                              },
                                              [
                                                _c(
                                                  "span",
                                                  { staticClass: "text-muted" },
                                                  [
                                                    _vm._v(
                                                      _vm._s(
                                                        _vm.$t(
                                                          "meeting.scheduled_for"
                                                        )
                                                      )
                                                    )
                                                  ]
                                                ),
                                                _c(
                                                  "span",
                                                  {
                                                    directives: [
                                                      {
                                                        name: "b-tooltip",
                                                        rawName:
                                                          "v-b-tooltip.hover.d500",
                                                        modifiers: {
                                                          hover: true,
                                                          d500: true
                                                        }
                                                      }
                                                    ],
                                                    staticClass:
                                                      "text-muted-lg m-l-5",
                                                    attrs: {
                                                      title: _vm._f(
                                                        "momentDateTime"
                                                      )(
                                                        _vm.entity.startDateTime
                                                      )
                                                    }
                                                  },
                                                  [
                                                    _vm._v(
                                                      _vm._s(
                                                        _vm._f(
                                                          "momentCalendar"
                                                        )(
                                                          _vm.entity
                                                            .startDateTime,
                                                          {
                                                            sameElse:
                                                              _vm.vars
                                                                .defaultDateTimeFormat
                                                          }
                                                        )
                                                      )
                                                    )
                                                  ]
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _vm.liveMembersCount > 0
                                              ? _c(
                                                  "div",
                                                  {
                                                    staticClass:
                                                      "text-muted text-center my-3"
                                                  },
                                                  [
                                                    _c(
                                                      "span",
                                                      {
                                                        staticClass:
                                                          "text-xs text-success mr-1"
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fas fa-circle"
                                                        })
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c("animated-number", {
                                                      attrs: {
                                                        number:
                                                          _vm.liveMembersCount,
                                                        delay: 100
                                                      }
                                                    }),
                                                    _vm._v(" "),
                                                    _vm.liveMembersCount > 1
                                                      ? _c("span", [
                                                          _vm._v(
                                                            _vm._s(
                                                              _vm.$t(
                                                                "meeting.members_online"
                                                              )
                                                            )
                                                          )
                                                        ])
                                                      : _c("span", [
                                                          _vm._v(
                                                            _vm._s(
                                                              _vm.$t(
                                                                "meeting.member_online"
                                                              )
                                                            )
                                                          )
                                                        ])
                                                  ],
                                                  1
                                                )
                                              : _vm._e(),
                                            _vm._v(" "),
                                            _vm.entity.canModerate
                                              ? [
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass:
                                                        "d-flex justify-content-center my-3"
                                                    },
                                                    [
                                                      _c(
                                                        "base-button",
                                                        {
                                                          attrs: {
                                                            type: "button",
                                                            design:
                                                              "gray-darker",
                                                            size: "sm"
                                                          },
                                                          on: {
                                                            click:
                                                              _vm.cancelMeeting
                                                          }
                                                        },
                                                        [
                                                          _c("i", {
                                                            staticClass:
                                                              "fas fa-times m-r-5"
                                                          }),
                                                          _vm._v(
                                                            " " +
                                                              _vm._s(
                                                                _vm.$t(
                                                                  "meeting.cancel"
                                                                )
                                                              )
                                                          )
                                                        ]
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "base-dropdown",
                                                        {
                                                          attrs: {
                                                            tag: "div",
                                                            size: "sm",
                                                            direction: "up"
                                                          }
                                                        },
                                                        [
                                                          _c(
                                                            "base-button",
                                                            {
                                                              attrs: {
                                                                slot: "title",
                                                                type: "button",
                                                                design:
                                                                  "gray-darker",
                                                                size: "sm",
                                                                "data-toggle":
                                                                  "dropdown",
                                                                role: "button"
                                                              },
                                                              slot: "title"
                                                            },
                                                            [
                                                              _c("i", {
                                                                staticClass:
                                                                  "far fa-clock m-r-5"
                                                              }),
                                                              _vm._v(" "),
                                                              _c(
                                                                "span",
                                                                {
                                                                  staticClass:
                                                                    "nav-link-inner--text"
                                                                },
                                                                [
                                                                  _vm._v(
                                                                    _vm._s(
                                                                      _vm.$t(
                                                                        "meeting.snooze"
                                                                      )
                                                                    )
                                                                  )
                                                                ]
                                                              ),
                                                              _vm._v(" "),
                                                              _c("i", {
                                                                staticClass:
                                                                  "fas fa-chevron-up m-l-5"
                                                              })
                                                            ]
                                                          ),
                                                          _vm._v(" "),
                                                          _vm._l(
                                                            _vm.snoozeOpts,
                                                            function(opt) {
                                                              return _c(
                                                                "button",
                                                                {
                                                                  staticClass:
                                                                    "dropdown-item",
                                                                  attrs: {
                                                                    type:
                                                                      "button"
                                                                  },
                                                                  on: {
                                                                    click: function(
                                                                      $event
                                                                    ) {
                                                                      return _vm.snoozeMeeting(
                                                                        opt.uuid
                                                                      )
                                                                    }
                                                                  }
                                                                },
                                                                [
                                                                  _vm._v(
                                                                    _vm._s(
                                                                      opt.name
                                                                    ) +
                                                                      " " +
                                                                      _vm._s(
                                                                        _vm.$t(
                                                                          "list.general.durations." +
                                                                            opt.type
                                                                        )
                                                                      )
                                                                  )
                                                                ]
                                                              )
                                                            }
                                                          )
                                                        ],
                                                        2
                                                      )
                                                    ],
                                                    1
                                                  )
                                                ]
                                              : _vm._e()
                                          ]
                                        : _vm.entity.status === "live"
                                        ? [
                                            _vm.roomIdAlive
                                              ? [
                                                  _c(
                                                    "h5",
                                                    {
                                                      staticClass:
                                                        "text-muted text-center my-3"
                                                    },
                                                    [
                                                      _vm._v(
                                                        "\n                                        " +
                                                          _vm._s(
                                                            _vm.$t(
                                                              "meeting.is_live_now"
                                                            )
                                                          ) +
                                                          "\n                                    "
                                                      )
                                                    ]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass:
                                                        "d-flex justify-content-center my-3"
                                                    },
                                                    [
                                                      _c(
                                                        "base-button",
                                                        {
                                                          attrs: {
                                                            type: "button",
                                                            design: "primary",
                                                            size: "lg"
                                                          },
                                                          on: {
                                                            click: _vm.getOnline
                                                          }
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              _vm.$t(
                                                                "global.click_to",
                                                                {
                                                                  attribute: _vm.$t(
                                                                    "meeting.get_live"
                                                                  )
                                                                }
                                                              )
                                                            )
                                                          )
                                                        ]
                                                      )
                                                    ],
                                                    1
                                                  )
                                                ]
                                              : [
                                                  _vm.entity.canModerate
                                                    ? [
                                                        _c(
                                                          "h5",
                                                          {
                                                            staticClass:
                                                              "text-muted text-center my-3"
                                                          },
                                                          [
                                                            _vm._v(
                                                              "\n                                            " +
                                                                _vm._s(
                                                                  _vm.$t(
                                                                    "meeting.waiting_for_you_to_start"
                                                                  )
                                                                ) +
                                                                "\n                                        "
                                                            )
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "d-flex justify-content-center my-3"
                                                          },
                                                          [
                                                            _c(
                                                              "base-button",
                                                              {
                                                                attrs: {
                                                                  type:
                                                                    "button",
                                                                  design:
                                                                    "primary",
                                                                  size: "lg"
                                                                },
                                                                on: {
                                                                  click:
                                                                    _vm.getOnline
                                                                }
                                                              },
                                                              [
                                                                _vm._v(
                                                                  _vm._s(
                                                                    _vm.$t(
                                                                      "global.click_to",
                                                                      {
                                                                        attribute: _vm.$t(
                                                                          "meeting.get_live"
                                                                        )
                                                                      }
                                                                    )
                                                                  )
                                                                )
                                                              ]
                                                            )
                                                          ],
                                                          1
                                                        )
                                                      ]
                                                    : _c(
                                                        "h5",
                                                        {
                                                          staticClass:
                                                            "text-muted text-center my-3"
                                                        },
                                                        [
                                                          _vm._v(
                                                            "\n                                        " +
                                                              _vm._s(
                                                                _vm.$t(
                                                                  "meeting.starting_any_time_now"
                                                                )
                                                              ) +
                                                              "\n                                    "
                                                          )
                                                        ]
                                                      )
                                                ],
                                            _vm._v(" "),
                                            _vm.liveMembersCount > 0
                                              ? _c(
                                                  "div",
                                                  {
                                                    staticClass:
                                                      "text-muted text-center my-3"
                                                  },
                                                  [
                                                    _c(
                                                      "span",
                                                      {
                                                        staticClass:
                                                          "text-xs text-success mr-1"
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fas fa-circle"
                                                        })
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c("animated-number", {
                                                      attrs: {
                                                        number:
                                                          _vm.liveMembersCount,
                                                        delay: 100
                                                      }
                                                    }),
                                                    _vm._v(" "),
                                                    _vm.liveMembersCount > 1
                                                      ? _c("span", [
                                                          _vm._v(
                                                            _vm._s(
                                                              _vm.$t(
                                                                "meeting.members_online"
                                                              )
                                                            )
                                                          )
                                                        ])
                                                      : _c("span", [
                                                          _vm._v(
                                                            _vm._s(
                                                              _vm.$t(
                                                                "meeting.member_online"
                                                              )
                                                            )
                                                          )
                                                        ])
                                                  ],
                                                  1
                                                )
                                              : _vm._e()
                                          ]
                                        : _vm.entity.status === "cancelled"
                                        ? [
                                            _c(
                                              "h4",
                                              {
                                                staticClass:
                                                  "text-muted text-center my-3"
                                              },
                                              [
                                                _vm._v(
                                                  "\n                                    " +
                                                    _vm._s(
                                                      _vm.$t(
                                                        "meeting.meeting_cancelled"
                                                      )
                                                    ) +
                                                    "\n                                "
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              {
                                                staticClass:
                                                  "d-flex justify-content-center"
                                              },
                                              [
                                                _c(
                                                  "base-button",
                                                  {
                                                    attrs: {
                                                      type: "button",
                                                      design: "light",
                                                      size: "lg"
                                                    },
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.$router.push(
                                                          {
                                                            name:
                                                              "appMeetingList"
                                                          }
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _vm._v(
                                                      _vm._s(
                                                        _vm.$t(
                                                          "global.click_to",
                                                          {
                                                            attribute: _vm.$t(
                                                              "general.go_back"
                                                            )
                                                          }
                                                        )
                                                      )
                                                    )
                                                  ]
                                                )
                                              ],
                                              1
                                            )
                                          ]
                                        : _vm.entity.status === "ended"
                                        ? [
                                            _c(
                                              "h4",
                                              {
                                                staticClass:
                                                  "text-muted text-center my-3"
                                              },
                                              [
                                                _vm._v(
                                                  "\n                                    " +
                                                    _vm._s(
                                                      _vm.$t(
                                                        "meeting.meeting_ended"
                                                      )
                                                    ) +
                                                    "\n                                "
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              {
                                                staticClass:
                                                  "d-flex justify-content-center"
                                              },
                                              [
                                                _c(
                                                  "base-button",
                                                  {
                                                    attrs: {
                                                      type: "button",
                                                      design: "light",
                                                      size: "lg"
                                                    },
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.$router.push(
                                                          {
                                                            name:
                                                              "appMeetingList"
                                                          }
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _vm._v(
                                                      _vm._s(
                                                        _vm.$t(
                                                          "global.click_to",
                                                          {
                                                            attribute: _vm.$t(
                                                              "general.go_back"
                                                            )
                                                          }
                                                        )
                                                      )
                                                    )
                                                  ]
                                                )
                                              ],
                                              1
                                            )
                                          ]
                                        : _vm._e()
                                    ]
                                  : [
                                      _vm.entity
                                        ? _c(
                                            "h3",
                                            {
                                              staticClass:
                                                "text-center text-danger my-3"
                                            },
                                            [
                                              _vm._v(
                                                "\n                                " +
                                                  _vm._s(
                                                    _vm.$t(
                                                      "meeting.not_allowed"
                                                    )
                                                  ) +
                                                  "\n                            "
                                              )
                                            ]
                                          )
                                        : _vm._e()
                                    ]
                              ],
                              2
                            )
                      ],
                      2
                    )
                  ]
                )
              ],
              2
            ),
            _vm._v(" "),
            _vm.pageConfigs.enableChat &&
            _vm.configs.chat &&
            _vm.configs.chat.enabled &&
            _vm.entity &&
            !_vm.entity.isBlocked
              ? _c(
                  "div",
                  { staticClass: "chat-box-container" },
                  [
                    _vm.isChatBoxShown
                      ? _c("chat-box", {
                          attrs: {
                            "box-visibility": _vm.isChatBoxShown,
                            meeting: _vm.entity,
                            channel: _vm.subscriptions.userPrivate
                          },
                          on: {
                            boxHidden: function($event) {
                              _vm.isChatBoxShown = false
                            },
                            boxShown: function($event) {
                              _vm.isChatBoxShown = true
                            }
                          }
                        })
                      : _vm._e()
                  ],
                  1
                )
              : _vm._e(),
            _vm._v(" "),
            _c(
              "b-modal",
              {
                attrs: {
                  size: "md",
                  centered: "",
                  lazy: "",
                  busy: _vm.isLoading,
                  id: "devicesModal",
                  "no-close-on-backdrop": "",
                  "no-close-on-esc": "",
                  "ok-only": ""
                },
                on: { ok: _vm.onDevicesModalOK },
                model: {
                  value: _vm.showDevicesModal,
                  callback: function($$v) {
                    _vm.showDevicesModal = $$v
                  },
                  expression: "showDevicesModal"
                }
              },
              [
                _c("template", { slot: "modal-header" }, [
                  _c("h5", { staticClass: "modal-title" }, [
                    _vm._v(
                      _vm._s(
                        _vm.$t("meeting.media_devices.video_and_audio_devices")
                      )
                    )
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  [
                    _c("base-select", {
                      attrs: {
                        options: _vm.mediaDevices.audioInput,
                        label: _vm.$t("meeting.media_devices.audio_input"),
                        "allow-empty": false,
                        "preselect-first": ""
                      },
                      model: {
                        value: _vm.mediaConfigurations.selectedAudioInput,
                        callback: function($$v) {
                          _vm.$set(
                            _vm.mediaConfigurations,
                            "selectedAudioInput",
                            $$v
                          )
                        },
                        expression: "mediaConfigurations.selectedAudioInput"
                      }
                    }),
                    _vm._v(" "),
                    _c("base-select", {
                      attrs: {
                        options: _vm.mediaDevices.videoInput,
                        label: _vm.$t("meeting.media_devices.video_input"),
                        "allow-empty": false,
                        "preselect-first": ""
                      },
                      model: {
                        value: _vm.mediaConfigurations.selectedVideoInput,
                        callback: function($$v) {
                          _vm.$set(
                            _vm.mediaConfigurations,
                            "selectedVideoInput",
                            $$v
                          )
                        },
                        expression: "mediaConfigurations.selectedVideoInput"
                      }
                    }),
                    _vm._v(" "),
                    _c("div", { staticClass: "row" }, [
                      _c(
                        "div",
                        { staticClass: "col-12 col-md-4" },
                        [
                          _c("base-select", {
                            attrs: {
                              options: _vm.mediaDevices.resolutions,
                              label: _vm.$t(
                                "meeting.media_devices.video_resolution"
                              ),
                              "allow-empty": false,
                              "preselect-first": "",
                              simple: "",
                              "track-by": "label",
                              "show-by": "label"
                            },
                            model: {
                              value: _vm.mediaConfigurations.selectedResolution,
                              callback: function($$v) {
                                _vm.$set(
                                  _vm.mediaConfigurations,
                                  "selectedResolution",
                                  $$v
                                )
                              },
                              expression:
                                "mediaConfigurations.selectedResolution"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-12 col-md-4" },
                        [
                          _c("base-select", {
                            attrs: {
                              options: _vm.mediaDevices.frameRates,
                              label: _vm.$t("meeting.media_devices.frame_rate"),
                              "allow-empty": false,
                              "preselect-first": "",
                              simple: "",
                              "track-by": "uuid",
                              "show-by": "label"
                            },
                            model: {
                              value: _vm.mediaConfigurations.frameRate,
                              callback: function($$v) {
                                _vm.$set(
                                  _vm.mediaConfigurations,
                                  "frameRate",
                                  $$v
                                )
                              },
                              expression: "mediaConfigurations.frameRate"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-12 col-md-4" },
                        [
                          _c("base-select", {
                            attrs: {
                              options: _vm.mediaDevices.facingModes,
                              label: _vm.$t(
                                "meeting.media_devices.facing_mode"
                              ),
                              "allow-empty": false,
                              "preselect-first": "",
                              simple: "",
                              "track-by": "uuid",
                              "show-by": "label"
                            },
                            model: {
                              value: _vm.mediaConfigurations.facingMode,
                              callback: function($$v) {
                                _vm.$set(
                                  _vm.mediaConfigurations,
                                  "facingMode",
                                  $$v
                                )
                              },
                              expression: "mediaConfigurations.facingMode"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-12" }, [
                        _c("h6", { staticClass: "mt-2 pb-1" }, [
                          _vm._v(
                            _vm._s(_vm.$t("meeting.media_devices.bitrates"))
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-12 col-sm-4" },
                        [
                          _c("base-input", {
                            attrs: {
                              label: _vm.$t("meeting.media_devices.audio"),
                              type: "number",
                              min: 6,
                              max: 510
                            },
                            model: {
                              value: _vm.mediaConfigurations.bandwidth.audio,
                              callback: function($$v) {
                                _vm.$set(
                                  _vm.mediaConfigurations.bandwidth,
                                  "audio",
                                  $$v
                                )
                              },
                              expression: "mediaConfigurations.bandwidth.audio"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-12 col-sm-4" },
                        [
                          _c("base-input", {
                            attrs: {
                              label: _vm.$t("meeting.media_devices.video"),
                              type: "number",
                              min: 100,
                              max: 2000
                            },
                            model: {
                              value: _vm.mediaConfigurations.bandwidth.video,
                              callback: function($$v) {
                                _vm.$set(
                                  _vm.mediaConfigurations.bandwidth,
                                  "video",
                                  $$v
                                )
                              },
                              expression: "mediaConfigurations.bandwidth.video"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-12 col-sm-4" },
                        [
                          _c("base-input", {
                            attrs: {
                              label: _vm.$t("meeting.media_devices.screen"),
                              type: "number",
                              min: 300,
                              max: 4000
                            },
                            model: {
                              value: _vm.mediaConfigurations.bandwidth.screen,
                              callback: function($$v) {
                                _vm.$set(
                                  _vm.mediaConfigurations.bandwidth,
                                  "screen",
                                  $$v
                                )
                              },
                              expression: "mediaConfigurations.bandwidth.screen"
                            }
                          })
                        ],
                        1
                      )
                    ])
                  ],
                  1
                )
              ],
              2
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                class: [
                  "files-sharing-box d-none",
                  { "d-md-flex": _vm.showFileSharing }
                ]
              },
              [
                _c(
                  "card",
                  {
                    staticClass: "files-container",
                    attrs: { shadow: "", "body-classes": "files-wrapper" }
                  },
                  [
                    _c("file-sharer", {
                      attrs: { connection: _vm.fileSharingConnection },
                      on: {
                        hide: function($event) {
                          _vm.showFileSharing = false
                        },
                        show: function($event) {
                          _vm.showFileSharing = true
                        }
                      }
                    })
                  ],
                  1
                )
              ],
              1
            )
          ]
        : _c(
            "card",
            {
              staticClass:
                "duplicate-tab d-flex justify-content-center align-items-center min-height-90vh",
              attrs: { shadow: "" }
            },
            [_c("h2", [_vm._v(_vm._s(_vm.$t("meeting.duplicate_tab")))])]
          )
    ],
    2
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", { staticClass: "meeting-title" }, [
      _vm._v(_vm._s(_vm.entity.title))
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", [
      _vm._v(
        _vm._s(_vm.$t("meeting.props.type")) +
          ": " +
          _vm._s(_vm.entity.type.name) +
          ","
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", [
      _vm._v(
        _vm._s(_vm.$t("meeting.meeting_category.category")) +
          ": " +
          _vm._s(_vm.entity.category.name)
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/FileSharer.vue":
/*!************************************************!*\
  !*** ./resources/js/components/FileSharer.vue ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _FileSharer_vue_vue_type_template_id_68270e3a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./FileSharer.vue?vue&type=template&id=68270e3a& */ "./resources/js/components/FileSharer.vue?vue&type=template&id=68270e3a&");
/* harmony import */ var _FileSharer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./FileSharer.vue?vue&type=script&lang=js& */ "./resources/js/components/FileSharer.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _FileSharer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _FileSharer_vue_vue_type_template_id_68270e3a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _FileSharer_vue_vue_type_template_id_68270e3a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/FileSharer.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/FileSharer.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./resources/js/components/FileSharer.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FileSharer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./FileSharer.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/FileSharer.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FileSharer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/FileSharer.vue?vue&type=template&id=68270e3a&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/FileSharer.vue?vue&type=template&id=68270e3a& ***!
  \*******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FileSharer_vue_vue_type_template_id_68270e3a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./FileSharer.vue?vue&type=template&id=68270e3a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/FileSharer.vue?vue&type=template&id=68270e3a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FileSharer_vue_vue_type_template_id_68270e3a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FileSharer_vue_vue_type_template_id_68270e3a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/core/components/AnimatedNumber.vue":
/*!*********************************************************!*\
  !*** ./resources/js/core/components/AnimatedNumber.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AnimatedNumber_vue_vue_type_template_id_96304fda___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AnimatedNumber.vue?vue&type=template&id=96304fda& */ "./resources/js/core/components/AnimatedNumber.vue?vue&type=template&id=96304fda&");
/* harmony import */ var _AnimatedNumber_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AnimatedNumber.vue?vue&type=script&lang=js& */ "./resources/js/core/components/AnimatedNumber.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _AnimatedNumber_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AnimatedNumber_vue_vue_type_template_id_96304fda___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AnimatedNumber_vue_vue_type_template_id_96304fda___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/core/components/AnimatedNumber.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/core/components/AnimatedNumber.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/core/components/AnimatedNumber.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AnimatedNumber_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./AnimatedNumber.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/core/components/AnimatedNumber.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AnimatedNumber_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/core/components/AnimatedNumber.vue?vue&type=template&id=96304fda&":
/*!****************************************************************************************!*\
  !*** ./resources/js/core/components/AnimatedNumber.vue?vue&type=template&id=96304fda& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AnimatedNumber_vue_vue_type_template_id_96304fda___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./AnimatedNumber.vue?vue&type=template&id=96304fda& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/core/components/AnimatedNumber.vue?vue&type=template&id=96304fda&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AnimatedNumber_vue_vue_type_template_id_96304fda___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AnimatedNumber_vue_vue_type_template_id_96304fda___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/core/plugins/detect-duplicate-tab.js":
/*!***********************************************************!*\
  !*** ./resources/js/core/plugins/detect-duplicate-tab.js ***!
  \***********************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! js-cookie */ "./node_modules/js-cookie/src/js.cookie.js");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! uuid */ "./node_modules/uuid/dist/esm-browser/index.js");



var DuplicateWindow = function DuplicateWindow() {
  var localStorageTimeout = 5 * 1000; // 15,000 milliseconds = 15 seconds.

  var localStorageResetInterval = 1 / 2 * 1000; // 10,000 milliseconds = 10 seconds.

  var localStorageTabKey = 'my-application-browser-tab';
  var sessionStorageGuidKey = 'browser-tab-guid';
  var ItemType = {
    Session: 1,
    Local: 2
  };

  function getItem(itemtype) {
    var val = "";

    switch (itemtype) {
      case ItemType.Session:
        val = window.name;
        break;

      case ItemType.Local:
        val = decodeURIComponent(js_cookie__WEBPACK_IMPORTED_MODULE_0___default.a.get(localStorageTabKey));
        if (val == undefined || val == 'undefined') val = "";
        break;
    }

    return val;
  }

  function setItem(itemtype, val) {
    switch (itemtype) {
      case ItemType.Session:
        window.name = val;
        break;

      case ItemType.Local:
        js_cookie__WEBPACK_IMPORTED_MODULE_0___default.a.set(localStorageTabKey, val || "");
        break;
    }
  }

  function testIfDuplicate() {
    //console.log("In testTab")
    var sessionGuid = getItem(ItemType.Session) || Object(uuid__WEBPACK_IMPORTED_MODULE_1__["v4"])();
    setItem(ItemType.Session, sessionGuid);
    var val = getItem(ItemType.Local);
    var tabObj = (val == "" ? null : JSON.parse(val)) || null; // If no or stale tab object, our session is the winner.  If the guid matches, ours is still the winner

    if (tabObj === null || tabObj.timestamp < new Date().getTime() - localStorageTimeout || tabObj.guid === sessionGuid) {
      var setTabObj = function setTabObj() {
        //console.log("In setTabObj")
        var newTabObj = {
          guid: sessionGuid,
          timestamp: new Date().getTime()
        };
        setItem(ItemType.Local, JSON.stringify(newTabObj));
      };

      setTabObj();
      setInterval(setTabObj, localStorageResetInterval); //every x interval refresh timestamp in cookie

      return false;
    } else {
      // An active tab is already open that does not match our session guid.
      return true;
    }
  }

  window.isDuplicate = function () {
    var duplicate = testIfDuplicate(); //console.log("Is Duplicate: "+ duplicate)

    return duplicate;
  };

  window.addEventListener('beforeunload', function () {
    if (testIfDuplicate() == false) {
      setItem(ItemType.Local, "");
    }
  });
};

DuplicateWindow();

/***/ }),

/***/ "./resources/js/mixins/meeting.js":
/*!****************************************!*\
  !*** ./resources/js/mixins/meeting.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! socket.io-client */ "./node_modules/socket.io-client/lib/index.js");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(socket_io_client__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var rtcmulticonnection__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rtcmulticonnection */ "./node_modules/rtcmulticonnection/dist/RTCMultiConnection.js");
/* harmony import */ var rtcmulticonnection__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(rtcmulticonnection__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var recordrtc__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! recordrtc */ "./node_modules/recordrtc/RecordRTC.js");
/* harmony import */ var recordrtc__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(recordrtc__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var webrtc_adapter__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! webrtc-adapter */ "./node_modules/webrtc-adapter/src/js/adapter_core.js");
/* harmony import */ var fbr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! fbr */ "./node_modules/fbr/FileBufferReader.js");
/* harmony import */ var fbr__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(fbr__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var screenfull__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! screenfull */ "./node_modules/screenfull/dist/screenfull.js");
/* harmony import */ var screenfull__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(screenfull__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _js_echo_setup__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @js/echo-setup */ "./resources/js/echo-setup.js");
/* harmony import */ var _core_utils_media__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @core/utils/media */ "./resources/js/core/utils/media.js");
/* harmony import */ var _core_utils__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @core/utils */ "./resources/js/core/utils/index.js");
/* harmony import */ var _core_utils_auth__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @core/utils/auth */ "./resources/js/core/utils/auth.js");
/* harmony import */ var _core_filters_momentz__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @core/filters/momentz */ "./resources/js/core/filters/momentz.js");
/* harmony import */ var _core_plugins_detect_duplicate_tab__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @core/plugins/detect-duplicate-tab */ "./resources/js/core/plugins/detect-duplicate-tab.js");
/* harmony import */ var _core_configs_sweet_alert__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @core/configs/sweet-alert */ "./resources/js/core/configs/sweet-alert.js");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var ebml = document.createElement('script');
ebml.setAttribute('src', '/js/EBML.js');
document.head.appendChild(ebml);














window.io = socket_io_client__WEBPACK_IMPORTED_MODULE_1___default.a;
/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    RTCMultiConnection: rtcmulticonnection__WEBPACK_IMPORTED_MODULE_2___default.a
  },
  data: function data() {
    return {
      uuid: null,
      entity: null,
      isLoading: false,
      prevRoute: null,
      fullScreenItemId: null,
      meetingRoomId: null,
      initUrl: 'meetings',
      fallBackRoute: 'appMeetingList',
      newMessages: false,
      roomIdAlive: false,
      showFlipClock: true,
      duplicateTab: false,
      fullScreenInOn: false,
      permissionRejected: false,
      membersLive: [],
      videoList: [],
      mediaConfigurations: {
        selectedAudioInput: null,
        selectedAudioOutput: null,
        selectedVideoInput: null,
        selectedResolution: 'Auto',
        facingMode: 'Auto',
        frameRate: 'Auto',
        bandwidth: {
          audio: 256,
          video: 512,
          screen: 300
        }
      },
      mediaDevices: {
        audioInput: [],
        audioOutput: [],
        videoInput: [],
        resolutions: [{
          label: 'Auto',
          constraints: {}
        }, {
          label: '240p',
          constraints: {
            width: {
              max: 426,
              ideal: 426
            },
            height: {
              max: 240,
              ideal: 240
            }
          }
        }, {
          label: '360p',
          constraints: {
            width: {
              max: 640,
              ideal: 640
            },
            height: {
              max: 360,
              ideal: 360
            }
          }
        }, {
          label: '720p',
          constraints: {
            width: {
              max: 1280,
              ideal: 1280
            },
            height: {
              max: 720,
              ideal: 720
            }
          }
        }, {
          label: '1080p',
          constraints: {
            width: {
              max: 1920,
              ideal: 1920
            },
            height: {
              max: 1080,
              ideal: 1080
            }
          }
        }, {
          label: '4K',
          constraints: {
            width: {
              max: 4096,
              ideal: 4096
            },
            height: {
              max: 2160,
              ideal: 2160
            }
          }
        }],
        facingModes: [{
          uuid: 'Auto',
          label: 'Auto'
        }, {
          uuid: 'user',
          label: 'User'
        }, {
          uuid: 'environment',
          label: 'Environment'
        }],
        frameRates: [{
          uuid: 'Auto',
          label: 'Auto'
        }, {
          uuid: 30,
          label: '30 fps'
        }, {
          uuid: 60,
          label: '60 fps'
        }]
      },
      socketURL: 'aHR0cHM6Ly9zaWduYWwua29kZW1pbnQuaW46OTAwMS8=',
      // socketURL: 'aHR0cDovL2xvY2FsaG9zdDo5MDAxLw==',
      rtcmConnection: null,
      localVideo: null,
      localScreenStreamid: null,
      audioConstraints: {},
      videoConstraints: {},
      pageConfigs: {
        hasAgenda: true,
        enableChat: true,
        enableAudio: true,
        enableVideo: true,
        showEnableAudioBtn: true,
        showEnableVideoBtn: true,
        enableScreenSharing: true,
        enableRecording: true,
        enableHandGesture: true,
        footerAutoHide: false,
        enableFileSharing: true,
        layout: 'fullpage'
      },
      meetingRulesHost: {
        session: {
          audio: false,
          video: false,
          screen: false,
          data: false,
          oneway: false
        },
        mediaConstraints: {
          audio: false,
          video: false,
          screen: false
        },
        mandatory: {
          OfferToReceiveAudio: false,
          OfferToReceiveVideo: false
        }
      },
      meetingRulesGuest: {
        session: {
          audio: false,
          video: false,
          screen: false,
          data: false,
          oneway: false
        },
        mediaConstraints: {
          audio: false,
          video: false,
          screen: false
        },
        mandatory: {
          OfferToReceiveAudio: false,
          OfferToReceiveVideo: false
        }
      },
      snoozeOpts: [{
        uuid: 5,
        name: 5,
        type: 'm'
      }, {
        uuid: 10,
        name: 10,
        type: 'm'
      }, {
        uuid: 15,
        name: 15,
        type: 'm'
      }, {
        uuid: 30,
        name: 30,
        type: 'm'
      }, {
        uuid: 60,
        name: 1,
        type: 'h'
      }],
      isHandUp: false,
      recording: false,
      recorded: false,
      recordingDuration: null,
      showDevicesModal: false,
      showFileSharing: false,
      showAgenda: true,
      fileSharingConnection: null
    };
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('config', ['configs', 'uiConfigs', 'vars'])), {}, {
    user: function user() {
      return {
        uuid: this.userUuid,
        username: this.username,
        name: this.profile.name
      };
    },
    hasVideos: function hasVideos() {
      return this.videoList && this.videoList.length > 0 ? this.videoList.length : 0;
    },
    hasVideosClasses: function hasVideosClasses() {
      if (!this.hasVideos) {
        return 'has-no-video';
      }

      return 'has-videos ' + "has-".concat(this.hasVideos, "-videos ") + (this.hasVideos > 20 ? 'has-gt-20-videos' : this.hasVideos > 12 ? 'has-gt-12-videos' : this.hasVideos > 4 ? 'has-gt-4-videos' : this.hasVideos > 1 ? 'has-2-3-videos' : 'has-1-video');
    },
    liveMembersCount: function liveMembersCount() {
      return this.membersLive.length - 1;
    },
    startDateTimeIsFuture: function startDateTimeIsFuture() {
      var isInFuture = this.entity && this.showFlipClock && this.isStartDateTimeInFuture();

      if (isInFuture) {
        this.startCountDown();
      } else {
        if (window.countdownInterval) {
          clearInterval(window.countdownInterval);
        }
      }

      return isInFuture;
    }
  }),
  watch: {
    liveMembersCount: function liveMembersCount(newVal, oldVal) {
      if (!window.isLiveMeetingDestroyed && newVal !== oldVal) {
        this.meetingRoomCreated(this.entity);
      }
    }
  },
  methods: _objectSpread(_objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])('config', ['SetUiConfig'])), Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])('common', ['Init', 'Get', 'Custom', 'GetPreRequisite'])), {}, {
    shareURL: function shareURL() {
      var _this = this;

      if (!this.entity) {
        return;
      }

      this.$gaEvent('engagement', 'shareURL', 'Shown');
      var url = window.location.protocol + '//' + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
      url = url + (this.entity.identifier ? "/m/".concat(this.entity.identifier) : "/app/live/meetings/".concat(this.entity.uuid));
      var alertTitle = this.entity.identifier ? "".concat($t('meeting.meeting_code'), ": <strong class=\"ml-2\"> ").concat(this.entity.identifier, "</strong>") : null;
      Object(_core_utils_media__WEBPACK_IMPORTED_MODULE_8__["share"])({
        title: $t('meeting.user_inviting_msg', {
          attribute: this.user.name
        }),
        alertTitle: alertTitle,
        alertHtml: "".concat($t('misc.share_alert.text'), "<br>") + "<small class='text-muted'>".concat(url, "</small><br>"),
        url: url
      }, function () {
        _this.$gaEvent('engagement', 'shareURL', 'Copied');

        _this.$toasted.success($t('meeting.url_has_been_copied'), _this.$toastConfig);
      });
    },
    // query / countdown / timer methods
    isStartDateTimeInFuture: function isStartDateTimeInFuture() {
      return this.entity && this.entity.startDateTime && moment(this.entity.startDateTime, this.vars.serverDateTimeFormat).isAfter(moment());
    },
    startCountDown: function startCountDown() {
      var _this2 = this;

      if (window.countdownInterval) {
        clearInterval(window.countdownInterval);
      }

      window.countdownInterval = window.setInterval(function () {
        if (_this2.isStartDateTimeInFuture()) {
          _this2.showFlipClock = true;
        } else {
          if (_this2.showFlipClock && !document.hasFocus()) {
            Object(_core_utils_media__WEBPACK_IMPORTED_MODULE_8__["playIncomingMessage"])();
          }

          _this2.showFlipClock = false;
        }
      }, 1000);
    },
    startRecording: function startRecording() {
      var _this3 = this;

      this.$gaEvent('engagement', 'startRecording');
      this.pageConfigs.recording = true;
      this.recorded = false;
      var recorderInstance = this.rtcmConnection.recorder;
      recorderInstance.startRecording();
      var recordingStartedAt = new Date().getTime();

      if (window.recordingDurationInterval) {
        this.recordingDuration = null;
        clearInterval(window.recordingDurationInterval);
      }

      window.recordingDurationInterval = window.setInterval(function () {
        _this3.recordingDuration = Object(_core_utils__WEBPACK_IMPORTED_MODULE_9__["calculateTimeDuration"])((new Date().getTime() - recordingStartedAt) / 1000);
      }, 1000);
      var internalRecorder = recorderInstance.getInternalRecorder();
      this.rtcmConnection.streamEvents.selectAll().forEach(function (streamEvent) {
        if (streamEvent.type !== 'local') {
          internalRecorder.addStreams([streamEvent.stream]);
        }
      });
    },
    stopRecording: function stopRecording() {
      var _this4 = this;

      this.$gaEvent('engagement', 'stopRecording');
      this.pageConfigs.recording = false;
      this.recorded = true;
      var recorderInstance = this.rtcmConnection.recorder;

      if (!recorderInstance) {
        return alert('No recorder found!');
      }

      recorderInstance.stopRecording(function () {
        if (window.recordingDurationInterval) {
          _this4.recordingDuration = null;
          clearInterval(window.recordingDurationInterval);
        }

        recordrtc__WEBPACK_IMPORTED_MODULE_3__["getSeekableBlob"](recorderInstance.getBlob(), function (seekableBlob) {
          var recordedVideo = URL.createObjectURL(seekableBlob);
          var downloadLinkBtn = document.createElement("a");
          recorderInstance.destroy();
          recorderInstance = null;
          recordrtc__WEBPACK_IMPORTED_MODULE_3__["invokeSaveAsDialog"](seekableBlob, "meeting_record_" + moment().format(_this4.vars.serverDateTimeFormat) + ".webm");
          downloadLinkBtn.style.display = "none";
          downloadLinkBtn.href = recordedVideo;
          downloadLinkBtn.download = "meeting_record_" + moment().format(_this4.vars.serverDateTimeFormat) + ".webm";
          document.body.appendChild(downloadLinkBtn); // downloadLinkBtn.click()

          setTimeout(function () {
            document.body.removeChild(downloadLinkBtn);
            window.URL.revokeObjectURL(downloadLinkBtn);
            _this4.recorded = false;
          }, 100);
        });
      });
    },
    // toggle methods
    toggleHandUp: function toggleHandUp() {
      var _this5 = this;

      this.$gaEvent('engagement', 'toggleHandUp');

      if (window.lowerHandTimer) {
        clearTimeout(window.lowerHandTimer);
      }

      var found = this.$refs.videos.find(function (video) {
        return video.id === _this5.localVideo.id;
      });
      var videoIndex = this.videoList.findIndex(function (video) {
        return video.id === _this5.localVideo.id;
      });

      if (found && found.srcObject) {
        var stream = found.srcObject;

        if (this.isHandUp) {
          this.isHandUp = false;
          this.videoList[videoIndex].isHandUp = false;
          this.rtcmConnection.socket.emit('remoteHandToggled', {
            isHandUp: false,
            streamid: stream.streamid
          });
        } else {
          this.isHandUp = true;
          this.videoList[videoIndex].isHandUp = true;
          this.rtcmConnection.socket.emit('remoteHandToggled', {
            isHandUp: true,
            streamid: stream.streamid
          });
          window.lowerHandTimer = setTimeout(function () {
            _this5.toggleHandUp();
          }, 30000);
        }

        this.rtcmConnection.extra.isHandUp = this.isHandUp;
        this.rtcmConnection.updateExtraData();
      } else {
        this.localVideo = null;
      }
    },
    toggleAudio: function toggleAudio() {
      var _this6 = this;

      var found = this.$refs.videos.find(function (video) {
        return video.id === _this6.localVideo.id;
      });
      var videoIndex = this.videoList.findIndex(function (video) {
        return video.id === _this6.localVideo.id;
      });

      if (found && found.srcObject) {
        var stream = found.srcObject;
        var tracks = stream.getAudioTracks();
        tracks.forEach(function (track) {
          if (_this6.pageConfigs.enableAudio) {
            _this6.pageConfigs.enableAudio = false;
            track.enabled = false;
            _this6.videoList[videoIndex].audioMuted = true;

            _this6.rtcmConnection.socket.emit('remoteMutedUnmuted', {
              audioEnabled: false,
              streamid: stream.streamid
            });
          } else {
            _this6.pageConfigs.enableAudio = true;
            track.enabled = true;
            _this6.videoList[videoIndex].audioMuted = true;

            _this6.rtcmConnection.socket.emit('remoteMutedUnmuted', {
              audioEnabled: true,
              streamid: stream.streamid
            });
          }
        });
        this.rtcmConnection.extra.audioMuted = !this.pageConfigs.enableAudio;
        this.rtcmConnection.updateExtraData();
      } else {
        this.localVideo = null;
      }
    },
    toggleVideo: function toggleVideo() {
      var _this7 = this;

      var found = this.$refs.videos.find(function (video) {
        return video.id === _this7.localVideo.id;
      });
      var videoIndex = this.videoList.findIndex(function (video) {
        return video.id === _this7.localVideo.id;
      });

      if (found && found.srcObject) {
        var stream = found.srcObject;
        var tracks = stream.getVideoTracks();
        tracks.forEach(function (track) {
          if (_this7.pageConfigs.enableVideo) {
            _this7.pageConfigs.enableVideo = false;
            track.enabled = false;
            _this7.videoList[videoIndex].videoMuted = true;

            _this7.rtcmConnection.socket.emit('remoteMutedUnmuted', {
              videoEnabled: false,
              streamid: stream.streamid
            });
          } else {
            _this7.pageConfigs.enableVideo = true;
            track.enabled = true;
            _this7.videoList[videoIndex].videoMuted = false;

            _this7.rtcmConnection.socket.emit('remoteMutedUnmuted', {
              videoEnabled: true,
              streamid: stream.streamid
            });
          }
        });
        this.rtcmConnection.extra.videoMuted = !this.pageConfigs.enableVideo;
        this.rtcmConnection.updateExtraData();
      } else {
        this.localVideo = null;
      }
    },
    toggleRemoteAudio: function toggleRemoteAudio(videoItem, itemIndex) {
      if (videoItem.muted) {
        this.videoList[itemIndex].muted = false;
        this.$refs.videos[itemIndex].muted = false;
      } else {
        this.videoList[itemIndex].muted = true;
        this.$refs.videos[itemIndex].muted = true;
      } // if(videoItem.streamUserId) {
      //     const streamByUserId = this.rtcmConnection.streamEvents.selectFirst({ userid: videoItem.streamUserId }).stream
      //     if(videoItem.muted) {
      //         streamByUserId.unmute('audio')
      //     } else {
      //         streamByUserId.mute('audio')
      //     }
      // }

    },
    toggleEleFullScreen: function toggleEleFullScreen(videoItem, itemIndex) {
      var _this8 = this;

      var targetParentEl = this.$refs['videoListEle'];
      this.fullScreenItemId = videoItem.id;
      this.$fullscreen.toggle(targetParentEl, {
        wrap: false,
        callback: function callback(fullscreen) {
          _this8.fullScreenInOn = fullscreen;
        }
      });
    },
    toggleFullScreen: function toggleFullScreen(to) {
      if (screenfull__WEBPACK_IMPORTED_MODULE_6___default.a.isEnabled) {
        if (to) {
          screenfull__WEBPACK_IMPORTED_MODULE_6___default.a.request();
        } else {
          screenfull__WEBPACK_IMPORTED_MODULE_6___default.a.exit();
        }
      }
    },
    toggleFooterAutoHide: function toggleFooterAutoHide() {
      this.pageConfigs.footerAutoHide = !this.pageConfigs.footerAutoHide;
      this.$gaEvent('engagement', 'toggleFooterAutoHide');
    },
    toggleLayout: function toggleLayout(layout) {
      this.pageConfigs.layout = layout ? layout : this.pageConfigs.layout;
      this.$gaEvent('engagement', 'toggleLayout', layout);
    },
    toggleFileSharing: function toggleFileSharing() {
      this.showFileSharing = !this.showFileSharing;
      this.$gaEvent('engagement', 'toggleFileSharing');
    },
    toggleAgenda: function toggleAgenda() {
      this.showAgenda = !this.showAgenda;
      this.$gaEvent('engagement', 'toggleAgenda');
    },
    changeFocus: function changeFocus(item) {
      if (item.local) {
        return;
      }

      this.videoList.forEach(function (v) {
        v.maximized = v.id === item.id;
      });
    },
    kickRemoteUser: function kickRemoteUser(item, itemIndex) {
      var _this9 = this;

      formUtil.confirmAction().then(function (result) {
        if (result.value) {
          var found = _this9.$refs.videos.find(function (video) {
            return video.id === item.id;
          });

          if (found && found.srcObject) {
            var stream = found.srcObject;

            _this9.rtcmConnection.removeStream(stream.streamid);

            window.meetingChannel.whisper('BanAttendee', item);

            _this9.Custom({
              url: "/".concat(_this9.initUrl, "/").concat(_this9.uuid, "/invitees/").concat(item.extra.uuid, "/block"),
              method: 'post'
            }).then(function (response) {
              _this9.$toasted.success(response.message, _this9.$toastConfig);
            })["catch"](function (error) {
              formUtil.handleErrors(error);
            });
          }
        } else {
          result.dismiss === _core_configs_sweet_alert__WEBPACK_IMPORTED_MODULE_13__["default"].DismissReason.cancel;
        }
      });
    },
    switchCamera: function switchCamera() {
      this.mediaConfigurations.facingMode = this.mediaConfigurations.facingMode === 'user' ? 'environment' : 'user';
      this.applyMediaConstraints();
    },
    // channel event callback methods
    afterJoiningChannel: function afterJoiningChannel(members) {
      this.membersLive = members;
    },
    newMemberJoining: function newMemberJoining(member) {
      this.membersLive.push(member);
    },
    memberLeaving: function memberLeaving(member) {
      this.membersLive = this.membersLive.filter(function (u) {
        return u.uuid !== member.uuid;
      });
    },
    meetingStatusChanged: function meetingStatusChanged(e) {
      if (e.uuid === this.entity.uuid) {
        if (this.entity.status === e.status && !e.delayed) {
          return;
        }

        this.entity.status = e.status;
        this.entity.startDateTime = e.startDateTime;
        var meetingStatus = e.status;

        if (meetingStatus === 'scheduled' && e.delayed) {
          meetingStatus = 'delayed';
        }

        var statusUpdateMessages = {
          'live': 'meeting.is_live_now',
          'delayed': 'meeting.meeting_delayed',
          'cancelled': 'meeting.meeting_cancelled',
          'ended': 'meeting.meeting_ended'
        };
        this.$toasted.success($t(statusUpdateMessages[meetingStatus]), this.$toastConfig);
      }
    },
    gotNewMessage: function gotNewMessage() {
      if (!(this.configs.chat && this.configs.chat.enabled)) {
        return;
      }

      if (!this.pageConfigs.showChat || this.fullScreenInOn) {
        this.newMessages = true;
        Object(_core_utils_media__WEBPACK_IMPORTED_MODULE_8__["playIncomingMessage"])();
      }
    },
    meetingRoomCreated: function meetingRoomCreated(e) {
      var _this10 = this;

      this.entity.roomId = e.roomId;
      this.initMediaAndRtcmConnection();
      this.rtcmConnection.checkPresence(this.entity.roomId, function (isRoomExist, roomid) {
        _this10.roomIdAlive = !!isRoomExist;
      });
    },
    banAttendee: function banAttendee(e) {
      var found = this.$refs.videos.find(function (video) {
        return video.id === e.id;
      });

      if (found && found.srcObject) {
        var stream = found.srcObject;
        this.rtcmConnection.removeStream(stream.streamid);
      }

      if (e.uuid === this.userUuid) {
        this.closeConnectionAndStream();
        this.getInitialData();
      }

      this.$toasted.info($t('meeting.ban_notification'), this.$toastConfig.info);
    },
    streamRemoved: function streamRemoved(e) {
      if (this.rtcmConnection && e.id) {
        this.rtcmConnection.removeStream(e.id);
      }

      this.rtcmOnStreamEnded(e);
    },
    meetingEnded: function meetingEnded(e) {
      this.closeConnectionAndStream();
      this.getInitialData();
    },
    // channel action methods
    joinChannel: function joinChannel() {
      window.meetingChannel = window.Echo.join("Meeting.".concat(this.uuid));
      window.meetingChannel.here(this.afterJoiningChannel).joining(this.newMemberJoining).leaving(this.memberLeaving).listen('MeetingStatusChanged', this.meetingStatusChanged).listen('NewMessage', this.gotNewMessage).listenForWhisper('MeetingRoomCreated', this.meetingRoomCreated).listenForWhisper('RemovedStream', this.streamRemoved).listenForWhisper('BanAttendee', this.banAttendee).listenForWhisper('MeetingEnded', this.meetingEnded);
    },
    // rtcm event callback methods
    rtcmOnStream: function rtcmOnStream(stream) {
      var _this11 = this;

      // const track = stream.stream.getVideoTracks()[0]
      // const constraints = track.getConstraints()
      // console.log('Result constraints: ' + JSON.stringify(constraints))
      this.logEvent('On Stream: ', stream);
      var found = this.videoList.find(function (video) {
        return video.id === stream.streamid;
      });
      var streamInstance = stream.stream.idInstance ? JSON.parse(stream.stream.idInstance) : stream.stream;

      if (streamInstance.isScreen && stream.type === 'local') {
        this.localScreenStreamid = stream.streamid;
        var tracks = stream.stream.getTracks();
        tracks.forEach(function (track) {
          track.addEventListener('ended', _this11.stopSharingScreen);
        });
      }

      if (this.videoList.length > 1) {
        this.recheckLiveParticipants(null);
      }

      if (found === undefined) {
        var video = _objectSpread(_objectSpread({
          id: stream.streamid,
          streamUserId: stream.userid,
          muted: stream.type === 'local'
        }, stream.extra), {}, {
          extra: stream.extra,
          maximized: stream.type === 'local' && !streamInstance.isScreen,
          local: stream.type === 'local',
          screen: this.localScreenStreamid === stream.streamid,
          status: true,
          hasAudio: streamInstance.isAudio || streamInstance.audio,
          hasVideo: streamInstance.isVideo || streamInstance.video || streamInstance.isScreen || streamInstance.screen
        });

        if (streamInstance.isScreen) {
          video.videoMuted = false;
        } // let foundUserIndex = this.videoList.findIndex(item => item.uuid === stream.extra.uuid)
        // if (foundUserIndex >= 0) {
        //     const foundUserVideo = this.videoList[foundUserIndex]
        //     if (!foundUserVideo.status) {
        //         let newList = this.videoList.map(item => item.uuid !== foundUserVideo.uuid)
        //         this.videoList = newList
        //     }
        // }


        this.videoList.push(video);

        if (stream.type === 'local' && this.localScreenStreamid !== stream.streamid) {
          this.localVideo = video;
        }
      }

      this.autoSetVideoMaximized();

      if (this.pageConfigs.enableRecording) {
        var recorderInstance = this.rtcmConnection.recorder;

        if (!recorderInstance) {
          recorderInstance = recordrtc__WEBPACK_IMPORTED_MODULE_3__([stream.stream], {
            type: 'video',
            mimeType: "video/webm",
            timeSlice: 1000,
            // pass this parameter 
            disableLogs: false
          });
          this.rtcmConnection.recorder = recorderInstance;
        } else {
          var internalRecorder = recorderInstance.getInternalRecorder();

          if (this.pageConfigs.recording && internalRecorder) {
            internalRecorder.addStreams([stream.stream]);
          }
        }

        if (!this.rtcmConnection.recorder.streams) {
          this.rtcmConnection.recorder.streams = [];
        }

        this.rtcmConnection.recorder.streams.push(stream.stream);
      }

      setTimeout(function () {
        for (var i = 0, len = _this11.$refs.videos.length; i < len; i++) {
          if (_this11.$refs.videos[i].id === stream.streamid) {
            _this11.$refs.videos[i].srcObject = stream.stream;
            break;
          }
        } // this.rtcmConnection.streamEvents.selectAll({
        //     isScreen: true
        // }).forEach(function(screenEvent) {
        //     this.videoList.forEach((item, index) => {
        //         if (item.id !== screenEvent.stream.streamid) {
        //             this.videoList[index].screen = true
        //         } else {
        //             this.videoList[index].screen = false
        //         }
        //     })
        // })

      }, 500);
      this.isLoading = false;
    },
    rtcmOnStreamEnded: function rtcmOnStreamEnded(stream) {
      this.recheckLiveParticipants(stream);
      this.autoSetVideoMaximized();
    },
    rtcmOnMediaError: function rtcmOnMediaError(error) {
      var _this12 = this;

      this.isLoading = false;

      if (window.currentUserMediaRequest) {
        window.currentUserMediaRequest.mutex = false;
      }

      var msgObj = Object(_core_utils_media__WEBPACK_IMPORTED_MODULE_8__["showMediaPermissionError"])(error);
      msgObj.alert.then(function (result) {
        if (error.name === 'NotFoundError' || error.name === 'NotReadableError' || error.name === 'OverconstrainedError') {
          _this12.applyMediaConstraints(true);

          _this12.$toasted.info($t('meeting.media_devices.setup_reset_try_again'), _this12.$toastConfig.info);
        } else if (error.name === 'NotAllowedError' || error.name === 'PermissionDeniedError') {
          _this12.permissionRejected = true;
        }

        if (!result.value) {
          _this12.isLoading = false;
          result.dismiss === _core_configs_sweet_alert__WEBPACK_IMPORTED_MODULE_13__["default"].DismissReason.cancel;
        }
      }); // this.meetingAction('leave', { error: { name: error.name, title: msg.title } }, { alert: false })
    },
    rtcmOnMute: function rtcmOnMute(stream) {
      var videoIndex = this.videoList.findIndex(function (v) {
        return v.id === stream.streamid;
      });
      var videoEle = this.$refs.videos.find(function (video) {
        return video.id === stream.streamid;
      });

      if (stream.muteType === 'video') {
        this.videoList[videoIndex].videoMuted = true;
      } else if (stream.muteType === 'audio') {
        this.videoList[videoIndex].muted = true;
      } else {
        this.videoList[videoIndex].videoMuted = true;
        this.videoList[videoIndex].muted = true;
        videoEle.srcObject = null;
      }
    },
    rtcmOnUnmute: function rtcmOnUnmute(stream) {
      var videoIndex = this.videoList.findIndex(function (v) {
        return v.id === stream.streamid;
      });
      var videoEle = this.$refs.videos.find(function (video) {
        return video.id === stream.streamid;
      });

      if (stream.unmuteType === 'video') {
        this.videoList[videoIndex].videoMuted = false;
      } else if (stream.unmuteType === 'audio') {
        this.videoList[videoIndex].muted = false;
      } else {
        this.videoList[videoIndex].videoMuted = false;
        this.videoList[videoIndex].muted = false;
        videoEle.srcObject = stream;
      }
    },
    rtcmOnRemoteMuteUnmute: function rtcmOnRemoteMuteUnmute(data) {
      var videoIndex = this.videoList.findIndex(function (video) {
        return video.id === data.streamid;
      });
      var stream = this.rtcmConnection.streamEvents[data.streamid].stream;

      if (data.hasOwnProperty('audioEnabled')) {
        if (data.audioEnabled) {
          this.videoList[videoIndex].audioMuted = false;
        } else {
          this.videoList[videoIndex].audioMuted = true;
        }
      } else if (data.hasOwnProperty('videoEnabled')) {
        // const videoEle = this.$refs.videos.find(video => {
        //     return video.id === data.streamid
        // })
        if (data.videoEnabled) {
          this.videoList[videoIndex].videoMuted = false;
        } else {
          this.videoList[videoIndex].videoMuted = true;
        }
      }
    },
    rtcmOnRemoteHandToggled: function rtcmOnRemoteHandToggled(data) {
      if (!(data && data.streamid && this.rtcmConnection.streamEvents[data.streamid])) {
        return;
      }

      var videoIndex = this.videoList.findIndex(function (video) {
        return video.id === data.streamid;
      });
      var stream = this.rtcmConnection.streamEvents[data.streamid].stream;

      if (data.hasOwnProperty('isHandUp')) {
        if (data.isHandUp) {
          this.videoList[videoIndex].isHandUp = true;
          this.$toasted.info($t('meeting.handup_notification', {
            attribute: this.videoList[videoIndex].name
          }), this.$toastConfig.info);
        } else {
          this.videoList[videoIndex].isHandUp = false;
        }
      }
    },
    rtcmOnUserIdAlreadyTaken: function rtcmOnUserIdAlreadyTaken(useridAlreadyTaken, yourNewUserId) {
      this.rtcmConnection.userid = yourNewUserId;
    },
    // rtc action methods
    initMediaAndRtcmConnection: function initMediaAndRtcmConnection() {
      var _this13 = this;

      if (!this.rtcmConnection) {
        this.rtcmConnection = new rtcmulticonnection__WEBPACK_IMPORTED_MODULE_2___default.a();
        this.rtcmConnection.socketURL = window.atob(this.socketURL);

        if (this.configs.signal && this.configs.signal.url) {
          this.rtcmConnection.socketURL = this.configs.signal.url;
        }

        this.rtcmConnection.autoCreateMediaElement = false; // this.rtcmConnection.autoCloseEntireSession = true // set this line to close room as soon as room creator leaves

        this.rtcmConnection.enableLogs = false;
        this.rtcmConnection.onstream = this.rtcmOnStream;
        this.rtcmConnection.onstreamended = this.rtcmOnStreamEnded;
        this.rtcmConnection.onmute = this.rtcmOnMute;
        this.rtcmConnection.onunmute = this.rtcmOnUnmute;
        this.rtcmConnection.onMediaError = this.rtcmOnMediaError;
        this.rtcmConnection.onUserIdAlreadyTaken = this.rtcmOnUserIdAlreadyTaken;
        this.rtcmConnection.setCustomSocketEvent('remoteMutedUnmuted');
        this.rtcmConnection.setCustomSocketEvent('remoteHandToggled');
        this.fileSharingConnection = this.rtcmConnection;
      }

      this.getMediaConstraints();
      var bwHandler = this.rtcmConnection.BandwidthHandler;
      this.rtcmConnection.bandwidth = _objectSpread({}, this.mediaConfigurations.bandwidth);

      this.rtcmConnection.processSdp = function (sdp) {
        if (!_this13.rtcmConnection) {
          return sdp;
        }

        sdp = bwHandler.setApplicationSpecificBandwidth(sdp, _this13.rtcmConnection.bandwidth, !!_this13.rtcmConnection.session.screen);
        sdp = bwHandler.setVideoBitrates(sdp, {
          min: _this13.rtcmConnection.bandwidth.video,
          max: _this13.rtcmConnection.bandwidth.video
        });
        sdp = bwHandler.setOpusAttributes(sdp);
        sdp = bwHandler.setOpusAttributes(sdp, {
          'stereo': 1,
          //'sprop-stereo': 1,
          'maxaveragebitrate': _this13.rtcmConnection.bandwidth.audio * 1000 * 8,
          'maxplaybackrate': _this13.rtcmConnection.bandwidth.audio * 1000 * 8,
          //'cbr': 1,
          //'useinbandfec': 1,
          // 'usedtx': 1,
          'maxptime': 3
        });
        return sdp;
      };

      this.rtcmConnection.iceServers = [{
        urls: ["stun.l.google.com:19302", "stun1.l.google.com:19302", "stun2.l.google.com:19302", "stun3.l.google.com:19302", "stun4.l.google.com:19302"]
      }];

      if (this.configs.iceServers.length) {
        this.rtcmConnection.iceServers = _toConsumableArray(this.configs.iceServers);
      }

      this.rtcmConnection.session = _objectSpread({}, this.meetingRulesHost.session);
      this.rtcmConnection.sdpConstraints.mandatory = _objectSpread({}, this.meetingRulesHost.mandatory);
      this.rtcmConnection.mediaConstraints = {
        video: this.meetingRulesHost.mediaConstraints.video ? this.videoConstraints : false,
        audio: this.meetingRulesHost.mediaConstraints.audio ? this.audioConstraints : false,
        screen: this.meetingRulesHost.mediaConstraints.screen
      };
    },
    getMediaConstraints: function getMediaConstraints() {
      var _this14 = this;

      var setDefaults = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      if (setDefaults) {
        this.audioConstraints = {
          "sampleSize": 16,
          "channelCount": 2,
          "echoCancellation": false
        };
        this.videoConstraints = {
          width: {
            min: 426,
            ideal: 1280,
            max: 4096
          },
          height: {
            min: 240,
            ideal: 720,
            max: 2160
          }
        };
        return;
      }

      if (this.mediaConfigurations.selectedAudioInput && this.mediaConfigurations.selectedAudioInput.uuid) {
        this.audioConstraints = {
          deviceId: {
            ideal: this.mediaConfigurations.selectedAudioInput.uuid
          }
        };
      } else {
        this.audioConstraints = true;
      }

      if (this.mediaConfigurations.selectedVideoInput && this.mediaConfigurations.selectedVideoInput.uuid) {
        this.videoConstraints = {
          deviceId: {
            ideal: this.mediaConfigurations.selectedVideoInput.uuid
          }
        };
      } else {
        this.videoConstraints = {};
      }

      if (this.mediaConfigurations.facingMode !== 'Auto') {
        this.videoConstraints.facingMode = this.mediaConfigurations.facingMode;
      } else {
        if (this.videoConstraints.hasOwnProperty('facingMode')) {
          delete this.videoConstraints.facingMode;
        }
      }

      if (this.mediaConfigurations.frameRate !== 'Auto') {
        this.videoConstraints.frameRate = this.mediaConfigurations.frameRate;
      } else {
        if (this.videoConstraints.hasOwnProperty('frameRate')) {
          delete this.videoConstraints.frameRate;
        }
      }

      var selectedResolution = this.mediaDevices.resolutions.find(function (r) {
        return r.label === _this14.mediaConfigurations.selectedResolution;
      });

      if (selectedResolution && selectedResolution.label !== 'Auto') {
        this.videoConstraints.width = selectedResolution.constraints.width;
        this.videoConstraints.height = selectedResolution.constraints.height;
      } else {
        if (this.videoConstraints.hasOwnProperty('width')) {
          delete this.videoConstraints.width;
        }

        if (this.videoConstraints.hasOwnProperty('height')) {
          delete this.videoConstraints.height;
        }
      }
    },
    applyMediaConstraints: function applyMediaConstraints() {
      var setDefaults = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      if (!(this.rtcmConnection && this.localVideo)) {
        return;
      }

      this.getMediaConstraints(setDefaults);
      this.rtcmConnection.mediaConstraints = {
        video: this.meetingRulesHost.mediaConstraints.video ? this.videoConstraints : false,
        audio: this.meetingRulesHost.mediaConstraints.audio ? this.audioConstraints : false,
        screen: this.meetingRulesHost.mediaConstraints.screen
      };

      if (!MediaStreamTrack || !MediaStreamTrack.prototype.applyConstraints) {// TODO: code pending for chrome
      } else {
        this.rtcmConnection.applyConstraints(this.rtcmConnection.mediaConstraints);
      }
    },
    closeConnectionAndStream: function closeConnectionAndStream() {
      var _this15 = this;

      if (this.rtcmConnection) {
        this.rtcmConnection.attachStreams.forEach(function (localStream) {
          localStream.stop();
        });

        if (this.localVideo) {
          // this.rtcmConnection.removeStream(this.localVideo.id)
          window.meetingChannel.whisper('RemovedStream', this.localVideo);
        }

        this.rtcmConnection.getAllParticipants().forEach(function (pid) {
          _this15.rtcmConnection.disconnectWith(pid);
        });
        this.rtcmConnection.leave();
        this.rtcmConnection.closeSocket();
        window.setTimeout(function () {
          _this15.localVideo = null;
          _this15.localScreenStreamid = null;
          _this15.rtcmConnection.recorder = null;
          _this15.rtcmConnection = null;
        }, 500);
      }

      this.videoList = [];
    },
    recheckLiveParticipants: function recheckLiveParticipants(stream) {
      var _this16 = this;

      var newList = [];
      var membersWhoLeft = [];
      var liveParticipants = this.rtcmConnection ? this.rtcmConnection.getAllParticipants() : [];
      this.videoList.forEach(function (item, index) {
        var userIndex = liveParticipants.findIndex(function (m) {
          return m === item.streamUserId;
        });

        if ((!stream || stream && item.id !== stream.streamid) && (item.local || !item.local && userIndex !== -1)) {
          newList.push(item);
        } else {
          item.status = false;
          newList.push(item);
        }
      });
      this.videoList = newList;
      setTimeout(function () {
        _this16.videoList = _this16.videoList.filter(function (v) {
          return v.status;
        });

        _this16.autoSetVideoMaximized();
      }, 3000);
    },
    autoSetVideoMaximized: function autoSetVideoMaximized() {
      if (this.videoList.length > 1) {
        var maximizedRemoteVideoIndex = this.videoList.findIndex(function (v) {
          return !v.local && v.maximized && v.status;
        });

        if (maximizedRemoteVideoIndex === -1) {
          var remoteVideoIndex = this.videoList.findIndex(function (v) {
            return !v.local;
          });

          if (remoteVideoIndex !== -1) {
            this.videoList = this.videoList.map(function (v, index) {
              v.maximized = false;

              if (index === remoteVideoIndex) {
                v.maximized = true;
              }

              return v;
            });
          }
        }
      } else if (this.videoList.length) {
        this.videoList[0].maximized = true;
      }
    },
    openRoom: function openRoom(meetingRoomId) {
      var _this17 = this;

      this.rtcmConnection.session = _objectSpread({}, this.meetingRulesHost.session);
      this.getMediaConstraints();
      this.rtcmConnection.mediaConstraints = {
        video: this.meetingRulesHost.mediaConstraints.video ? this.videoConstraints : false,
        audio: this.meetingRulesHost.mediaConstraints.audio ? this.audioConstraints : false,
        screen: this.meetingRulesHost.mediaConstraints.screen
      };
      this.rtcmConnection.sdpConstraints.mandatory = _objectSpread({}, this.meetingRulesHost.mandatory); // console.log('mediaConstraints')
      // console.log(this.rtcmConnection.mediaConstraints)
      // console.log('sdpConstraints')
      // console.log(this.rtcmConnection.sdpConstraints)

      this.rtcmConnection.open(meetingRoomId, function (isRoomOpened, roomid, error) {
        _this17.rtcmConnection.socket.on('remoteMutedUnmuted', _this17.rtcmOnRemoteMuteUnmute);

        _this17.rtcmConnection.socket.on('remoteHandToggled', _this17.rtcmOnRemoteHandToggled);

        _this17.logEvent('Room Opened: ', roomid);

        _this17.isLoading = false;

        if (error) {
          formUtil.handleErrors(error);
        } else if (isRoomOpened === true) {
          _this17.updatePageConfigs(true);

          window.meetingChannel.whisper('MeetingRoomCreated', {
            roomId: meetingRoomId
          });

          _this17.$toasted.success($t('meeting.meeting_created'), _this17.$toastConfig);
        }
      });
    },
    joinRoom: function joinRoom(meetingRoomId) {
      var _this18 = this;

      this.rtcmConnection.session = _objectSpread({}, this.meetingRulesGuest.session);
      this.getMediaConstraints();
      this.rtcmConnection.mediaConstraints = {
        video: this.meetingRulesGuest.mediaConstraints.video ? this.videoConstraints : false,
        audio: this.meetingRulesGuest.mediaConstraints.audio ? this.audioConstraints : false,
        screen: this.meetingRulesGuest.mediaConstraints.screen
      };
      this.rtcmConnection.sdpConstraints.mandatory = _objectSpread({}, this.meetingRulesGuest.mandatory); // console.log('mediaConstraints')
      // console.log(this.rtcmConnection.mediaConstraints)
      // console.log('sdpConstraints')
      // console.log(this.rtcmConnection.sdpConstraints)

      this.rtcmConnection.join(meetingRoomId, function (isJoined, roomid, error) {
        _this18.rtcmConnection.socket.on('remoteMutedUnmuted', _this18.rtcmOnRemoteMuteUnmute);

        _this18.rtcmConnection.socket.on('remoteHandToggled', _this18.rtcmOnRemoteHandToggled);

        _this18.logEvent('Room Joined: ', roomid);

        _this18.isLoading = false;

        if (isJoined === false || error) {
          formUtil.handleErrors(error);
        } else {
          _this18.updatePageConfigs();

          _this18.$toasted.success($t('meeting.meeting_joined'), _this18.$toastConfig);
        }
      });
    },
    shareScreen: function shareScreen() {
      try {
        this.rtcmConnection.addStream({
          screen: true
        });
      } catch (e) {}
    },
    stopSharingScreen: function stopSharingScreen() {
      var _this19 = this;

      this.isLoading = true;
      var found = this.$refs.videos.find(function (video) {
        return video.id === _this19.localScreenStreamid;
      });

      if (found && found.srcObject) {
        var tracks = found.srcObject.getTracks();
        tracks.forEach(function (track) {
          track.removeEventListener('ended', _this19.stopSharingScreen);
          track.enabled = false;
          track.stop();
        });
        this.rtcmConnection.removeStream(this.localScreenStreamid);
        window.meetingChannel.whisper('RemovedStream', found.srcObject); // this.videoList = this.videoList.filter(video => video.id !== this.localScreenStreamid)

        this.localScreenStreamid = null;
        this.isLoading = false;
      } else {
        this.localScreenStreamid = null;
        this.isLoading = false;
      }
    },
    getOnline: function getOnline() {
      var _this20 = this;

      this.isLoading = true;
      this.showAgenda = false;
      var meetingStatusEalier = this.entity.status;
      this.$gaEvent('engagement', 'meeting_getOnline');
      this.initMediaAndRtcmConnection();
      this.Custom({
        url: "/".concat(this.initUrl, "/").concat(this.uuid, "/join"),
        method: 'post'
      }).then(function (response) {
        _this20.meetingRoomId = response.meeting.roomId;
        response.meeting = _this20.updateMeetingTimezone(response.meeting);
        _this20.entity = response.meeting;
        _this20.rtcmConnection.extra = {
          username: _this20.user.username,
          name: _this20.user.name,
          uuid: _this20.user.uuid,
          image: _this20.profile.image,
          audioMuted: !_this20.pageConfigs.enableAudio,
          videoMuted: !_this20.pageConfigs.enableVideo,
          isHandUp: _this20.isHandUp
        };

        _this20.rtcmConnection.checkPresence(_this20.meetingRoomId, function (isRoomExist, roomid) {
          if (isRoomExist === true) {
            _this20.joinRoom(_this20.meetingRoomId);
          } else {
            if (_this20.entity.canModerate) {
              _this20.openRoom(_this20.meetingRoomId);
            } else {
              _this20.isLoading = false;

              _this20.$toasted.error($t('meeting.room_not_found'), _this20.$toastConfig.error);
            }
          }
        });
      })["catch"](function (error) {
        _this20.isLoading = false;
        formUtil.handleErrors(error);
      });
    },
    getOffline: function getOffline() {
      var _this21 = this;

      this.$gaEvent('engagement', 'meeting_getOffline');
      this.isLoading = true;
      this.meetingAction('leave', null, {
        alert: 'confirm',
        callback: function callback(e) {
          _this21.showAgenda = true;

          _this21.closeConnectionAndStream();
        }
      });
    },
    // meeting action methods
    meetingAction: function meetingAction(action) {
      var _this22 = this;

      var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var opts = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {
        alert: 'confirm'
      };
      var defaultOpts = {
        alert: 'confirm'
      };
      opts = Object.assign({}, defaultOpts, opts);

      var callApi = function callApi(dataToSend) {
        _this22.isLoading = true;
        data = dataToSend ? dataToSend : data;

        _this22.Custom({
          url: "/".concat(_this22.initUrl, "/").concat(_this22.uuid, "/").concat(action),
          method: 'post',
          data: data
        }).then(function (response) {
          response.meeting = _this22.updateMeetingTimezone(response.meeting);
          _this22.entity = response.meeting;

          _this22.$toasted.success(response.message, _this22.$toastConfig);

          if (opts.callback) {
            opts.callback(response);
          }

          _this22.isLoading = false;
        })["catch"](function (error) {
          _this22.isLoading = false;
          formUtil.handleErrors(error);
        });
      };

      if (!action) {
        this.isLoading = false;
        return;
      }

      if (opts.alert === 'confirm' || opts.alert === true) {
        formUtil.confirmAction().then(function (result) {
          if (result.value) {
            callApi();
          } else {
            _this22.isLoading = false;
            result.dismiss === _core_configs_sweet_alert__WEBPACK_IMPORTED_MODULE_13__["default"].DismissReason.cancel;
          }
        });
      } else if (opts.alert === 'input') {
        swtAlert.fire({
          title: opts.title,
          input: 'text',
          inputPlaceholder: opts.inputPlaceholder,
          showCancelButton: true,
          confirmButtonText: 'Proceed!',
          cancelButtonText: 'Go Back!'
        }).then(function (result) {
          if (result.value) {
            var toSend = {};
            toSend[opts.fieldName] = result.value;
            callApi(toSend);
          } else {
            _this22.isLoading = false;
            result.dismiss === _core_configs_sweet_alert__WEBPACK_IMPORTED_MODULE_13__["default"].DismissReason.cancel;
          }
        });
      } else {
        callApi();
      }
    },
    snoozeMeeting: function snoozeMeeting(period) {
      this.meetingAction('snooze', {
        period: period
      });
    },
    leaveMeeting: function leaveMeeting() {
      this.meetingAction('leave');
    },
    cancelMeeting: function cancelMeeting() {
      this.meetingAction('cancel', null, {
        alert: 'input',
        title: $t('meeting.reason_for_cancellation'),
        inputPlaceholder: $t('meeting.reason_for_cancellation'),
        fieldName: 'cancellationReason'
      });
    },
    endMeeting: function endMeeting() {
      var _this23 = this;

      this.isLoading = true;
      this.meetingAction('end', null, {
        callback: function callback(e) {
          _this23.showAgenda = true;

          _this23.closeConnectionAndStream();

          window.meetingChannel.whisper('MeetingEnded', {
            status: e.meeting.status
          });
        }
      });
    },
    // meeting related methods
    updateMeetingRules: function updateMeetingRules() {
      if (this.entity.type.uuid === 'video_conference') {
        this.meetingRulesHost = {
          session: {
            audio: true,
            video: true,
            screen: false,
            data: true,
            oneway: false
          },
          mediaConstraints: {
            audio: true,
            video: true,
            screen: false
          },
          mandatory: {
            OfferToReceiveAudio: true,
            OfferToReceiveVideo: true
          }
        };
        this.meetingRulesGuest = {
          session: {
            audio: true,
            video: true,
            screen: false,
            data: true,
            oneway: false
          },
          mediaConstraints: {
            audio: true,
            video: true,
            screen: false
          },
          mandatory: {
            OfferToReceiveAudio: true,
            OfferToReceiveVideo: true
          }
        };
      } else if (this.entity.type.uuid === 'audio_conference') {
        this.meetingRulesHost = {
          session: {
            audio: true,
            video: false,
            screen: false,
            data: true,
            oneway: false
          },
          mediaConstraints: {
            audio: true,
            video: false,
            screen: false
          },
          mandatory: {
            OfferToReceiveAudio: true,
            OfferToReceiveVideo: true
          }
        };
        this.meetingRulesGuest = {
          session: {
            audio: true,
            video: false,
            screen: false,
            data: true,
            oneway: false
          },
          mediaConstraints: {
            audio: true,
            video: false,
            screen: false
          },
          mandatory: {
            OfferToReceiveAudio: true,
            OfferToReceiveVideo: true
          }
        };
      } else if (this.entity.type.uuid === 'webinar') {
        this.meetingRulesHost = {
          session: {
            audio: true,
            video: true,
            screen: false,
            data: false,
            oneway: true
          },
          mediaConstraints: {
            audio: true,
            video: true,
            screen: false
          },
          mandatory: {
            OfferToReceiveAudio: false,
            OfferToReceiveVideo: false
          }
        };
        this.meetingRulesGuest = {
          session: {
            audio: true,
            video: true,
            screen: false,
            data: false,
            oneway: true
          },
          mediaConstraints: {
            audio: true,
            video: false,
            screen: false
          },
          mandatory: {
            OfferToReceiveAudio: true,
            OfferToReceiveVideo: true
          }
        };
      } else if (this.entity.type.uuid === 'live_class') {
        this.meetingRulesHost = {
          session: {
            audio: true,
            video: true,
            screen: false,
            data: true,
            oneway: false
          },
          mediaConstraints: {
            audio: true,
            video: true,
            screen: false
          },
          mandatory: {
            OfferToReceiveAudio: true,
            OfferToReceiveVideo: false
          }
        };
        this.meetingRulesGuest = {
          session: {
            audio: true,
            video: false,
            screen: false,
            data: true,
            oneway: false
          },
          mediaConstraints: {
            audio: true,
            video: false,
            screen: false
          },
          mandatory: {
            OfferToReceiveAudio: true,
            OfferToReceiveVideo: true
          }
        };
      } else if (this.entity.type.uuid === 'podcast') {
        this.meetingRulesHost = {
          session: {
            audio: true,
            video: false,
            screen: false,
            data: false,
            oneway: true
          },
          mediaConstraints: {
            audio: true,
            video: false,
            screen: false
          },
          mandatory: {
            OfferToReceiveAudio: false,
            OfferToReceiveVideo: false
          }
        };
        this.meetingRulesGuest = {
          session: {
            audio: true,
            video: false,
            screen: false,
            data: false,
            oneway: true
          },
          mediaConstraints: {
            audio: false,
            video: false,
            screen: false
          },
          mandatory: {
            OfferToReceiveAudio: true,
            OfferToReceiveVideo: false
          }
        };
      }
    },
    updateMeetingTimezone: function updateMeetingTimezone(meetingEntity) {
      var _this24 = this;

      var getTimezoneDateTime = function getTimezoneDateTime(dt) {
        return _core_filters_momentz__WEBPACK_IMPORTED_MODULE_11__["momentDateTimeTz"](dt, _this24.vars.serverDateTimeFormat);
      };

      meetingEntity.startDateTime = getTimezoneDateTime(meetingEntity.startDateTime);

      if (meetingEntity.plannedStartDateTime) {
        meetingEntity.plannedStartDateTime = getTimezoneDateTime(meetingEntity.plannedStartDateTime);
      }

      return meetingEntity;
    },
    // event callback methods
    beforeUnload: function beforeUnload(event) {
      if (this.localVideo) {
        window.meetingChannel.whisper('RemovedStream', this.localVideo);
      }
    },
    logEvent: function logEvent(msg, args) {
      var type = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'log';

      if (this.rtcmConnection.enableLogs) {
        if (type === 'log') {
          console.log(msg, args);
        } else if (type === 'error') {
          console.error(msg, args);
        } else if (type === 'debug') {
          console.debug(msg, args);
        }
      }
    },
    // devices modal methods
    toggleDevicesModal: function toggleDevicesModal() {
      this.showDevicesModal = true;
    },
    onDevicesModalOK: function onDevicesModalOK(e) {
      var _this25 = this;

      e.preventDefault();
      this.applyMediaConstraints();
      this.$nextTick(function () {
        _this25.$bvModal.hide('devicesModal');
      });
    },
    // page methods
    updatePageConfigs: function updatePageConfigs() {
      var _this26 = this;

      var isInitiator = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      var configOptions = {};

      if (this.entity.type.uuid === 'video_conference') {
        configOptions = {
          enableAudio: true,
          enableVideo: true,
          showEnableAudioBtn: true,
          showEnableVideoBtn: true
        };
      } else if (this.entity.type.uuid === 'audio_conference') {
        configOptions = {
          enableAudio: true,
          enableVideo: true,
          showEnableAudioBtn: true,
          showEnableVideoBtn: false
        };
      } else if (this.entity.type.uuid === 'webinar') {
        configOptions = isInitiator ? {
          enableAudio: true,
          enableVideo: true,
          showEnableAudioBtn: true,
          showEnableVideoBtn: true
        } : {
          enableAudio: true,
          enableVideo: true,
          showEnableAudioBtn: false,
          showEnableVideoBtn: false,
          enableScreenSharing: false,
          enableRecording: false
        };
      } else if (this.entity.type.uuid === 'live_class') {
        configOptions = isInitiator ? {
          enableAudio: true,
          enableVideo: true,
          showEnableAudioBtn: true,
          showEnableVideoBtn: true
        } : {
          enableAudio: true,
          enableVideo: true,
          showEnableAudioBtn: true,
          showEnableVideoBtn: false,
          enableScreenSharing: false,
          enableRecording: false
        };
      } else if (this.entity.type.uuid === 'podcast') {
        configOptions = isInitiator ? {
          enableAudio: true,
          enableVideo: false,
          showEnableAudioBtn: true,
          showEnableVideoBtn: false,
          enableScreenSharing: false,
          enableHandGesture: false
        } : {
          enableAudio: true,
          enableVideo: false,
          showEnableAudioBtn: false,
          showEnableVideoBtn: false,
          enableScreenSharing: false,
          enableRecording: false,
          enableHandGesture: false
        };
      }

      configOptions.objForEach(function (value, key) {
        return _this26.pageConfigs[key] = value;
      });
      this.rtcmConnection.extra.audioMuted = !this.pageConfigs.enableAudio;
      this.rtcmConnection.extra.videoMuted = !this.pageConfigs.enableVideo;
      this.rtcmConnection.extra.isHandUp = this.isHandUp;
      this.rtcmConnection.updateExtraData();
    },
    destroyPage: function destroyPage() {
      if (window.countdownInterval) {
        clearInterval(window.countdownInterval);
      }

      this.SetUiConfig({
        pageHeaderShow: true,
        pageFooterShow: true
      });
      this.closeConnectionAndStream();

      if (window.Echo) {
        if (window.meetingChannel) {
          window.meetingChannel.stopListening('MeetingStatusChanged');
          window.meetingChannel.stopListening('NewMessage');
        }

        window.Echo.leave("Meeting.".concat(this.uuid));
      }

      if (screenfull__WEBPACK_IMPORTED_MODULE_6___default.a.isEnabled) {
        screenfull__WEBPACK_IMPORTED_MODULE_6___default.a.off('change');
        screenfull__WEBPACK_IMPORTED_MODULE_6___default.a.off('error');
      }
    },
    getInitialData: function getInitialData() {
      var _this27 = this;

      this.isLoading = true;

      if (!window.Echo) {
        this.$toasted.error($t('config.pusher.credential_required'), this.$toastConfig.error);
        this.$router.push({
          name: this.fallBackRoute
        });
        return;
      }

      return this.Get({
        uuid: this.uuid
      }).then(function (response) {
        if (response.isInstantMeeting) {
          _this27.pageConfigs.hasAgenda = false;
        }

        response.config.objForEach(function (value, key) {
          return _this27.pageConfigs[key] = value;
        });
        response = _this27.updateMeetingTimezone(response);
        _this27.entity = response;

        _this27.updateMeetingRules();

        if (response.roomId && response.status === 'live') {
          setTimeout(function () {
            _this27.initMediaAndRtcmConnection();

            _this27.rtcmConnection.checkPresence(response.roomId, function (isRoomExist, roomid) {
              _this27.roomIdAlive = !!isRoomExist;
            });
          }, 1000);
        }

        _this27.joinChannel();

        if (response.isInstantMeeting && !response.isBlocked && response.status === 'live') {
          _this27.getOnline();
        } else {
          _this27.isLoading = false;
        }

        return response;
      })["catch"](function (error) {
        _this27.isLoading = false;
        formUtil.handleErrors(error);

        _this27.$router.push({
          name: _this27.fallBackRoute
        });

        return error;
      });
    },
    setupMediaDevices: function setupMediaDevices() {
      var _this28 = this;

      this.isLoading = true;

      var gotDevicesList = function gotDevicesList(devicesInfos) {
        devicesInfos.forEach(function (deviceInfo) {
          var option = {
            uuid: deviceInfo.deviceId
          };

          if (deviceInfo.kind === 'audioinput') {
            option.name = deviceInfo.label || "microphone ".concat(_this28.mediaDevices.audioInput.length + 1);

            _this28.mediaDevices.audioInput.push(option);
          } else if (deviceInfo.kind === 'audiooutput') {
            option.name = deviceInfo.label || "speaker ".concat(_this28.mediaDevices.audioOutput.length + 1);

            _this28.mediaDevices.audioOutput.push(option);
          } else if (deviceInfo.kind === 'videoinput') {
            option.name = deviceInfo.label || "camera ".concat(_this28.mediaDevices.videoInput.length + 1);

            _this28.mediaDevices.videoInput.push(option);
          } else {
            console.log('Some other kind of source/device: ', deviceInfo);
          }
        });
        _this28.mediaConfigurations.selectedAudioInput = _this28.mediaDevices.audioInput ? _this28.mediaDevices.audioInput[0] : null;
        _this28.mediaConfigurations.selectedVideoInput = _this28.mediaDevices.videoInput ? _this28.mediaDevices.videoInput[0] : null;

        _this28.getInitialData();
      };

      var handleError = function handleError(error) {
        console.log('navigator.MediaDevices.getUserMedia error: ', error.message, error.name);

        if (error.name === 'NotAllowedError' || error.name === 'PermissionDeniedError') {
          _this28.permissionRejected = true;
        }

        _this28.getInitialData();
      };

      this.mediaDevices.audioInput = [];
      this.mediaDevices.audioOutput = [];
      this.mediaDevices.videoInput = [];
      navigator.mediaDevices.getUserMedia({
        audio: true,
        video: true
      }).then(function (stream) {
        stream.getTracks().forEach(function (track) {
          return track.stop();
        });
        return navigator.mediaDevices.enumerateDevices();
      }).then(gotDevicesList)["catch"](handleError);
    },
    doInit: function doInit() {
      this.Init({
        url: this.initUrl
      });

      if (window.isDuplicate()) {
        this.duplicateTab = true;
      }

      this.setupMediaDevices();
    }
  }),
  mounted: function mounted() {
    var _this29 = this;

    document.body.classList.add("meeting-page");

    if (this.$route.params.uuid) {
      this.uuid = this.$route.params.uuid;
    }

    if (screenfull__WEBPACK_IMPORTED_MODULE_6___default.a.isEnabled) {
      screenfull__WEBPACK_IMPORTED_MODULE_6___default.a.on('change', function () {
        _this29.SetUiConfig({
          fullScreen: screenfull__WEBPACK_IMPORTED_MODULE_6___default.a.isFullscreen
        });
      });
      screenfull__WEBPACK_IMPORTED_MODULE_6___default.a.on('error', function (event) {
        console.error('Failed to enable fullscreen', event);
      });
      this.SetUiConfig({
        fullScreen: false
      });
    }

    this.doInit();
    window.addEventListener('beforeunload', this.beforeUnload);
  },
  created: function created() {
    this.SetUiConfig({
      pageHeaderShow: false,
      pageFooterShow: false
    }); // detect 2G and alert

    if (navigator.connection && navigator.connection.type === 'cellular' && navigator.connection.downlinkMax <= 0.115) {
      alert('2G is not supported. Please use a better internet service.');
    }
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    if (!to.params.uuid) {
      next({
        name: from === 'appMeetingView' ? from : 'AppMeetingList'
      });
    } else {
      next(function (vm) {
        vm.prevRoute = from;
      });
    }
  },
  beforeDestroy: function beforeDestroy() {
    if (!window.isMeetingPageDestroyed) {
      this.isLoading = true;
      this.destroyPage();

      if (this.entity && this.entity.status === 'live') {
        this.Custom({
          url: "/".concat(this.initUrl, "/").concat(this.uuid, "/leave"),
          method: 'post'
        });
      }

      window.isMeetingPageDestroyed = true;
    }
  },
  beforeRouteLeave: function beforeRouteLeave(to, from, next) {
    this.isLoading = true;
    this.destroyPage();
    window.removeEventListener('beforeunload', this.beforeUnload);

    if (this.entity && this.entity.status === 'live') {
      this.Custom({
        url: "/".concat(this.initUrl, "/").concat(this.uuid, "/leave"),
        method: 'post'
      }).then(function (response) {
        window.isMeetingPageDestroyed = true;
        next();
      })["catch"](function (error) {
        window.isMeetingPageDestroyed = true;
        next();
      });
    } else {
      window.isMeetingPageDestroyed = true;
      next();
    }
  },
  destroyed: function destroyed() {
    document.body.classList.remove("meeting-page");
  }
});

/***/ }),

/***/ "./resources/js/views/app/meeting/live.vue":
/*!*************************************************!*\
  !*** ./resources/js/views/app/meeting/live.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _live_vue_vue_type_template_id_12b2cb6d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./live.vue?vue&type=template&id=12b2cb6d& */ "./resources/js/views/app/meeting/live.vue?vue&type=template&id=12b2cb6d&");
/* harmony import */ var _live_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./live.vue?vue&type=script&lang=js& */ "./resources/js/views/app/meeting/live.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _live_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./live.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/views/app/meeting/live.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _live_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _live_vue_vue_type_template_id_12b2cb6d___WEBPACK_IMPORTED_MODULE_0__["render"],
  _live_vue_vue_type_template_id_12b2cb6d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/app/meeting/live.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/app/meeting/live.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/views/app/meeting/live.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_live_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./live.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/meeting/live.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_live_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/app/meeting/live.vue?vue&type=style&index=0&lang=scss&":
/*!***********************************************************************************!*\
  !*** ./resources/js/views/app/meeting/live.vue?vue&type=style&index=0&lang=scss& ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_live_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--9-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--9-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./live.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/meeting/live.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_live_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_live_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_live_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_live_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_live_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/views/app/meeting/live.vue?vue&type=template&id=12b2cb6d&":
/*!********************************************************************************!*\
  !*** ./resources/js/views/app/meeting/live.vue?vue&type=template&id=12b2cb6d& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_live_vue_vue_type_template_id_12b2cb6d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./live.vue?vue&type=template&id=12b2cb6d& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/meeting/live.vue?vue&type=template&id=12b2cb6d&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_live_vue_vue_type_template_id_12b2cb6d___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_live_vue_vue_type_template_id_12b2cb6d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);
//# sourceMappingURL=live.js.map?id=bcc86061d186cc4f8904