(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/app/meeting/live~js/layouts/app-layout"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/conversation-header.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/chat-box/conversation-header.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    room: {
      type: Object,
      required: true
    },
    roomName: {
      type: String,
      required: true
    },
    vars: {
      type: Object,
      "default": function _default() {
        return {};
      }
    },
    typingUser: {
      type: String
    }
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('user', ['liveUsers'])), {}, {
    computedUser: function computedUser() {
      return this.room.isGroup || !this.room.member ? this.room : this.room.member;
    }
  }),
  methods: {
    isLiveOnline: function isLiveOnline(user) {
      return user && this.liveUsers.find(function (u) {
        return u.uuid === user.uuid;
      }) ? true : false;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/header.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/chat-box/header.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    activeContainer: {
      type: String,
      "default": 'home',
      required: true
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/index.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/chat-box/index.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _components_FileUploader__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @components/FileUploader */ "./resources/js/components/FileUploader.vue");
/* harmony import */ var vue_emoji_picker__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-emoji-picker */ "./node_modules/vue-emoji-picker/dist-module/main.js");
/* harmony import */ var vue_emoji_picker__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(vue_emoji_picker__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _header__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./header */ "./resources/js/components/chat-box/header.vue");
/* harmony import */ var _settings__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./settings */ "./resources/js/components/chat-box/settings.vue");
/* harmony import */ var _room_item__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./room-item */ "./resources/js/components/chat-box/room-item.vue");
/* harmony import */ var _conversation_header__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./conversation-header */ "./resources/js/components/chat-box/conversation-header.vue");
/* harmony import */ var _message_item__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./message-item */ "./resources/js/components/chat-box/message-item.vue");
/* harmony import */ var vue_at__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! vue-at */ "./node_modules/vue-at/dist/vue-at.js");
/* harmony import */ var vue_at__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(vue_at__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _js_event_bus__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @js/event-bus */ "./resources/js/event-bus.js");
/* harmony import */ var _core_utils_form__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @core/utils/form */ "./resources/js/core/utils/form.js");
/* harmony import */ var _core_utils_formatter__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @core/utils/formatter */ "./resources/js/core/utils/formatter.js");
/* harmony import */ var _js_vars__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @js/vars */ "./resources/js/vars.js");
/* harmony import */ var _core_utils_media__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @core/utils/media */ "./resources/js/core/utils/media.js");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//














/* harmony default export */ __webpack_exports__["default"] = ({
  name: "ChatBox",
  components: {
    FileUploader: _components_FileUploader__WEBPACK_IMPORTED_MODULE_1__["default"],
    EmojiPicker: vue_emoji_picker__WEBPACK_IMPORTED_MODULE_2___default.a,
    At: vue_at__WEBPACK_IMPORTED_MODULE_8___default.a,
    ChatBoxHeader: _header__WEBPACK_IMPORTED_MODULE_3__["default"],
    ChatBoxSettings: _settings__WEBPACK_IMPORTED_MODULE_4__["default"],
    ChatRoomItem: _room_item__WEBPACK_IMPORTED_MODULE_5__["default"],
    ConversationHeader: _conversation_header__WEBPACK_IMPORTED_MODULE_6__["default"],
    MessageItem: _message_item__WEBPACK_IMPORTED_MODULE_7__["default"]
  },
  props: {
    channel: {
      "default": null
    },
    boxVisibility: {
      type: Boolean,
      "default": false
    },
    meeting: {
      type: Object,
      "default": function _default() {
        return {};
      }
    }
  },
  data: function data() {
    return {
      isBoxShown: false,
      isSearchShown: false,
      showChatOptions: false,
      largeChatBox: false,
      searched: false,
      newChatScreen: false,
      cbHeaderTitle: $t('chat.live_chat'),
      searchTerm: '',
      searchTermEmoji: '',
      activeContainer: 'home',
      members: [],
      newMessage: {
        message: '',
        attachments: [],
        updated: false
      },
      newMessageRequestUUID: uuid(),
      activeConversation: null,
      uploadURL: '',
      formErrors: {},
      preRequisite: {
        uploaderConfig: {
          module: '',
          token: '',
          allowedExtensions: '',
          maxNoOfFiles: 1,
          uuid: uuid()
        }
      },
      isFetching: false,
      noMoreMessages: false,
      newUnreadMessageOnFeed: false,
      showScrollToLatestBtn: false,
      chatRoomChannel: null,
      typingUser: null,
      typingUserTimeout: null,
      initialNewMessage: null,
      firstFetch: true,
      fileIcons: _js_vars__WEBPACK_IMPORTED_MODULE_12__["fileIcons"],
      fileMimeTypes: _js_vars__WEBPACK_IMPORTED_MODULE_12__["fileMimeTypes"]
    };
  },
  watch: {
    boxVisibility: function boxVisibility(newVal, oldVal) {
      this.isBoxShown = newVal;

      if (this.isBoxShown) {
        document.body.classList.add("chat-box-shown");
      }
    },
    activeChatUser: function activeChatUser(newVal, oldVal) {
      if (newVal !== null && (!oldVal || oldVal !== null && newVal.uuid !== oldVal.uuid)) {
        this.goToConversation({
          user: newVal
        });
      }
    },
    activeChatRoom: function activeChatRoom(newVal, oldVal) {
      if (newVal !== null && (!oldVal || oldVal !== null && newVal.uuid !== oldVal.uuid)) {
        this.goToConversation({
          room: newVal
        });
      }
    }
  },
  computed: _objectSpread(_objectSpread(_objectSpread(_objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('config', ['vars', 'configs', 'uiConfigs'])), Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('user', {
    'userUuid': 'uuid'
  })), Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('user', ['profile', 'username'])), Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('chat', ['rooms', 'roomsPageMeta', 'roomsSearch', 'messages', 'messagesPageMeta', 'messagesSearch', 'activeChatRoom', 'activeChatUser'])), {}, {
    user: function user() {
      return {
        uuid: this.userUuid,
        username: this.username,
        name: this.profile.name,
        profile: this.profile
      };
    },
    hasMessages: function hasMessages() {
      return this.activeConversation && this.messages.data;
    },
    computedParticipants: function computedParticipants() {
      if (this.activeConversation) {
        return this.activeConversation.members;
      }

      return [];
    },
    enterToSubmit: function enterToSubmit() {
      return this.configs.chat && this.configs.chat.enterToSubmit;
    }
  }),
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])('chat', ['GetRooms', 'GetRoom', 'SearchRooms', 'SearchMessages', 'GetMessages', 'SendMessage', 'PutMessageToFeed', 'PatchMessageInFeed', 'ResetMessage', 'SetMessage', 'ResetTotalUnreadCount', 'SetActiveChatRoom', 'SetActiveChatUser', 'ResetActiveChatRoomAndUser'])), {}, {
    // TOGGLE & HIDE SHOW METHODS
    hideChatBox: function hideChatBox() {
      var _this = this;

      this.isBoxShown = false;
      this.ResetActiveChatRoomAndUser();
      setTimeout(function () {
        _this.$emit('boxHidden');
      }, 200);
    },
    showChatBox: function showChatBox() {
      this.isBoxShown = true;
      this.$emit('boxShown');
    },
    hideSearch: function hideSearch() {
      this.searchTerm = '';
      this.searched = false;
      this.isSearchShown = false;
      this.showChatOptions = true;
      this.newChatScreen = false;

      if (this.activeContainer === 'home') {
        this.fetchRooms(true);
        this.scrollTo("vue-scroll-rooms", 0);
      } else if (this.activeContainer === 'conversation') {
        this.fetchMessages(true);
      }
    },
    toggleSearch: function toggleSearch() {
      var _this2 = this;

      var startNewChat = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      this.searchTerm = '';
      this.searched = false;
      this.isSearchShown = !this.isSearchShown;

      if (this.isSearchShown) {
        this.newChatScreen = startNewChat;
        this.showChatOptions = !startNewChat;
        this.$nextTick(function () {
          _this2.$refs["searchTerm"].focus();
        });
      } else {
        this.newChatScreen = false;
        this.showChatOptions = true;

        if (this.activeContainer === 'home') {
          this.fetchRooms(true);
          this.scrollTo("vue-scroll-rooms", 0);
        } else if (this.activeContainer === 'conversation') {
          this.fetchMessages(true);
        }
      }
    },
    startNewChat: function startNewChat() {
      this.toggleSearch(true);
    },
    createGroup: function createGroup() {
      this.toggleSearch(true);
    },
    // SWITCH TO VIEW METHODS
    goHome: function goHome() {
      var _this3 = this;

      var comingFrom = this.activeContainer;
      this.activeContainer = 'home';
      this.ResetActiveChatRoomAndUser();

      if (comingFrom === 'conversation') {
        this.leaveChatRoomChannel(this.activeConversation.chatRoom);
        this.hideSearch();

        if (this.searchRoomObj && this.searchRoomObj.searched) {
          this.searchTerm = this.searchRoomObj.searchTerm;
          this.searched = this.searchRoomObj.searched;
          this.isSearchShown = true;
          this.showChatOptions = false;
        } else {
          this.fetchRooms();
        }
      } else {
        this.fetchRooms();
      }

      this.scrollTo("vue-scroll-rooms", 0);
      setTimeout(function () {
        _this3.activeConversation = null;
      }, 100);
    },
    goToConversation: function goToConversation(_ref) {
      var _this4 = this;

      var room = _ref.room,
          user = _ref.user;
      var fetchRoomForUser = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      var swtichToConversationTab = function swtichToConversationTab() {
        setTimeout(function () {
          _this4.activeContainer = 'conversation';

          if (_this4.isSearchShown) {
            _this4.searchRoomObj = {
              searchTerm: _this4.searchTerm,
              searched: _this4.searched
            };

            _this4.hideSearch();
          } else {
            _this4.searchRoomObj = {
              searchTerm: null,
              searched: null
            };
          }

          window.setTimeout(function () {
            _this4.scrollToLatestMessages();

            _this4.focusNewMessage();

            _this4.showScrollToLatestBtn = false;
            _this4.newUnreadMessageOnFeed = false;
          }, 300);
        }, 100);
      };

      this.activeConversation = {
        chatRoom: room || null,
        user: user || null,
        members: user ? [user] : []
      };
      this.ResetMessage();

      if (room) {
        this.joinChatRoomChannel(room);
        this.GetRoom({
          uuid: room.uuid
        }).then(function (response) {
          _this4.activeConversation.chatRoom = response;

          _this4.SetMessage({
            data: _toConsumableArray(response.chats),
            meta: _objectSpread(_objectSpread({}, _this4.messages.meta), {}, {
              total: response.chats.length,
              lastItemUuid: response.chats.length ? response.chats[response.chats.length - 1].uuid : null
            })
          });

          if (_this4.activeConversation.chatRoom && _this4.activeConversation.chatRoom.hasOwnProperty('chats')) {
            delete _this4.activeConversation.chatRoom.chats;
          }

          _this4.noMoreMessages = false;

          _this4.SetActiveChatRoom(_this4.activeConversation.chatRoom);

          swtichToConversationTab();
        })["catch"](function (error) {
          formUtil.handleErrors(error);
        });
      } else {
        this.SetActiveChatUser(this.activeConversation.user);
        this.fetchMessages(true);
        swtichToConversationTab();
      }
    },
    goToSettings: function goToSettings() {
      var _this5 = this;

      var comingFrom = this.activeContainer;
      this.activeContainer = 'settings';
      this.scrollTo("vue-scroll-rooms", 0);
      setTimeout(function () {
        if (comingFrom === 'conversation') {
          _this5.ResetActiveChatRoomAndUser();

          _this5.leaveChatRoomChannel(_this5.activeConversation.chatRoom);
        }

        if (_this5.isSearchShown) {
          _this5.hideSearch();
        }

        _this5.activeConversation = null;
      }, 100);
    },
    // FETCH DATA METHODS
    searchTermChanged: _.debounce(function () {
      if (!this.searchTerm || this.searchTerm.length < 2) {
        return;
      }

      if (this.activeContainer === 'home') {
        this.searchRooms();
        this.scrollTo("vue-scroll-rooms", 0);
      } else if (this.activeContainer === 'conversation') {
        this.searchMessages();
      }
    }, 500),
    fetchRooms: function fetchRooms() {
      var _this6 = this;

      var reset = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
      this.isFetching = true;

      var query = _objectSpread(_objectSpread({}, this.roomsPageMeta), {}, {
        lastItemUuid: reset ? null : this.rooms.meta.lastItemUuid,
        perPage: this.rooms.meta.perPage
      });

      this.GetRooms(query).then(function (response) {
        _this6.isFetching = false;
      })["catch"](function (error) {
        _this6.isFetching = false;
        formUtil.handleErrors(error);
      });
    },
    searchRooms: function searchRooms() {
      var _this7 = this;

      this.isFetching = true;
      this.SearchRooms(this.searchTerm).then(function (response) {
        _this7.searched = true;
        _this7.showChatOptions = false;
        _this7.isFetching = false;
      })["catch"](function (error) {
        _this7.isFetching = false;
        formUtil.handleErrors(error);
      });
    },
    searchMessages: function searchMessages() {
      var _this8 = this;

      if (!this.activeConversation.chatRoom) {
        return;
      }

      this.isFetching = true;
      this.SearchMessages({
        q: this.searchTerm,
        chatRoom: this.activeConversation.chatRoom.uuid
      }).then(function (response) {
        _this8.searched = true;
        _this8.isFetching = false;
        window.setTimeout(function () {
          _this8.scrollToLatestMessages();

          _this8.showScrollToLatestBtn = false;
          _this8.newUnreadMessageOnFeed = false;
        }, 300);
      })["catch"](function (error) {
        _this8.isFetching = false;
        formUtil.handleErrors(error);
      });
    },
    fetchMessages: function fetchMessages() {
      var _this9 = this;

      var reset = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      if (!this.activeConversation.chatRoom && !this.activeConversation.user) {
        return;
      }

      this.isFetching = true;
      this.noMoreMessages = false;
      this.newUnreadMessageOnFeed = false;

      var query = _objectSpread(_objectSpread({}, this.messagesPageMeta), {}, {
        lastItemUuid: reset ? null : this.messages.meta.lastItemUuid,
        perPage: this.messages.meta.perPage,
        chatRoom: this.activeConversation.chatRoom ? this.activeConversation.chatRoom.uuid : null,
        user: this.activeConversation.user ? this.activeConversation.user.uuid : null
      });

      this.GetMessages(query).then(function (response) {
        _this9.isFetching = false;

        if (!response.data.length) {
          _this9.noMoreMessages = true;
        }

        if (reset) {
          window.setTimeout(function () {
            _this9.scrollToLatestMessages();

            _this9.showScrollToLatestBtn = false;
            _this9.newUnreadMessageOnFeed = false;
          }, 300);
        }

        if (!_this9.searched) {
          _this9.focusNewMessage();
        }
      })["catch"](function (error) {
        _this9.isFetching = false;
        formUtil.handleErrors(error);
      });
    },
    // POST DATA & JOIN LEAVE METHODS
    postMessage: function postMessage() {
      var _this10 = this;

      if (!this.newMessage.message) {
        return;
      }

      this.newMessage.message = this.newMessage.message.trim();
      var messageObj = {
        uuid: this.newMessageRequestUUID,
        message: this.newMessage.message,
        user: this.user,
        sentAt: moment().format(this.vars.serverDateTimeFormat),
        status: 'sending'
      };
      this.newMessageRequestUUID = uuid();
      this.newMessage = _.cloneDeep(this.initialNewMessage);
      this.noMoreMessages = false;
      this.putMessageToFeed(messageObj);
      window.setTimeout(function () {
        _this10.scrollTo("vue-scroll-messages", 100);

        _this10.focusNewMessage();
      }, 300);
      this.SendMessage({
        message: messageObj.message,
        chatRoom: this.activeConversation.chatRoom,
        user: this.activeConversation.user
      }).then(function (response) {
        _this10.messageSentSuccess(messageObj, response);
      })["catch"](function (error) {
        _this10.messageSendingFailed(messageObj, error);
      });
    },
    attachmentUploadStarted: function attachmentUploadStarted(count) {
      var _this11 = this;

      if (!this.newMessage.attachments || this.newMessage.attachments.length) {
        return;
      }

      var messageObj = {
        uuid: this.newMessageRequestUUID,
        message: null,
        user: this.user,
        sentAt: moment().format(this.vars.serverDateTimeFormat),
        status: 'sending'
      };
      this.newMessageRequestUUID = uuid();
      this.newMessage = _.cloneDeep(this.initialNewMessage);
      this.putMessageToFeed(messageObj);
      window.setTimeout(function () {
        _this11.scrollTo("vue-scroll-messages", 100);

        _this11.focusNewMessage();
      }, 300);
    },
    attachmentUploaded: function attachmentUploaded(response) {
      _js_event_bus__WEBPACK_IMPORTED_MODULE_9__["default"].$emit('RESET_UPLOAD');
      this.messageSentSuccess({
        uuid: response.meta.requestUuid
      }, response);
    },
    attachmentUploadError: function attachmentUploadError(error) {
      _js_event_bus__WEBPACK_IMPORTED_MODULE_9__["default"].$emit('RESET_UPLOAD');

      if (error) {
        this.messageSendingFailed({
          uuid: response.meta.requestUuid
        }, error);
      }
    },
    messageSentSuccess: function messageSentSuccess(oldMessage, newMessageResponse) {
      var _this12 = this;

      var newMessage = newMessageResponse.data;
      this.PatchMessageInFeed({
        old: oldMessage,
        "new": _objectSpread(_objectSpread({}, newMessage), {}, {
          newUuid: newMessage.uuid,
          uuid: oldMessage.uuid
        })
      });

      if (!this.activeConversation.chatRoom) {
        this.joinChatRoomChannel(newMessage.chatRoom);
        this.GetRoom({
          uuid: newMessage.chatRoom.uuid
        }).then(function (response) {
          _this12.activeConversation.chatRoom = response;

          if (_this12.activeConversation.chatRoom && _this12.activeConversation.chatRoom.hasOwnProperty('chats')) {
            delete _this12.activeConversation.chatRoom.chats;
          }
        })["catch"](function (error) {
          formUtil.handleErrors(error);
        });
      }
    },
    messageSendingFailed: function messageSendingFailed(oldMessage, error) {
      this.PatchMessageInFeed({
        old: oldMessage,
        "new": _objectSpread(_objectSpread({}, oldMessage), {}, {
          status: 'error',
          error: formUtil.getError(error)
        })
      });
    },
    noNeedCallback: function noNeedCallback() {
      this.afterUploadComplete();
    },
    joinChatRoomChannel: function joinChatRoomChannel(chatRoom) {
      window.EchoOpts.subscriptions.chatRoom = window.Echo.join("ChatRoom.".concat(chatRoom.uuid));
      this.chatRoomChannel = window.EchoOpts.subscriptions.chatRoom;
      this.chatRoomChannel.listen('ChatMessageSentToRoom', this.newMessageReceived);
      this.chatRoomChannel.listenForWhisper('typing', this.anotherUserTyping);
    },
    leaveChatRoomChannel: function leaveChatRoomChannel(chatRoom) {
      if (this.chatRoomChannel) {
        this.chatRoomChannel.stopListening('ChatMessageSentToRoom');
        this.chatRoomChannel = null;
      }

      if (chatRoom) {
        window.Echo.leave("ChatRoom.".concat(chatRoom.uuid));
      }

      if (window.EchoOpts && window.EchoOpts.subscriptions) {
        window.EchoOpts.subscriptions.chatRoom = null;
      }
    },
    // HELPER METHODS OR EVENT CALLBACKS
    enterPressedEvt: function enterPressedEvt(fnCallback, event) {
      if (this.enterToSubmit) {
        return Object(_core_utils_form__WEBPACK_IMPORTED_MODULE_10__["enterPressed"])(fnCallback, event);
      }

      return false;
    },
    putMessageToFeed: function putMessageToFeed(message) {
      if (this.messages.data.findIndex(function (c) {
        return c.uuid === message.uuid;
      }) !== -1) {
        return;
      }

      this.PutMessageToFeed(message);
    },
    focusNewMessage: function focusNewMessage() {
      if (this.$refs["newMessageInput"]) {
        this.$refs['newMessageInput'].focus();
      }
    },
    scrollTo: function scrollTo(refId, pos) {
      if (this.$refs[refId]) {
        this.$refs[refId].scrollTo({
          y: "".concat(pos, "%")
        }, 300);
      }
    },
    scrollToLatestMessages: function scrollToLatestMessages() {
      this.scrollTo("vue-scroll-messages", 100);
    },
    handleMessageScroll: _.debounce(function (vertical, horizontal) {
      if (vertical.process > 0.95) {
        this.showScrollToLatestBtn = false;
        this.newUnreadMessageOnFeed = false;
      } else {
        if (!this.searched || this.searched && vertical.process !== 0) {
          this.showScrollToLatestBtn = true;
        }
      }

      if (vertical.process < 0.5 && !this.isFetching && !this.noMoreMessages) {
        this.fetchMessages();
      }
    }, 500),
    handleRoomsScroll: _.debounce(function (vertical, horizontal) {
      if (vertical.process > 0.5 && !this.isFetching) {
        this.fetchRooms();
      }
    }, 500),
    isTyping: _.throttle(function () {
      if (this.chatRoomChannel) {
        this.chatRoomChannel.whisper('typing', {
          user: this.user
        });
      }
    }, 500, {
      'leading': true,
      'trailing': false
    }),
    anotherUserTyping: function anotherUserTyping(_ref2) {
      var _this13 = this;

      var user = _ref2.user;
      this.typingUser = user.name;

      if (this.typingUserTimeout) {
        clearTimeout(this.typingUserTimeout);
      }

      this.typingUserTimeout = setTimeout(function () {
        _this13.typingUser = null;
      }, 1500);
    },
    newMessageReceived: function newMessageReceived(message) {
      var _this14 = this;

      if (!message) {
        return;
      }

      message = Object(_core_utils_formatter__WEBPACK_IMPORTED_MODULE_11__["formatKeysToCamelCase"])(message);

      if (message && message.user && message.user.uuid !== this.userUuid) {
        this.putMessageToFeed(message); // playIncomingMessage()

        window.setTimeout(function () {
          var _this14$$refs$vueScr = _this14.$refs["vue-scroll-messages"].getScrollProcess(),
              v = _this14$$refs$vueScr.v,
              h = _this14$$refs$vueScr.h;

          if (v > 0.5) {
            _this14.scrollToLatestMessages();
          } else {
            _this14.showScrollToLatestBtn = true;
            _this14.newUnreadMessageOnFeed = true;
          }
        }, 300);
      }
    },
    appendEmoji: function appendEmoji(emoji) {
      console.log(this.newMessage);
      console.log(emoji);
      this.newMessage.message += emoji;
    },
    doInit: function doInit() {
      var _this15 = this;

      this.initialNewMessage = _.cloneDeep(this.newMessage);

      if (this.activeChatRoom) {
        this.goToConversation({
          room: this.activeChatRoom
        });
      } else if (this.activeChatUser) {
        this.goToConversation({
          user: this.activeChatUser
        });
      } else {
        this.fetchRooms();
      }

      if (this.isBoxShown) {
        document.body.classList.add("chat-box-shown");
      }

      setTimeout(function () {
        _this15.showChatOptions = _this15.boxVisibility;
      }, 1000);
    }
  }),
  directives: {
    focus: {
      inserted: function inserted(el) {
        el.focus();
      }
    }
  },
  mounted: function mounted() {
    var _this16 = this;

    setTimeout(function () {
      _this16.isBoxShown = _this16.boxVisibility;
    }, 100);
    setTimeout(function () {
      _this16.doInit();
    }, 200);
  },
  created: function created() {},
  beforeDestroy: function beforeDestroy() {
    document.body.classList.remove("chat-box-shown");

    if (window.Echo && window.EchoOpts) {
      if (window.EchoOpts.subscriptions && window.EchoOpts.subscriptions.chatRoom) {
        window.EchoOpts.subscriptions.chatRoom.stopListening('ChatMessageSentToRoom');
        window.EchoOpts.subscriptions.chatRoom = null;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/message-item.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/chat-box/message-item.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    message: {
      type: Object,
      required: true
    },
    fileIcons: {
      type: Object,
      required: true
    },
    fileMimeTypes: {
      type: Object,
      required: true
    },
    vars: {
      type: Object,
      "default": function _default() {
        return {};
      }
    }
  },
  data: function data() {
    return {
      messageStatusIcons: {
        'sending': 'far fa-clock',
        'sent': 'fas fa-check',
        'delivered': 'fas fa-check-double',
        'read': 'fas fa-check-double',
        'error': 'fas fa-exclamation-triangle'
      }
    };
  },
  computed: {
    hasAttachment: function hasAttachment() {
      return !this.message.message && this.message.attachments && this.message.attachments.length;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/room-item.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/chat-box/room-item.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    room: {
      type: Object,
      required: true
    },
    roomName: {
      type: String,
      required: true
    },
    vars: {
      type: Object,
      "default": function _default() {
        return {};
      }
    }
  },
  computed: {
    computedAvatar: function computedAvatar() {
      return this.room.isGroup || !this.room.member ? this.room : this.room.member;
    }
  },
  methods: {
    goToConversation: function goToConversation() {
      if (this.room.isGroup || this.room.member) {
        this.$emit('goToConversation', {
          room: this.room,
          user: null
        });
      } else {
        this.$emit('goToConversation', {
          room: null,
          user: this.room
        });
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/settings.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/chat-box/settings.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _mixins_config_form__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @mixins/config-form */ "./resources/js/mixins/config-form.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": _mixins_config_form__WEBPACK_IMPORTED_MODULE_0__["default"],
  props: {
    activeContainer: {
      type: String,
      "default": 'home',
      required: true
    }
  },
  watch: {
    activeContainer: function activeContainer(newVal, oldVal) {
      if (newVal === 'settings' && newVal !== oldVal) {
        this.formData = Object.assign({}, this.formData, _.cloneDeep(this.initialFormData));
      }
    }
  },
  data: function data() {
    return {
      now: moment(),
      formData: {
        chat: {
          enableAutoOpen: false,
          enterToSubmit: false,
          muteSoundNotification: false
        },
        override: true
      },
      useUserPreference: true
    };
  },
  methods: {
    afterSubmitSuccess: function afterSubmitSuccess() {
      this.$emit('goHome');
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/index.vue?vue&type=style&index=0&lang=scss&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--9-2!./node_modules/sass-loader/dist/cjs.js??ref--9-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/chat-box/index.vue?vue&type=style&index=0&lang=scss& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".emoji-picker-container {\n  position: absolute;\n  left: 0;\n  bottom: 45px;\n  z-index: 1;\n  width: 100%;\n  height: 300px;\n  overflow: hidden;\n  padding: 1rem;\n  box-sizing: border-box;\n  border-radius: 15px 15px 0 0;\n  background: #fff;\n  box-shadow: 1px 1px 8px #000000;\n}\n.emoji-picker-container .emoji-picker {\n  overflow: scroll;\n  height: 100%;\n}\n.emoji-picker__search {\n  display: flex;\n  margin-bottom: 1rem;\n}\n.emoji-picker__search > input {\n  flex: 1;\n  border-radius: 10px;\n  border: 1px solid #ccc;\n  padding: 0.5rem 1rem;\n  outline: none;\n}\n.emoji-picker h5 {\n  margin-bottom: 0;\n  color: #b1b1b1;\n  text-transform: uppercase;\n  font-size: 0.8rem;\n  cursor: default;\n}\n.emoji-picker .emojis {\n  display: flex;\n  flex-wrap: wrap;\n  justify-content: space-between;\n}\n.emoji-picker .emojis:after {\n  content: \"\";\n  flex: auto;\n}\n.emoji-picker .emojis span {\n  padding: 0.2rem;\n  cursor: pointer;\n  border-radius: 5px;\n}\n.emoji-picker .emojis span:hover {\n  background: #ececec;\n  cursor: pointer;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/settings.vue?vue&type=style&index=0&lang=scss&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--9-2!./node_modules/sass-loader/dist/cjs.js??ref--9-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/chat-box/settings.vue?vue&type=style&index=0&lang=scss& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".settings-wrapper {\n  display: flex;\n  flex-direction: column;\n  height: 100%;\n  max-height: 100%;\n  overflow: hidden;\n}\n.settings-wrapper .settings {\n  display: flex;\n  flex-grow: 1;\n  justify-content: stretch;\n  align-items: stretch;\n  height: 100%;\n  max-height: 100%;\n  overflow: hidden;\n  position: relative;\n  padding: 0.85rem;\n  display: flex;\n  flex-direction: row;\n}\n.settings-wrapper .settings .switch-wrapper > .input-group-material-label {\n  padding-left: 0;\n}\n.settings-wrapper .settings-footer {\n  box-shadow: 0 -2px 6px 0 rgba(0, 0, 0, 0.4);\n  z-index: 1;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/index.vue?vue&type=style&index=0&lang=scss&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--9-2!./node_modules/sass-loader/dist/cjs.js??ref--9-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/chat-box/index.vue?vue&type=style&index=0&lang=scss& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--9-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--9-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/index.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/settings.vue?vue&type=style&index=0&lang=scss&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--9-2!./node_modules/sass-loader/dist/cjs.js??ref--9-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/chat-box/settings.vue?vue&type=style&index=0&lang=scss& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--9-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--9-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./settings.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/settings.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/conversation-header.vue?vue&type=template&id=2ce8a029&":
/*!*******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/chat-box/conversation-header.vue?vue&type=template&id=2ce8a029& ***!
  \*******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "conversation-header" }, [
    _c(
      "div",
      { staticClass: "room-thumb" },
      [_c("user-avatar", { attrs: { user: _vm.computedUser, size: 40 } })],
      1
    ),
    _vm._v(" "),
    _c("div", { staticClass: "room-details" }, [
      _c("div", { staticClass: "title-row" }, [
        _c("h6", { staticClass: "room-title" }, [_vm._v(_vm._s(_vm.roomName))])
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "meta-row" },
        [
          !_vm.room.isGroup && !_vm.typingUser
            ? [
                _vm.isLiveOnline(_vm.computedUser)
                  ? _c("span", [_vm._v(_vm._s(_vm.$t("general.online")))])
                  : _c("span", [_vm._v(_vm._s(_vm.$t("general.offline")))])
              ]
            : _vm.room.isGroup
            ? [
                _c(
                  "span",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.typingUser,
                        expression: "typingUser"
                      }
                    ],
                    staticClass: "typing-indicator"
                  },
                  [
                    _vm._v(
                      "@" +
                        _vm._s(_vm.typingUser) +
                        " " +
                        _vm._s(_vm.$t("general.is_typing"))
                    )
                  ]
                ),
                _vm._v(" "),
                _c("span", [_vm._v("  ")])
              ]
            : [
                _c(
                  "span",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.typingUser,
                        expression: "typingUser"
                      }
                    ],
                    staticClass: "typing-indicator"
                  },
                  [_vm._v(_vm._s(_vm.$t("general.is_typing")))]
                ),
                _vm._v(" "),
                _c("span", [_vm._v("  ")])
              ]
        ],
        2
      )
    ]),
    _vm._v(" "),
    _vm._m(0)
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "room-btns d-none" }, [
      _c("button", { attrs: { type: "button" } }, [
        _c("i", { staticClass: "fas fa-phone-alt" })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/header.vue?vue&type=template&id=b871232a&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/chat-box/header.vue?vue&type=template&id=b871232a& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "chat-box-header" }, [
    _c("h6", { staticClass: "cb-header-title" }, [
      _vm._v(
        _vm._s(
          _vm.activeContainer === "settings"
            ? _vm.$t("config.chat.chat_config")
            : _vm.$t("chat.live_chat")
        )
      )
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "cb-header-buttons" },
      [
        _vm.activeContainer !== "home"
          ? _c(
              "button",
              {
                attrs: { type: "button" },
                on: {
                  click: function($event) {
                    return _vm.$emit("goHome")
                  }
                }
              },
              [
                _c("i", {
                  class: [
                    "fas",
                    {
                      "fa-chevron-left": _vm.activeContainer === "conversation"
                    },
                    { "fa-chevron-right": _vm.activeContainer === "settings" }
                  ]
                })
              ]
            )
          : _vm._e(),
        _vm._v(" "),
        _vm.activeContainer !== "settings"
          ? [
              _c(
                "button",
                {
                  attrs: { type: "button" },
                  on: {
                    click: function($event) {
                      return _vm.$emit("toggleSearch")
                    }
                  }
                },
                [_c("i", { staticClass: "fas fa-search" })]
              ),
              _vm._v(" "),
              _c(
                "button",
                {
                  attrs: { type: "button" },
                  on: {
                    click: function($event) {
                      return _vm.$emit("goToSettings")
                    }
                  }
                },
                [_c("i", { staticClass: "fas fa-cog" })]
              )
            ]
          : _vm._e(),
        _vm._v(" "),
        _c(
          "button",
          {
            attrs: { type: "button" },
            on: {
              click: function($event) {
                return _vm.$emit("hideChatBox")
              }
            }
          },
          [_c("i", { staticClass: "fas fa-times" })]
        )
      ],
      2
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/index.vue?vue&type=template&id=7eb429e4&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/chat-box/index.vue?vue&type=template&id=7eb429e4& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      class: [
        "chat-box-wrapper",
        { shown: _vm.isBoxShown },
        { "search-shown": _vm.isSearchShown },
        { "chat-box-lg": _vm.largeChatBox },
        { "show-chat-options": _vm.showChatOptions }
      ]
    },
    [
      _c("chat-box-header", {
        attrs: { "active-container": _vm.activeContainer },
        on: {
          goHome: _vm.goHome,
          toggleSearch: _vm.toggleSearch,
          goToSettings: _vm.goToSettings,
          hideChatBox: _vm.hideChatBox
        }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "search" }, [
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.searchTerm,
              expression: "searchTerm"
            }
          ],
          ref: "searchTerm",
          staticClass: "form-control",
          attrs: {
            type: "text",
            placeholder: _vm.$t("general.search_helper_default_text")
          },
          domProps: { value: _vm.searchTerm },
          on: {
            keydown: function($event) {
              if (
                !$event.type.indexOf("key") &&
                _vm._k($event.keyCode, "esc", 27, $event.key, ["Esc", "Escape"])
              ) {
                return null
              }
              return _vm.toggleSearch($event)
            },
            input: [
              function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.searchTerm = $event.target.value
              },
              _vm.searchTermChanged
            ]
          }
        }),
        _vm._v(" "),
        _vm.searchTerm
          ? _c(
              "button",
              {
                staticClass: "clear-btn",
                attrs: { type: "button" },
                on: { click: _vm.hideSearch }
              },
              [_vm._v(_vm._s(_vm.$t("general.clear")))]
            )
          : _vm._e()
      ]),
      _vm._v(" "),
      _c(
        "div",
        {
          class: [
            "chat-box",
            { "showing-settings": _vm.activeContainer === "settings" },
            { "showing-chats": _vm.activeContainer === "conversation" }
          ]
        },
        [
          _c("chat-box-settings", {
            attrs: { "active-container": _vm.activeContainer },
            on: { goHome: _vm.goHome }
          }),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "cb-inner-window rooms-container" },
            [
              _c(
                "vue-scroll",
                {
                  ref: "vue-scroll-rooms",
                  attrs: { id: "vue-scroll-rooms" },
                  on: { "handle-scroll": _vm.handleRoomsScroll }
                },
                [
                  _c(
                    "div",
                    { staticClass: "rooms-wrapper" },
                    [
                      (_vm.searchTerm && _vm.searched) || _vm.newChatScreen
                        ? [
                            _vm.roomsSearch.rooms.length ||
                            _vm.roomsSearch.users.length
                              ? _c(
                                  "ul",
                                  { staticClass: "rooms" },
                                  [
                                    !_vm.newChatScreen
                                      ? [
                                          _vm._l(
                                            _vm.roomsSearch.rooms,
                                            function(room) {
                                              return _c("chat-room-item", {
                                                key: room.uuid,
                                                attrs: {
                                                  room: room,
                                                  "room-name": room.name,
                                                  vars: _vm.vars
                                                },
                                                on: {
                                                  goToConversation:
                                                    _vm.goToConversation
                                                }
                                              })
                                            }
                                          ),
                                          _vm._v(" "),
                                          _vm.roomsSearch.users.length
                                            ? _c(
                                                "li",
                                                {
                                                  staticClass:
                                                    "my-2 pl-3 font-weight-500"
                                                },
                                                [
                                                  _vm._v(
                                                    _vm._s(_vm.$t("user.users"))
                                                  )
                                                ]
                                              )
                                            : _vm._e()
                                        ]
                                      : _vm._e(),
                                    _vm._v(" "),
                                    _vm._l(_vm.roomsSearch.users, function(
                                      user
                                    ) {
                                      return _c("chat-room-item", {
                                        key: user.uuid,
                                        attrs: {
                                          room: user,
                                          "room-name": user.profile.name,
                                          vars: _vm.vars
                                        },
                                        on: {
                                          goToConversation: _vm.goToConversation
                                        }
                                      })
                                    })
                                  ],
                                  2
                                )
                              : _c("div", { staticClass: "no-data" }, [
                                  _c("h5", [
                                    _vm._v(
                                      _vm._s(_vm.$t("chat.no_result_found"))
                                    )
                                  ])
                                ])
                          ]
                        : [
                            _vm.rooms.data && _vm.rooms.data.length
                              ? _c(
                                  "ul",
                                  { staticClass: "rooms" },
                                  _vm._l(_vm.rooms.data, function(room) {
                                    return _c("chat-room-item", {
                                      key: room.uuid,
                                      attrs: {
                                        room: room,
                                        "room-name": room.name,
                                        vars: _vm.vars
                                      },
                                      on: {
                                        goToConversation: _vm.goToConversation
                                      }
                                    })
                                  }),
                                  1
                                )
                              : _c("div", { staticClass: "no-data" }, [
                                  _c("h5", [
                                    _vm._v(_vm._s(_vm.$t("chat.no_chats_yet")))
                                  ])
                                ])
                          ]
                    ],
                    2
                  )
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "chat-option-buttons" }, [
                _c(
                  "button",
                  {
                    staticClass: "d-none",
                    attrs: { type: "button" },
                    on: { click: _vm.createGroup }
                  },
                  [_c("i", { staticClass: "fas fa-user-friends" })]
                ),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    attrs: { type: "button" },
                    on: { click: _vm.startNewChat }
                  },
                  [
                    _c("i", {
                      staticClass: "fas fa-comment-dots fa-flip-horizontal"
                    })
                  ]
                ),
                _vm._v(" "),
                _vm._m(0)
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c("div", { staticClass: "cb-inner-window conversation-container" }, [
            _vm.activeConversation
              ? _c(
                  "div",
                  { staticClass: "conversation-wrapper" },
                  [
                    _vm.activeConversation.chatRoom
                      ? _c("conversation-header", {
                          attrs: {
                            room: _vm.activeConversation.chatRoom,
                            "room-name": _vm.activeConversation.chatRoom.name,
                            vars: _vm.vars,
                            typingUser: _vm.typingUser
                          }
                        })
                      : _vm.activeConversation.user
                      ? _c("conversation-header", {
                          attrs: {
                            room: _vm.activeConversation.user,
                            "room-name":
                              _vm.activeConversation.user.profile.name,
                            vars: _vm.vars,
                            typingUser: _vm.typingUser
                          }
                        })
                      : _vm._e(),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        class: [
                          "conversation",
                          { "no-messages": !_vm.hasMessages },
                          { "got-new-messages": _vm.newUnreadMessageOnFeed },
                          { "show-scroll-btn": _vm.showScrollToLatestBtn }
                        ]
                      },
                      [
                        _vm.hasMessages
                          ? _c(
                              "div",
                              { staticClass: "messages-list-container" },
                              [
                                _c(
                                  "vue-scroll",
                                  {
                                    ref: "vue-scroll-messages",
                                    attrs: { id: "vue-scroll-messages" },
                                    on: {
                                      "handle-scroll": _vm.handleMessageScroll
                                    }
                                  },
                                  [
                                    _c(
                                      "div",
                                      {
                                        class: [
                                          "messages-list-wrapper",
                                          {
                                            "show-name":
                                              _vm.activeConversation.room &&
                                              _vm.activeConversation.room
                                                .isGroup
                                          }
                                        ]
                                      },
                                      [
                                        _vm.searchTerm && _vm.searched
                                          ? [
                                              _vm.messagesSearch.length
                                                ? _vm._l(
                                                    _vm.messagesSearch,
                                                    function(message, mIndex) {
                                                      return _c(
                                                        "message-item",
                                                        {
                                                          key: message.uuid,
                                                          class: [
                                                            {
                                                              "own-message":
                                                                message.user
                                                                  .uuid ===
                                                                _vm.user.uuid
                                                            },
                                                            {
                                                              "prev-diff-own":
                                                                !_vm.messages
                                                                  .data[
                                                                  mIndex + 1
                                                                ] ||
                                                                _vm.messages
                                                                  .data[
                                                                  mIndex + 1
                                                                ].user.uuid !==
                                                                  message.user
                                                                    .uuid
                                                            },
                                                            {
                                                              "next-diff-own":
                                                                !_vm.messages
                                                                  .data[
                                                                  mIndex - 1
                                                                ] ||
                                                                _vm.messages
                                                                  .data[
                                                                  mIndex - 1
                                                                ].user.uuid !==
                                                                  message.user
                                                                    .uuid
                                                            }
                                                          ],
                                                          attrs: {
                                                            message: message,
                                                            vars: _vm.vars,
                                                            "file-icons":
                                                              _vm.fileIcons,
                                                            "file-mime-types":
                                                              _vm.fileMimeTypes
                                                          }
                                                        }
                                                      )
                                                    }
                                                  )
                                                : _c(
                                                    "div",
                                                    { staticClass: "no-data" },
                                                    [
                                                      _c("h5", [
                                                        _vm._v(
                                                          _vm._s(
                                                            _vm.$t(
                                                              "chat.no_result_found"
                                                            )
                                                          )
                                                        )
                                                      ])
                                                    ]
                                                  )
                                            ]
                                          : [
                                              _vm._l(
                                                _vm.messages.data,
                                                function(message, mIndex) {
                                                  return _c("message-item", {
                                                    key: message.uuid,
                                                    class: [
                                                      {
                                                        "own-message":
                                                          message.user.uuid ===
                                                          _vm.user.uuid
                                                      },
                                                      {
                                                        "prev-diff-own":
                                                          !_vm.messages.data[
                                                            mIndex + 1
                                                          ] ||
                                                          _vm.messages.data[
                                                            mIndex + 1
                                                          ].user.uuid !==
                                                            message.user.uuid
                                                      },
                                                      {
                                                        "next-diff-own":
                                                          !_vm.messages.data[
                                                            mIndex - 1
                                                          ] ||
                                                          _vm.messages.data[
                                                            mIndex - 1
                                                          ].user.uuid !==
                                                            message.user.uuid
                                                      }
                                                    ],
                                                    attrs: {
                                                      message: message,
                                                      vars: _vm.vars,
                                                      "file-icons":
                                                        _vm.fileIcons,
                                                      "file-mime-types":
                                                        _vm.fileMimeTypes
                                                    }
                                                  })
                                                }
                                              ),
                                              _vm._v(" "),
                                              _vm.isFetching
                                                ? _c(
                                                    "div",
                                                    {
                                                      staticClass:
                                                        "d-flex justify-content-center my-2"
                                                    },
                                                    [
                                                      _c("animated-loader", {
                                                        attrs: {
                                                          "is-loading":
                                                            _vm.isFetching,
                                                          "loader-color":
                                                            _vm.vars.colors
                                                              .light,
                                                          "overlay-color":
                                                            "transparent",
                                                          size: "inline",
                                                          "loader-size": "20px",
                                                          "loader-stroke": "3px"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                : _vm._e(),
                                              _vm._v(" "),
                                              _vm.noMoreMessages
                                                ? _c(
                                                    "div",
                                                    {
                                                      staticClass:
                                                        "message-item my-4 d-flex justify-content-center align-items-center"
                                                    },
                                                    [
                                                      _c(
                                                        "h5",
                                                        {
                                                          staticClass:
                                                            "text-muted"
                                                        },
                                                        [
                                                          _vm._v(
                                                            "\n                                            " +
                                                              _vm._s(
                                                                _vm.$t(
                                                                  "chat.no_more_messages"
                                                                )
                                                              ) +
                                                              "\n                                        "
                                                          )
                                                        ]
                                                      )
                                                    ]
                                                  )
                                                : _vm._e()
                                            ]
                                      ],
                                      2
                                    )
                                  ]
                                )
                              ],
                              1
                            )
                          : _c(
                              "div",
                              {
                                staticClass:
                                  "d-flex justify-content-center align-items-center min-height-300px"
                              },
                              [
                                _c("h5", { staticClass: "text-muted" }, [
                                  _vm._v(
                                    "\n                            " +
                                      _vm._s(_vm.$t("chat.no_messages_yet")) +
                                      "\n                        "
                                  )
                                ])
                              ]
                            ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "scroll-latest-btn",
                            on: { click: _vm.scrollToLatestMessages }
                          },
                          [_vm._m(1)]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "conversation-footer" }, [
                      _c(
                        "div",
                        { staticClass: "new-message" },
                        [
                          _c(
                            "at",
                            {
                              attrs: {
                                members: _vm.computedParticipants,
                                "name-key": "username"
                              },
                              scopedSlots: _vm._u(
                                [
                                  {
                                    key: "item",
                                    fn: function(s) {
                                      return [
                                        _c(
                                          "span",
                                          { staticClass: "mentioned" },
                                          [
                                            _vm._v(
                                              _vm._s(s.item.profile.name) + " "
                                            ),
                                            _c(
                                              "span",
                                              {
                                                staticClass:
                                                  "bracketed text-muted-md"
                                              },
                                              [_vm._v(_vm._s(s.item.username))]
                                            )
                                          ]
                                        )
                                      ]
                                    }
                                  }
                                ],
                                null,
                                false,
                                1849386407
                              ),
                              model: {
                                value: _vm.newMessage.message,
                                callback: function($$v) {
                                  _vm.$set(_vm.newMessage, "message", $$v)
                                },
                                expression: "newMessage.message"
                              }
                            },
                            [
                              _vm._v(" "),
                              _c("div", {
                                ref: "newMessageInput",
                                staticClass: "new-message-input form-control",
                                attrs: {
                                  contenteditable: "",
                                  "data-placeholder": _vm.$t(
                                    "chat.type_a_message"
                                  ),
                                  placeholder: _vm.$t("chat.type_a_message")
                                },
                                on: {
                                  keydown: [
                                    _vm.isTyping,
                                    function($event) {
                                      if (
                                        !$event.type.indexOf("key") &&
                                        _vm._k(
                                          $event.keyCode,
                                          "enter",
                                          13,
                                          $event.key,
                                          "Enter"
                                        )
                                      ) {
                                        return null
                                      }
                                      return _vm.enterPressedEvt(
                                        _vm.postMessage,
                                        $event
                                      )
                                    }
                                  ]
                                }
                              })
                            ]
                          ),
                          _vm._v(" "),
                          _c("emoji-picker", {
                            staticClass: "emoji-picker-wrapper",
                            attrs: { search: _vm.searchTermEmoji },
                            on: { emoji: _vm.appendEmoji },
                            scopedSlots: _vm._u(
                              [
                                {
                                  key: "emoji-invoker",
                                  fn: function(ref) {
                                    var clickEvent = ref.events.click
                                    return _c(
                                      "button",
                                      {
                                        staticClass: "btn-custom emoji-button",
                                        attrs: { type: "button" },
                                        on: {
                                          click: function($event) {
                                            $event.stopPropagation()
                                            return clickEvent($event)
                                          }
                                        }
                                      },
                                      [_c("i", { staticClass: "far fa-grin" })]
                                    )
                                  }
                                },
                                {
                                  key: "emoji-picker",
                                  fn: function(ref) {
                                    var emojis = ref.emojis
                                    var insert = ref.insert
                                    var display = ref.display
                                    return _c(
                                      "div",
                                      { staticClass: "emoji-picker-container" },
                                      [
                                        _c(
                                          "div",
                                          { staticClass: "emoji-picker" },
                                          [
                                            _c(
                                              "div",
                                              {
                                                staticClass:
                                                  "emoji-picker__search"
                                              },
                                              [
                                                _c("input", {
                                                  directives: [
                                                    {
                                                      name: "model",
                                                      rawName: "v-model",
                                                      value:
                                                        _vm.searchTermEmoji,
                                                      expression:
                                                        "searchTermEmoji"
                                                    },
                                                    {
                                                      name: "focus",
                                                      rawName: "v-focus"
                                                    }
                                                  ],
                                                  attrs: { type: "text" },
                                                  domProps: {
                                                    value: _vm.searchTermEmoji
                                                  },
                                                  on: {
                                                    input: function($event) {
                                                      if (
                                                        $event.target.composing
                                                      ) {
                                                        return
                                                      }
                                                      _vm.searchTermEmoji =
                                                        $event.target.value
                                                    }
                                                  }
                                                })
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              _vm._l(emojis, function(
                                                emojiGroup,
                                                category
                                              ) {
                                                return _c(
                                                  "div",
                                                  {
                                                    key: category,
                                                    staticClass: "mt-2"
                                                  },
                                                  [
                                                    _c("h5", [
                                                      _vm._v(_vm._s(category))
                                                    ]),
                                                    _vm._v(" "),
                                                    _c(
                                                      "div",
                                                      { staticClass: "emojis" },
                                                      _vm._l(
                                                        emojiGroup,
                                                        function(
                                                          emoji,
                                                          emojiName
                                                        ) {
                                                          return _c(
                                                            "span",
                                                            {
                                                              key: emojiName,
                                                              attrs: {
                                                                title: emojiName
                                                              },
                                                              on: {
                                                                click: function(
                                                                  $event
                                                                ) {
                                                                  return insert(
                                                                    emoji
                                                                  )
                                                                }
                                                              }
                                                            },
                                                            [
                                                              _vm._v(
                                                                _vm._s(emoji)
                                                              )
                                                            ]
                                                          )
                                                        }
                                                      ),
                                                      0
                                                    )
                                                  ]
                                                )
                                              }),
                                              0
                                            )
                                          ]
                                        )
                                      ]
                                    )
                                  }
                                }
                              ],
                              null,
                              false,
                              1197881671
                            )
                          }),
                          _vm._v(" "),
                          !_vm.newMessage.message
                            ? _c("file-uploader", {
                                attrs: {
                                  url: "/chat/messages",
                                  "name-label": "upload.attachment",
                                  "names-label": "upload.attachments",
                                  "button-icon": "fas fa-paperclip",
                                  "button-classes": "btn-sm",
                                  "button-design": "custom",
                                  "hide-button-label": "",
                                  "hide-file-list": "",
                                  options: _vm.preRequisite.uploaderConfig,
                                  data: _vm.newMessage.attachments,
                                  "additional-form-data":
                                    _vm.activeConversation,
                                  "request-identifier":
                                    _vm.newMessageRequestUUID
                                },
                                on: {
                                  uploading: _vm.attachmentUploadStarted,
                                  updated: _vm.attachmentUploaded,
                                  error: _vm.attachmentUploadError,
                                  noNeed: _vm.noNeedCallback
                                }
                              })
                            : _vm._e(),
                          _vm._v(" "),
                          _c(
                            "button",
                            {
                              staticClass: "btn-custom post-message-button",
                              attrs: {
                                type: "button",
                                title: _vm.$t("general.submit")
                              },
                              on: { click: _vm.postMessage }
                            },
                            [_c("i", { staticClass: "far fa-paper-plane" })]
                          )
                        ],
                        1
                      )
                    ])
                  ],
                  1
                )
              : _vm._e()
          ])
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("button", { attrs: { type: "button" } }, [
      _c("i", { staticClass: "fas fa-ellipsis-h" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "scroll-btn-wrapper" }, [
      _c("i", { staticClass: "fas fa-angle-double-down" })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/message-item.vue?vue&type=template&id=5c5f8277&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/chat-box/message-item.vue?vue&type=template&id=5c5f8277& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "message-item-wrapper" }, [
    _c(
      "div",
      { class: ["message-item", { "has-attachments": _vm.hasAttachment }] },
      [
        _c("span", { staticClass: "tail-in" }, [
          _c(
            "svg",
            {
              attrs: {
                xmlns: "http://www.w3.org/2000/svg",
                viewBox: "0 0 8 13",
                width: "8",
                height: "13"
              }
            },
            [
              _c("path", {
                attrs: {
                  opacity: ".13",
                  fill: "#0000000",
                  d:
                    "M1.533 3.568L8 12.193V1H2.812C1.042 1 .474 2.156 1.533 3.568z"
                }
              }),
              _c("path", {
                attrs: {
                  fill: "currentColor",
                  d:
                    "M1.533 2.568L8 11.193V0H2.812C1.042 0 .474 1.156 1.533 2.568z"
                }
              })
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "message-header" }, [
          _c("h6", { staticClass: "message-user" }, [
            _vm._v(_vm._s(_vm.message.user.profile.name))
          ])
        ]),
        _vm._v(" "),
        _vm.hasAttachment
          ? _c(
              "div",
              { staticClass: "message-content" },
              [
                _vm._l(_vm.message.attachments, function(attachment) {
                  return [
                    _c(
                      "div",
                      {
                        key: attachment.uuid,
                        class: [
                          "attachment",
                          {
                            "non-image-attachment":
                              attachment.mime !== "image/png" &&
                              attachment.mime !== "image/jpeg"
                          }
                        ]
                      },
                      [
                        attachment.mime === "image/png" ||
                        attachment.mime === "image/jpeg"
                          ? [
                              _c(
                                "a",
                                {
                                  staticClass: "attachment-link",
                                  attrs: {
                                    href: attachment.fullUrl,
                                    target: "_blank"
                                  }
                                },
                                [
                                  _c("img", {
                                    staticClass: "img-fluid",
                                    attrs: {
                                      src: attachment.fullUrl,
                                      alt: attachment.filename
                                    }
                                  })
                                ]
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "attachment-shadow" }),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "message-footer small" },
                                [
                                  _vm.message.sentAt
                                    ? _c(
                                        "span",
                                        { staticClass: "message-created-at" },
                                        [
                                          _vm._v(
                                            _vm._s(
                                              _vm._f("momentCalendarTz")(
                                                _vm.message.sentAt
                                              )
                                            )
                                          )
                                        ]
                                      )
                                    : _vm._e(),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    {
                                      class: [
                                        "message-status",
                                        _vm.message.status
                                      ]
                                    },
                                    [
                                      _c("i", {
                                        class:
                                          _vm.messageStatusIcons[
                                            _vm.message.status
                                          ]
                                      })
                                    ]
                                  )
                                ]
                              )
                            ]
                          : [
                              _c(
                                "a",
                                {
                                  staticClass: "attachment-link",
                                  attrs: {
                                    href: attachment.fullUrl,
                                    target: "_blank"
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    { staticClass: "attachment-details" },
                                    [
                                      _c(
                                        "div",
                                        { staticClass: "attachment-icon" },
                                        [
                                          _c("i", {
                                            class: [
                                              "fas",
                                              _vm.fileIcons[attachment.mime]
                                            ]
                                          })
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        { staticClass: "attachment-name" },
                                        [
                                          _c("h6", [
                                            _vm._v(_vm._s(attachment.filename))
                                          ])
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _vm._m(0, true)
                                    ]
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "message-footer small" },
                                [
                                  _c("div", { staticClass: "left-side" }, [
                                    _c(
                                      "span",
                                      { staticClass: "attachment-type" },
                                      [
                                        _vm._v(
                                          _vm._s(
                                            _vm.fileMimeTypes[attachment.mime]
                                          )
                                        )
                                      ]
                                    ),
                                    _c(
                                      "span",
                                      { staticClass: "attachment-size" },
                                      [
                                        _vm._v(
                                          _vm._s(attachment.humanReadableSize)
                                        )
                                      ]
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "right-side" }, [
                                    _vm.message.sentAt
                                      ? _c(
                                          "span",
                                          { staticClass: "message-created-at" },
                                          [
                                            _vm._v(
                                              _vm._s(
                                                _vm._f("momentCalendarTz")(
                                                  _vm.message.sentAt
                                                )
                                              )
                                            )
                                          ]
                                        )
                                      : _vm._e(),
                                    _vm._v(" "),
                                    _c(
                                      "span",
                                      {
                                        class: [
                                          "message-status",
                                          _vm.message.status
                                        ]
                                      },
                                      [
                                        _c("i", {
                                          class:
                                            _vm.messageStatusIcons[
                                              _vm.message.status
                                            ]
                                        })
                                      ]
                                    )
                                  ])
                                ]
                              )
                            ]
                      ],
                      2
                    )
                  ]
                })
              ],
              2
            )
          : _c("div", { staticClass: "message-content" }, [
              _c("div", { staticClass: "message-body" }, [
                _c("div", {
                  staticClass: "message",
                  domProps: { innerHTML: _vm._s(_vm.message.message) }
                }),
                _vm._v(" "),
                _c("div", { staticClass: "footer-space" })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "message-footer small" }, [
                _vm.message.sentAt
                  ? _c("span", { staticClass: "message-created-at" }, [
                      _vm._v(
                        _vm._s(_vm._f("momentCalendarTz")(_vm.message.sentAt))
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _c("span", { class: ["message-status", _vm.message.status] }, [
                  _c("i", { class: _vm.messageStatusIcons[_vm.message.status] })
                ])
              ])
            ])
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "download-icon" }, [
      _c("i", { staticClass: "far fa-arrow-alt-circle-down" })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/room-item.vue?vue&type=template&id=7c922152&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/chat-box/room-item.vue?vue&type=template&id=7c922152& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "li",
    { class: ["room-item"], on: { click: _vm.goToConversation } },
    [
      _c(
        "div",
        { staticClass: "item-thumb" },
        [_c("user-avatar", { attrs: { user: _vm.computedAvatar, size: 40 } })],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "item-details" },
        [
          _c("div", { staticClass: "title-row" }, [
            _c("h6", { staticClass: "item-title" }, [
              _vm._v(_vm._s(_vm.roomName))
            ]),
            _vm._v(" "),
            _vm.room.lastConversationAt
              ? _c("small", { staticClass: "item-meta" }, [
                  _vm._v(
                    _vm._s(
                      _vm._f("momentCalendarTz")(_vm.room.lastConversationAt)
                    )
                  )
                ])
              : _vm._e()
          ]),
          _vm._v(" "),
          _vm.room.lastConversationAt
            ? [
                _vm.room.message
                  ? _c("div", { staticClass: "meta-row text-ellipsis" }, [
                      _c("span", {
                        domProps: { innerHTML: _vm._s(_vm.room.message) }
                      })
                    ])
                  : _c("div", { staticClass: "meta-row text-ellipsis" }, [
                      _c("span", [_vm._v(_vm._s(_vm.$t("upload.attachment")))])
                    ])
              ]
            : _vm._e(),
          _vm._v(" "),
          _vm.room.unreadMessages
            ? _c("div", { staticClass: "unread-messages" }, [
                _c(
                  "span",
                  { staticClass: "badge badge-sm badge-pill badge-light" },
                  [_vm._v(_vm._s(_vm.room.unreadMessages))]
                )
              ])
            : _vm._e()
        ],
        2
      )
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/settings.vue?vue&type=template&id=923098fe&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/chat-box/settings.vue?vue&type=template&id=923098fe& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "cb-inner-window settings-container" }, [
    _vm.activeContainer === "settings"
      ? _c("div", { staticClass: "settings-wrapper" }, [
          _c(
            "div",
            { staticClass: "settings" },
            [
              _c(
                "vue-scroll",
                {
                  ref: "vue-scroll-settings",
                  attrs: { id: "vue-scroll-settings" }
                },
                [
                  _c(
                    "div",
                    { staticClass: "settings-list" },
                    [
                      _c(
                        "switch-wrapper",
                        {
                          staticClass: "text-light",
                          attrs: {
                            label: _vm.$t("config.chat.enable_auto_open")
                          }
                        },
                        [
                          _c("base-switch", {
                            attrs: { design: "success" },
                            model: {
                              value: _vm.formData.chat.enableAutoOpen,
                              callback: function($$v) {
                                _vm.$set(
                                  _vm.formData.chat,
                                  "enableAutoOpen",
                                  $$v
                                )
                              },
                              expression: "formData.chat.enableAutoOpen"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "switch-wrapper",
                        {
                          staticClass: "text-light",
                          attrs: {
                            label: _vm.$t("config.chat.enter_to_submit")
                          }
                        },
                        [
                          _c("base-switch", {
                            attrs: { design: "success" },
                            model: {
                              value: _vm.formData.chat.enterToSubmit,
                              callback: function($$v) {
                                _vm.$set(
                                  _vm.formData.chat,
                                  "enterToSubmit",
                                  $$v
                                )
                              },
                              expression: "formData.chat.enterToSubmit"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "switch-wrapper",
                        {
                          staticClass: "text-light",
                          attrs: {
                            label: _vm.$t("config.chat.mute_sound_notification")
                          }
                        },
                        [
                          _c("base-switch", {
                            attrs: { design: "success" },
                            model: {
                              value: _vm.formData.chat.muteSoundNotification,
                              callback: function($$v) {
                                _vm.$set(
                                  _vm.formData.chat,
                                  "muteSoundNotification",
                                  $$v
                                )
                              },
                              expression: "formData.chat.muteSoundNotification"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "settings-footer" },
            [
              _c(
                "base-button",
                {
                  attrs: { type: "button", design: "primary", size: "block" },
                  on: { click: _vm.submit }
                },
                [_vm._v(_vm._s(_vm.$t("general.save")))]
              )
            ],
            1
          )
        ])
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/chat-box/conversation-header.vue":
/*!******************************************************************!*\
  !*** ./resources/js/components/chat-box/conversation-header.vue ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _conversation_header_vue_vue_type_template_id_2ce8a029___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./conversation-header.vue?vue&type=template&id=2ce8a029& */ "./resources/js/components/chat-box/conversation-header.vue?vue&type=template&id=2ce8a029&");
/* harmony import */ var _conversation_header_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./conversation-header.vue?vue&type=script&lang=js& */ "./resources/js/components/chat-box/conversation-header.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _conversation_header_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _conversation_header_vue_vue_type_template_id_2ce8a029___WEBPACK_IMPORTED_MODULE_0__["render"],
  _conversation_header_vue_vue_type_template_id_2ce8a029___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/chat-box/conversation-header.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/chat-box/conversation-header.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/chat-box/conversation-header.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_conversation_header_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./conversation-header.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/conversation-header.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_conversation_header_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/chat-box/conversation-header.vue?vue&type=template&id=2ce8a029&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/components/chat-box/conversation-header.vue?vue&type=template&id=2ce8a029& ***!
  \*************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_conversation_header_vue_vue_type_template_id_2ce8a029___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./conversation-header.vue?vue&type=template&id=2ce8a029& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/conversation-header.vue?vue&type=template&id=2ce8a029&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_conversation_header_vue_vue_type_template_id_2ce8a029___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_conversation_header_vue_vue_type_template_id_2ce8a029___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/chat-box/header.vue":
/*!*****************************************************!*\
  !*** ./resources/js/components/chat-box/header.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _header_vue_vue_type_template_id_b871232a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./header.vue?vue&type=template&id=b871232a& */ "./resources/js/components/chat-box/header.vue?vue&type=template&id=b871232a&");
/* harmony import */ var _header_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./header.vue?vue&type=script&lang=js& */ "./resources/js/components/chat-box/header.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _header_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _header_vue_vue_type_template_id_b871232a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _header_vue_vue_type_template_id_b871232a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/chat-box/header.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/chat-box/header.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/chat-box/header.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_header_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./header.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/header.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_header_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/chat-box/header.vue?vue&type=template&id=b871232a&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/chat-box/header.vue?vue&type=template&id=b871232a& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_header_vue_vue_type_template_id_b871232a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./header.vue?vue&type=template&id=b871232a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/header.vue?vue&type=template&id=b871232a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_header_vue_vue_type_template_id_b871232a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_header_vue_vue_type_template_id_b871232a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/chat-box/index.vue":
/*!****************************************************!*\
  !*** ./resources/js/components/chat-box/index.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index_vue_vue_type_template_id_7eb429e4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=7eb429e4& */ "./resources/js/components/chat-box/index.vue?vue&type=template&id=7eb429e4&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/components/chat-box/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/chat-box/index.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_7eb429e4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_7eb429e4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/chat-box/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/chat-box/index.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/chat-box/index.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/chat-box/index.vue?vue&type=style&index=0&lang=scss&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/chat-box/index.vue?vue&type=style&index=0&lang=scss& ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--9-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--9-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/index.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/chat-box/index.vue?vue&type=template&id=7eb429e4&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/chat-box/index.vue?vue&type=template&id=7eb429e4& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_7eb429e4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=7eb429e4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/index.vue?vue&type=template&id=7eb429e4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_7eb429e4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_7eb429e4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/chat-box/message-item.vue":
/*!***********************************************************!*\
  !*** ./resources/js/components/chat-box/message-item.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _message_item_vue_vue_type_template_id_5c5f8277___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./message-item.vue?vue&type=template&id=5c5f8277& */ "./resources/js/components/chat-box/message-item.vue?vue&type=template&id=5c5f8277&");
/* harmony import */ var _message_item_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./message-item.vue?vue&type=script&lang=js& */ "./resources/js/components/chat-box/message-item.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _message_item_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _message_item_vue_vue_type_template_id_5c5f8277___WEBPACK_IMPORTED_MODULE_0__["render"],
  _message_item_vue_vue_type_template_id_5c5f8277___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/chat-box/message-item.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/chat-box/message-item.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/chat-box/message-item.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_message_item_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./message-item.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/message-item.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_message_item_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/chat-box/message-item.vue?vue&type=template&id=5c5f8277&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/chat-box/message-item.vue?vue&type=template&id=5c5f8277& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_message_item_vue_vue_type_template_id_5c5f8277___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./message-item.vue?vue&type=template&id=5c5f8277& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/message-item.vue?vue&type=template&id=5c5f8277&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_message_item_vue_vue_type_template_id_5c5f8277___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_message_item_vue_vue_type_template_id_5c5f8277___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/chat-box/room-item.vue":
/*!********************************************************!*\
  !*** ./resources/js/components/chat-box/room-item.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _room_item_vue_vue_type_template_id_7c922152___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./room-item.vue?vue&type=template&id=7c922152& */ "./resources/js/components/chat-box/room-item.vue?vue&type=template&id=7c922152&");
/* harmony import */ var _room_item_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./room-item.vue?vue&type=script&lang=js& */ "./resources/js/components/chat-box/room-item.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _room_item_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _room_item_vue_vue_type_template_id_7c922152___WEBPACK_IMPORTED_MODULE_0__["render"],
  _room_item_vue_vue_type_template_id_7c922152___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/chat-box/room-item.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/chat-box/room-item.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/chat-box/room-item.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_room_item_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./room-item.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/room-item.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_room_item_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/chat-box/room-item.vue?vue&type=template&id=7c922152&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/chat-box/room-item.vue?vue&type=template&id=7c922152& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_room_item_vue_vue_type_template_id_7c922152___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./room-item.vue?vue&type=template&id=7c922152& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/room-item.vue?vue&type=template&id=7c922152&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_room_item_vue_vue_type_template_id_7c922152___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_room_item_vue_vue_type_template_id_7c922152___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/chat-box/settings.vue":
/*!*******************************************************!*\
  !*** ./resources/js/components/chat-box/settings.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _settings_vue_vue_type_template_id_923098fe___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./settings.vue?vue&type=template&id=923098fe& */ "./resources/js/components/chat-box/settings.vue?vue&type=template&id=923098fe&");
/* harmony import */ var _settings_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./settings.vue?vue&type=script&lang=js& */ "./resources/js/components/chat-box/settings.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _settings_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./settings.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/chat-box/settings.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _settings_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _settings_vue_vue_type_template_id_923098fe___WEBPACK_IMPORTED_MODULE_0__["render"],
  _settings_vue_vue_type_template_id_923098fe___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/chat-box/settings.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/chat-box/settings.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/chat-box/settings.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_settings_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./settings.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/settings.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_settings_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/chat-box/settings.vue?vue&type=style&index=0&lang=scss&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/chat-box/settings.vue?vue&type=style&index=0&lang=scss& ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_settings_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--9-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--9-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./settings.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/settings.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_settings_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_settings_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_settings_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_settings_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_settings_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/chat-box/settings.vue?vue&type=template&id=923098fe&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/chat-box/settings.vue?vue&type=template&id=923098fe& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_settings_vue_vue_type_template_id_923098fe___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./settings.vue?vue&type=template&id=923098fe& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/chat-box/settings.vue?vue&type=template&id=923098fe&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_settings_vue_vue_type_template_id_923098fe___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_settings_vue_vue_type_template_id_923098fe___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/core/filters/momentz.js":
/*!**********************************************!*\
  !*** ./resources/js/core/filters/momentz.js ***!
  \**********************************************/
/*! exports provided: momentDate, momentTime, momentDateTime, momentCalendar, momentTimeTz, momentDateTimeTz, momentCalendarTz, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "momentDate", function() { return momentDate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "momentTime", function() { return momentTime; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "momentDateTime", function() { return momentDateTime; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "momentCalendar", function() { return momentCalendar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "momentTimeTz", function() { return momentTimeTz; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "momentDateTimeTz", function() { return momentDateTimeTz; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "momentCalendarTz", function() { return momentCalendarTz; });
/* harmony import */ var _js_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @js/store */ "./resources/js/store/index.js");

var configs = _js_store__WEBPACK_IMPORTED_MODULE_0__["default"].getters['config/configs'];
var vars = _js_store__WEBPACK_IMPORTED_MODULE_0__["default"].getters['config/vars'];

var getActiveConfigs = function getActiveConfigs() {
  return {
    timezone: configs.system.timezone,
    defaultDateFormat: vars.defaultDateFormat,
    defaultDateTimeFormat: vars.defaultDateTimeFormat,
    serverDateFormat: vars.serverDateFormat,
    serverDateTimeFormat: vars.serverDateTimeFormat,
    calendarLocale: {
      // lastDay: '[Yesterday]',
      sameDay: vars.defaultTimeFormat,
      lastWeek: vars.defaultDateFormat,
      sameElse: vars.defaultDateFormat
    }
  };
};

var momentDate = function momentDate(value, formatOption) {
  var activeConfigs = getActiveConfigs();
  var momentObj = moment(value, activeConfigs.serverDateFormat);

  if (!value) {
    return 'INVALID DATE STRING';
  }

  if (Array.isArray(value)) {
    momentObj = moment(value[0], value[1]);
  }

  return momentObj.format(formatOption || activeConfigs.defaultDateFormat);
};
var momentTime = function momentTime(value, formatOption) {
  var activeConfigs = getActiveConfigs();
  var momentObj = moment(value, activeConfigs.serverTimeFormat);

  if (!value) {
    return 'INVALID TIME STRING';
  }

  if (Array.isArray(value)) {
    momentObj = moment(value[0], value[1]);
  }

  return momentObj.format(formatOption || activeConfigs.defaultTimeFormat);
};
var momentDateTime = function momentDateTime(value, formatOption) {
  var activeConfigs = getActiveConfigs();
  var momentObj = moment(value, activeConfigs.serverDateTimeFormat);

  if (!value) {
    return 'INVALID DATE TIME STRING';
  }

  if (Array.isArray(value)) {
    momentObj = moment(value[0], value[1]);
  }

  return momentObj.format(formatOption || activeConfigs.defaultDateTimeFormat);
};
var momentCalendar = function momentCalendar(value, calendarOption) {
  var activeConfigs = getActiveConfigs();
  var momentObj = moment(value, activeConfigs.serverDateTimeFormat);

  if (!value) {
    return 'INVALID DATE TIME STRING';
  }

  if (Array.isArray(value)) {
    momentObj = moment(value[0], value[1]);
  }

  return momentObj.calendar(calendarOption ? calendarOption : activeConfigs.calendarLocale);
};
var momentTimeTz = function momentTimeTz(value, formatOption, timezoneOption) {
  var activeConfigs = getActiveConfigs();
  var momentObj = moment.utc("".concat(moment().format(activeConfigs.serverDateFormat), " ").concat(value), activeConfigs.serverTimeFormat);

  if (!value) {
    return 'INVALID TIME STRING';
  }

  if (Array.isArray(value)) {
    momentObj = moment.utc(value[0], value[1]);
  }

  return momentObj.tz(timezoneOption || activeConfigs.timezone).format(formatOption || activeConfigs.defaultTimeFormat);
};
var momentDateTimeTz = function momentDateTimeTz(value, formatOption, timezoneOption) {
  var activeConfigs = getActiveConfigs();
  var momentObj = moment.utc(value, activeConfigs.serverDateTimeFormat);

  if (!value) {
    return 'INVALID DATE TIME STRING';
  }

  if (Array.isArray(value)) {
    momentObj = moment.utc(value[0], value[1]);
  }

  return momentObj.tz(timezoneOption || activeConfigs.timezone).format(formatOption || activeConfigs.defaultDateTimeFormat);
};
var momentCalendarTz = function momentCalendarTz(value) {
  var calendarOption = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  var timezoneOption = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
  var activeConfigs = getActiveConfigs();
  var momentObj = moment.utc(value, activeConfigs.serverDateTimeFormat);

  if (!value) {
    return 'INVALID DATE TIME STRING';
  }

  if (Array.isArray(value)) {
    momentObj = moment.utc(value[0], value[1]);
  }

  var result = momentObj.tz(timezoneOption || activeConfigs.timezone);
  result = result.calendar(calendarOption ? calendarOption : activeConfigs.calendarLocale);
  return result;
};
/* harmony default export */ __webpack_exports__["default"] = ({
  momentDate: momentDate,
  momentTime: momentTime,
  momentDateTime: momentDateTime,
  momentCalendar: momentCalendar,
  momentTimeTz: momentTimeTz,
  momentDateTimeTz: momentDateTimeTz,
  momentCalendarTz: momentCalendarTz
});

/***/ }),

/***/ "./resources/js/mixins/config-form.js":
/*!********************************************!*\
  !*** ./resources/js/mixins/config-form.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _api_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @api/config */ "./resources/js/api/config.js");
/* harmony import */ var _js_core_utils_formatter__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @js/core/utils/formatter */ "./resources/js/core/utils/formatter.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




/* harmony default export */ __webpack_exports__["default"] = ({
  components: {},
  data: function data() {
    return {
      formData: {},
      formErrors: {},
      initialFormData: null,
      initianLength: 0,
      emptyFormData: null,
      entity: null,
      preRequisite: {},
      isLoading: true,
      useUserPreference: false,
      dataType: null,
      configType: ''
    };
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapGetters"])('config', {
    configsWithPreference: 'configs',
    configs: 'configsOriginal',
    vars: 'vars'
  })),
  methods: _objectSpread(_objectSpread(_objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])('config', ['GetConfig'])), Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])('user', ['GetUser'])), Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapActions"])('navigation', ['Generate'])), {}, {
    findActualValue: function findActualValue(value, options) {
      var key = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'uuid';
      return options.find(function (option) {
        return option[key] === value;
      }) || null;
    },
    submit: function submit() {
      var _this = this;

      var preference = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      if (formUtil.isUnchanged(this.initialFormData, this.formData)) {
        this.$toasted.info(this.$t('general.nothing_changed'), this.$toastConfig.info);
        return false;
      }

      this.isLoading = true;

      if (this.formData.hasOwnProperty('type')) {
        this.formData.type = this.formData.type.snakeCase();
      }

      var storeFn = preference === true || this.useUserPreference ? _api_config__WEBPACK_IMPORTED_MODULE_2__["updateUserPref"] : this.configType === 'module' ? _api_config__WEBPACK_IMPORTED_MODULE_2__["updateModule"] : _api_config__WEBPACK_IMPORTED_MODULE_2__["update"];

      if (typeof this.beforeSubmit === "function") {
        this.beforeSubmit();
      }

      storeFn(this.formData).then(function (response) {
        var fnToCall = preference === true || _this.useUserPreference ? _this.GetUser : _this.GetConfig;
        fnToCall().then(function () {
          _this.$toasted.success(response.message, _this.$toastConfig);

          _this.initialFormData = _.cloneDeep(_this.formData);
          _this.isLoading = false;
        })["catch"](function (error) {
          _this.isLoading = false;
          _this.formErrors = formUtil.handleErrors(error);
        });

        if (typeof _this.afterSubmit === "function") {
          _this.afterSubmit();
        }

        if (typeof _this.afterSubmitSuccess === "function") {
          _this.afterSubmitSuccess();
        }
      })["catch"](function (error) {
        _this.isLoading = false;
        _this.formErrors = formUtil.handleErrors(error);

        if (typeof _this.afterSubmit === "function") {
          _this.afterSubmit();
        }

        if (typeof _this.afterSubmitError === "function") {
          _this.afterSubmitError();
        }
      });
    },
    reset: function reset() {
      var _this2 = this;

      formUtil.confirmAction().then(function (result) {
        if (result.value) {
          _this2.formData = Object.assign({}, _this2.formData, _.cloneDeep(_this2.initialFormData));
        }
      });
    },
    unsavedCheck: function unsavedCheck(next) {
      formUtil.unsavedCheckAlert(this.initialFormData, this.formData, next);
    },
    fillPreRequisite: function fillPreRequisite(response) {
      var _this3 = this;

      this.preRequisite.objForEach(function (value, key) {
        _this3.preRequisite[key] = response.hasOwnProperty(key) ? response[key] : value;
      });
      this.isLoading = false;
    },
    fillFormData: function fillFormData() {
      var _this4 = this;

      this.isLoading = true;
      var configsToUse = this.useUserPreference ? this.configsWithPreference : this.configs;

      if (this.formData.type && configsToUse[this.formData.type]) {
        this.formData = formUtil.assignValues(this.formData, configsToUse[this.formData.type]);
      }

      if (this.formData.types && Array.isArray(this.formData.types)) {
        this.formData.types.forEach(function (value) {
          _this4.formData = formUtil.assignValues(_this4.formData, configsToUse[value]);
        });
      }

      if (this.formData.override) {
        this.formData.objForEach(function (value, key) {
          if (value && _.isObject(value)) {
            _this4.formData[key] = formUtil.assignValues(_this4.formData[key], configsToUse[key]);
          } else {
            _this4.formData[key] = configsToUse[key] ? configsToUse[key] : value;
          }
        });
      }

      if (typeof this.addNewRow === "function" && typeof this.addNewRowIfNone === "function") {
        this.addNewRowIfNone();
      }

      this.isLoading = false;
    },
    getInitialData: function getInitialData(callbackFn) {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _this5.isLoading = true;
                _context.prev = 1;
                _context.next = 4;
                return _api_config__WEBPACK_IMPORTED_MODULE_2__["getPreRequisite"](Object.keys(_this5.preRequisite).join(','));

              case 4:
                response = _context.sent;

                _this5.fillPreRequisite(response);

                if (callbackFn) {
                  _this5.$nextTick(function () {
                    callbackFn();
                  });
                }

                return _context.abrupt("return", response);

              case 10:
                _context.prev = 10;
                _context.t0 = _context["catch"](1);
                _this5.isLoading = false;
                _this5.formErrors = formUtil.handleErrors(_context.t0);
                throw _context.t0;

              case 15:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[1, 10]]);
      }))();
    }
  }),
  mounted: function mounted() {
    this.fillFormData();
    this.initialFormData = _.cloneDeep(this.formData);
  },
  beforeDestroy: function beforeDestroy() {
    delete this.formData;
    delete this.formErrors;
    delete this.preRequisite;
  },
  beforeRouteLeave: function beforeRouteLeave(to, from, next) {
    this.unsavedCheck(next);
  }
});

/***/ }),

/***/ "./resources/js/mixins/live.js":
/*!*************************************!*\
  !*** ./resources/js/mixins/live.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _js_echo_setup__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @js/echo-setup */ "./resources/js/echo-setup.js");
/* harmony import */ var _core_utils_formatter__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @core/utils/formatter */ "./resources/js/core/utils/formatter.js");
/* harmony import */ var _core_utils_media__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @core/utils/media */ "./resources/js/core/utils/media.js");
/* harmony import */ var _core_utils_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @core/utils/auth */ "./resources/js/core/utils/auth.js");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! socket.io-client */ "./node_modules/socket.io-client/lib/index.js");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(socket_io_client__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _js_event_bus__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @js/event-bus */ "./resources/js/event-bus.js");
/* harmony import */ var _components_chat_box_index_vue__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @components/chat-box/index.vue */ "./resources/js/components/chat-box/index.vue");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }









/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    ChatBox: _components_chat_box_index_vue__WEBPACK_IMPORTED_MODULE_7__["default"]
  },
  data: function data() {
    return {
      subscriptions: {},
      isChatBoxShown: false,
      socketURL: 'aHR0cHM6Ly9zaWduYWwua29kZW1pbnQuaW46OTAwMS8=' // socketURL: 'aHR0cDovL2xvY2FsaG9zdDo5MDAxLw==',

    };
  },
  computed: _objectSpread(_objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('config', ['configs', 'uiConfigs', 'vars'])), Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('user', ['loggedInUser', 'liveUsers'])), Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('chat', ['roomsPageMeta', 'totalUnreadMessages', 'chatBoxLoaded'])),
  watch: {
    liveMembersCount: function liveMembersCount(newVal, oldVal) {
      if (!window.isLiveMeetingDestroyed && newVal !== oldVal) {
        this.meetingRoomCreated(this.entity);
      }
    }
  },
  methods: _objectSpread(_objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])('user', ['SetLastActivity', 'SetLiveUsers', 'AddLiveUser', 'RemoveLiveUser', 'UpdateLiveUser', 'ResetLiveUsers'])), Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])('chat', ['GetRooms', 'MessageReceived', 'ResetTotalUnreadCount'])), {}, {
    showChatBox: function showChatBox() {
      this.$gaEvent('engagement', 'showChatBox');
      this.isChatBoxShown = true;
      this.ResetTotalUnreadCount();
    },
    toggleChatBox: function toggleChatBox() {
      this.$gaEvent('engagement', 'toggleChatBox');
      this.isChatBoxShown = !this.isChatBoxShown; // this.ResetTotalUnreadCount()
    },
    afterJoiningUsersChannel: function afterJoiningUsersChannel(users) {
      this.ResetLiveUsers();
      this.SetLiveUsers(Object(_core_utils_formatter__WEBPACK_IMPORTED_MODULE_2__["formatKeysToCamelCase"])(users));
    },
    userIsOnline: function userIsOnline(user) {
      if (this.liveUsers.findIndex(function (u) {
        return u.uuid === user.uuid;
      }) !== -1) {
        this.RemoveLiveUser(user);
      }

      this.AddLiveUser(Object(_core_utils_formatter__WEBPACK_IMPORTED_MODULE_2__["formatKeysToCamelCase"])(user));
    },
    userIsOffline: function userIsOffline(user) {
      if (window.callTo) {
        this.callDroppedNotification({
          from: this.loggedInUser,
          to: user
        });
      }

      this.RemoveLiveUser(user);
    },
    updateOnlineUser: function updateOnlineUser(user) {
      this.UpdateLiveUser(Object(_core_utils_formatter__WEBPACK_IMPORTED_MODULE_2__["formatKeysToCamelCase"])(user));
    },
    setUserStatus: function setUserStatus() {
      var status = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      var timerToFalse = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      this.UpdateLiveUser(_objectSpread(_objectSpread({}, this.loggedInUser), {}, {
        busy: status,
        timerToFalse: timerToFalse
      }));
      this.subscriptions.users.whisper('SetUserStatus', _objectSpread(_objectSpread({}, this.loggedInUser), {}, {
        busy: status
      }));
    },
    // Message Related
    newMessageReceived: function newMessageReceived(chatMessage) {
      if (!chatMessage || !(this.configs.chat && this.configs.chat.enabled)) {
        return;
      }

      chatMessage = Object(_core_utils_formatter__WEBPACK_IMPORTED_MODULE_2__["formatKeysToCamelCase"])(chatMessage);

      if (chatMessage && chatMessage.user && chatMessage.user.uuid !== this.loggedInUser.uuid) {
        this.MessageReceived(chatMessage);

        if (!this.isChatBoxShown && !this.configs.chat.muteSoundNotification) {
          Object(_core_utils_media__WEBPACK_IMPORTED_MODULE_3__["playIncomingMessage"])();
        }
      }
    },
    startChatWith: function startChatWith(withUser) {
      if (this.configs.chat && this.configs.chat.enabled && !this.isChatBoxShown) {
        this.isChatBoxShown = true;
      }
    },
    leaveBeforeUnload: function leaveBeforeUnload() {
      if (window && window.Echo) {
        window.Echo.leave('Users');
        window.Echo.leave("User.".concat(this.loggedInUser.uuid));
      }
    },
    socketSetup: function socketSetup() {
      this.socketURLB = window.atob(this.socketURL);
      var ioSocket = socket_io_client__WEBPACK_IMPORTED_MODULE_5___default()(this.socketURLB);
      ioSocket.on('connect', function () {
        ioSocket.emit('socketAuth', {
          token: 201005
        });
      });
      ioSocket.on('socketAuthError', function () {
        Object(_core_utils_auth__WEBPACK_IMPORTED_MODULE_4__["clearStore"])(true);
      });
      ioSocket.on('socketAuthSuccess', function () {
        ioSocket.disconnect();
      });
      window.setTimeout(function () {
        if (ioSocket) {
          ioSocket.disconnect();
        }
      }, 3000);
    }
  }),
  mounted: function mounted() {
    var _this = this;

    if (!window.Echo) {
      Object(_js_echo_setup__WEBPACK_IMPORTED_MODULE_1__["setupPusher"])();
    }

    if (window.Echo && window.EchoOpts) {
      window.EchoOpts.subscriptions.users = window.Echo.join("Users");
      window.EchoOpts.subscriptions.userPrivate = window.Echo["private"]("User.".concat(this.loggedInUser.uuid));
      this.subscriptions = {
        users: window.EchoOpts.subscriptions.users,
        userPrivate: window.EchoOpts.subscriptions.userPrivate
      };
      this.ResetTotalUnreadCount();
      this.ResetLiveUsers();
      this.subscriptions.userPrivate.listen('ChatMessageSentToUser', this.newMessageReceived);
      this.subscriptions.users.here(this.afterJoiningUsersChannel).joining(this.userIsOnline).leaving(this.userIsOffline).listenForWhisper('SetUserStatus', this.updateOnlineUser);
      this.GetRooms(_objectSpread(_objectSpread({}, this.roomsPageMeta), {}, {
        lastItemUuid: null
      }));
      _js_event_bus__WEBPACK_IMPORTED_MODULE_6__["default"].$off('START_CHAT_WITH', this.startChatWith);
      _js_event_bus__WEBPACK_IMPORTED_MODULE_6__["default"].$on('START_CHAT_WITH', this.startChatWith);
    }

    if (!(this.configs && this.configs.disableSocketVerification)) {
      window.setTimeout(function () {
        _this.socketSetup();
      }, 1000);
    }
  },
  created: function created() {
    window.addEventListener('beforeunload', this.leaveBeforeUnload);
  },
  beforeDestroy: function beforeDestroy() {
    if (window.Echo) {
      if (window.EchoOpts) {
        window.EchoOpts.subscriptions.users = null;

        if (window.EchoOpts.subscriptions.privateUser) {
          window.EchoOpts.subscriptions.privateUser.stopListening('ChatMessageSentToUser');
          window.EchoOpts.subscriptions.privateUser = null;
        }

        if (window.EchoOpts.subscriptions.chatRoom) {
          window.EchoOpts.subscriptions.chatRoom.stopListening('ChatMessageSentToRoom');
          window.EchoOpts.subscriptions.chatRoom = null;
        }
      }

      this.subscriptions = {};
      window.Echo.leave('Users');
      window.Echo.leave("User.".concat(this.loggedInUser.uuid));
    }
  },
  destroyed: function destroyed() {
    _js_event_bus__WEBPACK_IMPORTED_MODULE_6__["default"].$off('START_CHAT_WITH', this.startChatWith);
    window.removeEventListener('beforeunload', this.leaveBeforeUnload);
  }
});

/***/ }),

/***/ "./resources/js/mixins/user-dropdown.js":
/*!**********************************************!*\
  !*** ./resources/js/mixins/user-dropdown.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var vue_avatar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-avatar */ "./node_modules/vue-avatar/dist/vue-avatar.min.js");
/* harmony import */ var vue_avatar__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_avatar__WEBPACK_IMPORTED_MODULE_1__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Avatar: vue_avatar__WEBPACK_IMPORTED_MODULE_1___default.a
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('config', ['configs', 'uiConfigs', 'vars'])), Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('user', {
    getUserDetails: 'get',
    hasRole: 'hasRole',
    hasPermission: 'hasPermission',
    loggedInUser: 'loggedInUser',
    userUuid: 'uuid',
    profile: 'profile',
    username: 'username',
    locked: 'locked'
  })),
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])('user', ['Logout', 'Lock'])), {}, {
    lock: function lock() {
      this.$gaEvent('engagement', 'lockScreen', 'Navbar');
      this.Lock();
    },
    logout: function logout() {
      var _this = this;

      this.$gaEvent('engagement', 'logout', 'Navbar');
      this.Logout().then(function (response) {
        _this.$gaEvent('activity', 'loggedout'); // unregisterModules()
        // this.$toasted.success(response.message, this.$toastConfig)


        _this.$router.push({
          name: 'login',
          query: {
            logout: 'true'
          }
        });
      });
    }
  })
});

/***/ })

}]);
//# sourceMappingURL=app-layout.js.map?id=e554f687b82756309870