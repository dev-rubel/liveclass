(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/app/call/index"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/FilterWrapper.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/FilterWrapper.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    boxed: {
      type: Boolean,
      "default": false
    },
    isLoading: {
      type: Boolean,
      "default": false
    }
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('common', ['filters'])), Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('config', ['vars'])),
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])('common', ['ResetFilters'])), {}, {
    submit: function submit() {
      var filterQuery = _objectSpread(_objectSpread(_objectSpread({}, this.$route.query), this.filters), {}, {
        filtered: true,
        filtered_at: moment().unix()
      });

      this.$router.push({
        path: this.$route.path,
        query: filterQuery
      })["catch"](function (err) {});
    },
    reset: function reset() {
      this.ResetFilters();

      if (this.$route.query && this.$route.query.filtered) {
        this.$router.push({
          path: this.$route.path
        });
      }
    }
  })
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/call/filter.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/app/call/filter.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _mixins_filter_form__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @mixins/filter-form */ "./resources/js/mixins/filter-form.js");
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": _mixins_filter_form__WEBPACK_IMPORTED_MODULE_0__["default"]
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/call/index.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/app/call/index.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _mixins_table__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @mixins/table */ "./resources/js/mixins/table.js");
/* harmony import */ var _filter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./filter */ "./resources/js/views/app/call/filter.vue");
/* harmony import */ var _js_helpers_transformers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @js/helpers/transformers */ "./resources/js/helpers/transformers.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    FilterForm: _filter__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  "extends": _mixins_table__WEBPACK_IMPORTED_MODULE_0__["default"],
  data: function data() {
    return {
      fields: [{
        key: 'title',
        label: $t('meeting.props.title'),
        sort: 'title',
        transformer: 'limitWords',
        tdClass: 'td-ellipsis max-width-250px'
      }, {
        key: 'user.name',
        label: $t('user.user')
      }, {
        key: 'agenda',
        label: $t('meeting.props.agenda'),
        transformer: 'limitWords',
        tdClass: 'd-none td-ellipsis max-width-100px',
        thClass: 'd-none'
      }, {
        key: 'type',
        label: $t('meeting.props.type'),
        transformer: 'objectWithName'
      }, {
        key: 'category',
        label: $t('meeting.meeting_category.category'),
        transformer: 'objectWithName'
      }, {
        key: 'status',
        label: $t('meeting.props.status')
      }, {
        key: 'startDateTime',
        label: $t('meeting.props.scheduled_on'),
        sort: 'start_date_time',
        transformer: 'datetime'
      }, {
        key: 'period',
        label: $t('meeting.props.estimated_period'),
        thClass: 'd-none',
        tdClass: 'd-none'
      }, {
        key: 'createdAt',
        label: $t('general.created_at'),
        sort: 'created_at',
        transformer: 'datetime',
        thClass: 'd-none',
        tdClass: 'd-none'
      }, {
        key: 'updatedAt',
        label: $t('general.updated_at'),
        sort: 'updated_at',
        transformer: 'datetime',
        thClass: 'd-none',
        tdClass: 'd-none'
      }, {
        key: 'actions',
        label: '',
        cantHide: true,
        tdClass: 'actions-dropdown-wrapper'
      }],
      filtersOptions: {
        name: ''
      },
      sortOptions: {
        hasScroll: true
      },
      columnsOptions: {
        hasScroll: true
      },
      exportOptions: {
        orientation: 'l'
      },
      permissionsRequired: {
        add: 'create-meeting',
        config: 'access-meeting-config'
      },
      routesRequired: {
        add: 'appMeetingAdd',
        config: 'appMeetingConfigGeneral'
      },
      initUrl: 'meetings',
      dataType: 'meeting'
    };
  },
  methods: {
    sameDate: function sameDate(_ref) {
      var startDateTime = _ref.startDateTime,
          endDateTime = _ref.endDateTime;
      return moment(startDateTime, this.vars.serverDateTimeFormat).format(this.vars.defaultDateFormat) === moment(endDateTime, this.vars.serverDateTimeFormat).format(this.vars.defaultDateFormat);
    }
  },
  mounted: function mounted() {
    this.updatePageMeta();
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/FilterWrapper.vue?vue&type=template&id=3b6aa020&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/FilterWrapper.vue?vue&type=template&id=3b6aa020& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "base-container",
    {
      staticClass: "mb-element",
      attrs: {
        boxed: _vm.boxed,
        "with-loader": "",
        "is-loading": _vm.isLoading,
        "loader-color": _vm.vars.loaderColor
      }
    },
    [
      _c("close-button", {
        attrs: { title: _vm.$t("general.close") },
        on: {
          click: function($event) {
            return _vm.$emit("close")
          }
        }
      }),
      _vm._v(" "),
      _c(
        "form",
        {
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.submit($event)
            }
          }
        },
        [
          _vm._t("default"),
          _vm._v(" "),
          _c("div", { staticClass: "form-footer mt-3" }, [
            _c(
              "div",
              { staticClass: "left-side" },
              [
                _c(
                  "base-button",
                  {
                    attrs: {
                      type: "button",
                      design: "light",
                      disabled: _vm.isLoading
                    },
                    on: {
                      click: function($event) {
                        return _vm.$emit("close")
                      }
                    }
                  },
                  [_vm._v(_vm._s(_vm.$t("general.close")))]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "right-side" },
              [
                _c(
                  "base-button",
                  {
                    attrs: {
                      type: "button",
                      design: "light",
                      disabled: _vm.isLoading
                    },
                    on: { click: _vm.reset }
                  },
                  [_vm._v(_vm._s(_vm.$t("general.clear")))]
                ),
                _vm._v(" "),
                _c(
                  "base-button",
                  {
                    attrs: {
                      type: "submit",
                      design: "primary",
                      disabled: _vm.isLoading
                    }
                  },
                  [_vm._v(_vm._s(_vm.$t("general.filter")))]
                )
              ],
              1
            )
          ])
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/call/filter.vue?vue&type=template&id=0ee5172c&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/app/call/filter.vue?vue&type=template&id=0ee5172c& ***!
  \*************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "filter-wrapper",
    {
      attrs: { boxed: _vm.boxed, "is-loading": _vm.isLoading },
      on: {
        close: function($event) {
          return _vm.$emit("close")
        }
      }
    },
    [
      _c("div", { staticClass: "row" }, [
        _c(
          "div",
          { staticClass: "col-12 col-md-3 mb-4" },
          [
            _c("base-input", {
              attrs: {
                "auto-focus": "",
                label: _vm.$t("meeting.props.name"),
                type: "text",
                disabled: _vm.isLoading
              },
              model: {
                value: _vm.filters.name,
                callback: function($$v) {
                  _vm.$set(_vm.filters, "name", $$v)
                },
                expression: "filters.name"
              }
            })
          ],
          1
        )
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/call/index.vue?vue&type=template&id=3e9ff9ce&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/app/call/index.vue?vue&type=template&id=3e9ff9ce& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "entity-list-container" },
    [
      _c(
        "collapse-transition",
        { attrs: { group: true, duration: 300, tag: "div" } },
        [
          _vm.showFilters
            ? _c("filter-form", {
                key: "filters",
                attrs: {
                  boxed: true,
                  "pre-requisite": _vm.preRequisite,
                  "is-loading": _vm.isLoading
                },
                on: { close: _vm.toggleFilter }
              })
            : _vm._e(),
          _vm._v(" "),
          _c(
            "base-container",
            {
              key: "list",
              staticClass: "p-0  zoom-print-85",
              attrs: {
                boxed: "",
                "with-loader": "",
                "is-loading": _vm.isLoading,
                "loader-color": _vm.vars.loaderColor
              }
            },
            [
              _vm.isInitialized
                ? _c(
                    "table-wrapper",
                    {
                      attrs: {
                        meta: _vm.entities.meta,
                        filtered: _vm.isFiltered,
                        "add-button-route": "appMeetingAdd",
                        "add-button-permissions": ["create-meeting"],
                        "entity-title": "meeting.meeting",
                        "entities-title": "meeting.meetings",
                        "entity-description": "meeting.module_description"
                      }
                    },
                    [
                      _c("b-table", {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.entities.meta.total,
                            expression: "entities.meta.total"
                          }
                        ],
                        ref: "btable",
                        attrs: {
                          items: _vm.itemsProvider,
                          fields: _vm.fields,
                          busy: _vm.isLoading,
                          hover: "",
                          striped: "",
                          stacked: "sm",
                          "per-page": _vm.entities.meta.perPage,
                          "current-page": _vm.entities.meta.currentPage,
                          filters: null
                        },
                        on: {
                          "update:busy": function($event) {
                            _vm.isLoading = $event
                          },
                          "row-dblclicked": function($event) {
                            return _vm.rowClickHandler(
                              { route: "appMeetingView" },
                              $event
                            )
                          }
                        },
                        scopedSlots: _vm._u(
                          [
                            {
                              key: "cell(title)",
                              fn: function(row) {
                                return [
                                  _c(
                                    "router-link",
                                    {
                                      staticClass: "row-link",
                                      attrs: {
                                        to: {
                                          name: "appMeetingView",
                                          params: { uuid: row.item.uuid }
                                        }
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n                            " +
                                          _vm._s(row.item.title) +
                                          "\n                        "
                                      )
                                    ]
                                  )
                                ]
                              }
                            },
                            {
                              key: "cell(createdAt)",
                              fn: function(row) {
                                return [
                                  _c("view-date", {
                                    staticClass: "mb-0",
                                    attrs: {
                                      value: row.item.createdAt,
                                      "to-format":
                                        _vm.vars.defaultDateTimeFormat,
                                      tag: "span"
                                    }
                                  })
                                ]
                              }
                            },
                            {
                              key: "cell(updatedAt)",
                              fn: function(row) {
                                return [
                                  _c("view-date", {
                                    staticClass: "mb-0",
                                    attrs: {
                                      value: row.item.updatedAt,
                                      "to-format":
                                        _vm.vars.defaultDateTimeFormat,
                                      tag: "span"
                                    }
                                  })
                                ]
                              }
                            },
                            {
                              key: "cell(startDateTime)",
                              fn: function(row) {
                                return [
                                  _c("view-date", {
                                    staticClass: "mb-0",
                                    attrs: {
                                      value: row.item.startDateTime,
                                      "to-format":
                                        _vm.vars.defaultDateTimeFormat,
                                      tag: "span"
                                    }
                                  })
                                ]
                              }
                            },
                            {
                              key: "cell(period)",
                              fn: function(row) {
                                return [
                                  _c("view-single", {
                                    staticClass: "mb-0",
                                    attrs: {
                                      value: row.item.period,
                                      tag: "span"
                                    }
                                  })
                                ]
                              }
                            },
                            {
                              key: "cell(startDatedTime)",
                              fn: function(row) {
                                return [
                                  _vm.sameDate(row.item)
                                    ? _c(
                                        "div",
                                        { staticClass: "d-flex" },
                                        [
                                          _c("view-date", {
                                            staticClass: "mb-0",
                                            attrs: {
                                              value: row.item.startDateTime,
                                              "to-format":
                                                _vm.vars.defaultDateFormat,
                                              tag: "span"
                                            }
                                          }),
                                          _vm._v(" "),
                                          _c("span", { staticClass: "mx-2" }, [
                                            _vm._v("-")
                                          ]),
                                          _vm._v(" "),
                                          _c("view-date", {
                                            staticClass: "mb-0",
                                            attrs: {
                                              value: row.item.startDateTime,
                                              "to-format":
                                                _vm.vars.defaultTimeFormat,
                                              tag: "span"
                                            }
                                          }),
                                          _vm._v(" "),
                                          _c("span", { staticClass: "mx-2" }, [
                                            _vm._v(_vm._s(_vm.$t("general.to")))
                                          ]),
                                          _vm._v(" "),
                                          _c("view-date", {
                                            staticClass: "mb-0",
                                            attrs: {
                                              value: row.item.endDateTime,
                                              "to-format":
                                                _vm.vars.defaultTimeFormat,
                                              tag: "span"
                                            }
                                          })
                                        ],
                                        1
                                      )
                                    : _c(
                                        "div",
                                        { staticClass: "d-flex" },
                                        [
                                          _c("view-date", {
                                            staticClass: "mb-0",
                                            attrs: {
                                              value: row.item.startDateTime,
                                              "to-format":
                                                _vm.vars.defaultDateTimeFormat,
                                              tag: "span"
                                            }
                                          }),
                                          _vm._v(" "),
                                          _c("span", { staticClass: "mx-2" }, [
                                            _vm._v(_vm._s(_vm.$t("general.to")))
                                          ]),
                                          _vm._v(" "),
                                          _c("view-date", {
                                            staticClass: "mb-0",
                                            attrs: {
                                              value: row.item.endDateTime,
                                              "to-format":
                                                _vm.vars.defaultDateTimeFormat,
                                              tag: "span"
                                            }
                                          })
                                        ],
                                        1
                                      )
                                ]
                              }
                            },
                            {
                              key: "cell(type)",
                              fn: function(row) {
                                return [
                                  row.item.type
                                    ? _c(
                                        "badge",
                                        {
                                          staticClass: "mb-0",
                                          attrs: { type: "default" }
                                        },
                                        [_vm._v(_vm._s(row.item.type.name))]
                                      )
                                    : _vm._e()
                                ]
                              }
                            },
                            {
                              key: "cell(category)",
                              fn: function(row) {
                                return [
                                  row.item.category
                                    ? _c(
                                        "badge",
                                        {
                                          staticClass: "mb-0",
                                          attrs: { type: "dark" }
                                        },
                                        [_vm._v(_vm._s(row.item.category.name))]
                                      )
                                    : _vm._e()
                                ]
                              }
                            },
                            {
                              key: "cell(status)",
                              fn: function(row) {
                                return [
                                  row.item.status
                                    ? _c(
                                        "badge",
                                        {
                                          attrs: {
                                            rounded: "",
                                            type:
                                              _vm.vars.colorsForStatus[
                                                "meeting-" + row.item.status
                                              ]
                                          }
                                        },
                                        [_vm._v(_vm._s(row.item.status))]
                                      )
                                    : _vm._e()
                                ]
                              }
                            },
                            {
                              key: "cell(actions)",
                              fn: function(row) {
                                return [
                                  _c(
                                    "table-row-actions",
                                    [
                                      _c(
                                        "router-link",
                                        {
                                          staticClass: "dropdown-item",
                                          attrs: {
                                            to: {
                                              name: "appMeetingView",
                                              params: { uuid: row.item.uuid }
                                            }
                                          }
                                        },
                                        [
                                          _c("i", {
                                            staticClass:
                                              "fas fa-arrow-circle-right"
                                          }),
                                          _vm._v(
                                            " " +
                                              _vm._s(
                                                _vm.$t("global.view", {
                                                  attribute: _vm.$t(
                                                    "meeting.meeting"
                                                  )
                                                })
                                              )
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      row.item.status === "scheduled" &&
                                      (_vm.hasPermission("create-meeting") ||
                                        row.item.canModerate)
                                        ? _c(
                                            "router-link",
                                            {
                                              staticClass: "dropdown-item",
                                              attrs: {
                                                to: {
                                                  name: "appMeetingEdit",
                                                  params: {
                                                    uuid: row.item.uuid
                                                  }
                                                }
                                              }
                                            },
                                            [
                                              _c("i", {
                                                staticClass: "fas fa-edit"
                                              }),
                                              _vm._v(
                                                " " +
                                                  _vm._s(
                                                    _vm.$t("global.edit", {
                                                      attribute: _vm.$t(
                                                        "meeting.meeting"
                                                      )
                                                    })
                                                  )
                                              )
                                            ]
                                          )
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _vm.hasPermission("create-meeting")
                                        ? _c(
                                            "router-link",
                                            {
                                              staticClass: "dropdown-item",
                                              attrs: {
                                                to: {
                                                  name: "appMeetingDuplicate",
                                                  params: {
                                                    uuid: row.item.uuid
                                                  }
                                                }
                                              }
                                            },
                                            [
                                              _c("i", {
                                                staticClass: "fas fa-copy"
                                              }),
                                              _vm._v(
                                                " " +
                                                  _vm._s(
                                                    _vm.$t("global.duplicate", {
                                                      attribute: _vm.$t(
                                                        "meeting.meeting"
                                                      )
                                                    })
                                                  )
                                              )
                                            ]
                                          )
                                        : _vm._e(),
                                      _vm._v(" "),
                                      row.item.status === "scheduled" &&
                                      _vm.hasPermission("list-meeting") &&
                                        row.item.canModerate
                                        ? _c(
                                            "router-link",
                                            {
                                              staticClass: "dropdown-item",
                                              attrs: {
                                                to: {
                                                  name:
                                                    "appMeetingSingleConfig",
                                                  params: {
                                                    subUuid: row.item.uuid
                                                  }
                                                }
                                              }
                                            },
                                            [
                                              _c("i", {
                                                staticClass: "fas fa-cog"
                                              }),
                                              _vm._v(
                                                " " +
                                                  _vm._s(
                                                    _vm.$t("global.config", {
                                                      attribute: _vm.$t(
                                                        "meeting.meeting"
                                                      )
                                                    })
                                                  )
                                              )
                                            ]
                                          )
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _vm.hasPermission("delete-meeting")
                                        ? _c(
                                            "a",
                                            {
                                              staticClass: "dropdown-item",
                                              on: {
                                                click: function($event) {
                                                  $event.stopPropagation()
                                                  return _vm.deleteEntity(
                                                    row.item
                                                  )
                                                }
                                              }
                                            },
                                            [
                                              _c("i", {
                                                staticClass: "fas fa-trash"
                                              }),
                                              _vm._v(
                                                " " +
                                                  _vm._s(
                                                    _vm.$t("global.delete", {
                                                      attribute: _vm.$t(
                                                        "meeting.meeting"
                                                      )
                                                    })
                                                  )
                                              )
                                            ]
                                          )
                                        : _vm._e()
                                    ],
                                    1
                                  )
                                ]
                              }
                            }
                          ],
                          null,
                          false,
                          2019634671
                        )
                      })
                    ],
                    1
                  )
                : _vm._e()
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/FilterWrapper.vue":
/*!***************************************************!*\
  !*** ./resources/js/components/FilterWrapper.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _FilterWrapper_vue_vue_type_template_id_3b6aa020___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./FilterWrapper.vue?vue&type=template&id=3b6aa020& */ "./resources/js/components/FilterWrapper.vue?vue&type=template&id=3b6aa020&");
/* harmony import */ var _FilterWrapper_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./FilterWrapper.vue?vue&type=script&lang=js& */ "./resources/js/components/FilterWrapper.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _FilterWrapper_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _FilterWrapper_vue_vue_type_template_id_3b6aa020___WEBPACK_IMPORTED_MODULE_0__["render"],
  _FilterWrapper_vue_vue_type_template_id_3b6aa020___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/FilterWrapper.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/FilterWrapper.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/FilterWrapper.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FilterWrapper_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./FilterWrapper.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/FilterWrapper.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FilterWrapper_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/FilterWrapper.vue?vue&type=template&id=3b6aa020&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/FilterWrapper.vue?vue&type=template&id=3b6aa020& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FilterWrapper_vue_vue_type_template_id_3b6aa020___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./FilterWrapper.vue?vue&type=template&id=3b6aa020& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/FilterWrapper.vue?vue&type=template&id=3b6aa020&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FilterWrapper_vue_vue_type_template_id_3b6aa020___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FilterWrapper_vue_vue_type_template_id_3b6aa020___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/event-bus.js":
/*!***********************************!*\
  !*** ./resources/js/event-bus.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);

var EventBus = new vue__WEBPACK_IMPORTED_MODULE_0___default.a();
/* harmony default export */ __webpack_exports__["default"] = (EventBus);

/***/ }),

/***/ "./resources/js/helpers/transformers.js":
/*!**********************************************!*\
  !*** ./resources/js/helpers/transformers.js ***!
  \**********************************************/
/*! exports provided: objectWithName, contact, contactWithAlias, contactWithAssociation, employee, employeeWithAlias, getRegion, percent, date, datetime, truncateString, limitWords, badgeStatus, badgeStatusYesNo, badgeStatusLabel, badgeStatusCompleted, currency, getCurrencyDescription */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "objectWithName", function() { return objectWithName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "contact", function() { return contact; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "contactWithAlias", function() { return contactWithAlias; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "contactWithAssociation", function() { return contactWithAssociation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "employee", function() { return employee; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "employeeWithAlias", function() { return employeeWithAlias; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getRegion", function() { return getRegion; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "percent", function() { return percent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "date", function() { return date; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "datetime", function() { return datetime; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "truncateString", function() { return truncateString; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "limitWords", function() { return limitWords; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "badgeStatus", function() { return badgeStatus; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "badgeStatusYesNo", function() { return badgeStatusYesNo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "badgeStatusLabel", function() { return badgeStatusLabel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "badgeStatusCompleted", function() { return badgeStatusCompleted; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "currency", function() { return currency; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCurrencyDescription", function() { return getCurrencyDescription; });
/* harmony import */ var _js_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @js/store */ "./resources/js/store/index.js");
/* harmony import */ var _core_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @core/utils */ "./resources/js/core/utils/index.js");


var vars = _js_store__WEBPACK_IMPORTED_MODULE_0__["default"].getters['config/vars'];
function objectWithName(_ref) {
  var name = _ref.name;
  return name;
}
function contact(_ref2) {
  var name = _ref2.name;
  return name;
}
function contactWithAlias(_ref3) {
  var name = _ref3.name,
      alias = _ref3.alias;
  return "".concat(name, " (").concat(alias, ")");
}
function contactWithAssociation(_ref4) {
  var name = _ref4.name,
      association = _ref4.association;
  return "".concat(name, " (").concat(association, ")");
}
function employee(_ref5) {
  var name = _ref5.name;
  return name;
}
function employeeWithAlias(_ref6) {
  var name = _ref6.name,
      alias = _ref6.alias;
  return "".concat(name, " (").concat(alias, ")");
}
function getRegion(value) {
  var item = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var region = item ? item.city : '';
  region = region ? item.zipcode ? "".concat(region, " - ").concat(item.zipcode) : region : '';
  region = item.state ? region ? "".concat(region, ", ").concat(item.state) : item.state : region;
  region = item.country ? region ? "".concat(region, ", ").concat(item.country.name) : item.country.name : region;
  return region;
}
function percent(content) {
  return "".concat(content, " %");
}
function date(content) {
  return moment(content, vars.serverDateFormat).format(vars.defaultDateFormat);
}
function datetime(content) {
  return moment(content, vars.serverDateTimeFormat).format(vars.defaultDateTimeFormat);
}
function truncateString(content, opts) {
  var max = opts.max || 30;
  var suffix = content.length > max ? opts.suffix || '...' : '';
  return content.slice(0, max) + suffix;
}
function limitWords(content, opts) {
  var contentArr = content.split(' ');
  var max = opts.max || 3;
  var suffix = contentArr.length > max ? opts.suffix || '...' : '';
  return contentArr.slice(0, max).join(' ') + suffix;
}
function badgeStatus(status) {
  return status ? 'true' : 'false';
}
function badgeStatusYesNo(status) {
  return status ? $t('general.yes') : $t('general.no');
}
function badgeStatusLabel(status, opts) {
  return status ? $t('general.yes') : $t('general.no');
}
function badgeStatusCompleted(status) {
  return status ? $t('utility.todo.completed') : $t('utility.todo.incomplete');
}
function currency(value, opts) {
  var currency = opts && opts.currency ? opts.currency : vars.defaultCurrency;
  return _core_utils__WEBPACK_IMPORTED_MODULE_1__["formatCurrency"].to(value, currency, true);
}
function getCurrencyDescription(value) {
  var item = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  return "".concat(item.currency.name, " - ").concat(item.currency.description, " (").concat(item.currency.symbol, ")");
}

/***/ }),

/***/ "./resources/js/mixins/filter-form.js":
/*!********************************************!*\
  !*** ./resources/js/mixins/filter-form.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _components_FilterWrapper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @components/FilterWrapper */ "./resources/js/components/FilterWrapper.vue");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    FilterWrapper: _components_FilterWrapper__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  props: {
    preRequisite: {
      type: Object,
      "default": function _default() {
        return {};
      }
    },
    boxed: {
      type: Boolean,
      "default": false
    },
    isLoading: {
      type: Boolean,
      "default": false
    }
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('common', ['filters']))
});

/***/ }),

/***/ "./resources/js/views/app/call/filter.vue":
/*!************************************************!*\
  !*** ./resources/js/views/app/call/filter.vue ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _filter_vue_vue_type_template_id_0ee5172c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./filter.vue?vue&type=template&id=0ee5172c& */ "./resources/js/views/app/call/filter.vue?vue&type=template&id=0ee5172c&");
/* harmony import */ var _filter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./filter.vue?vue&type=script&lang=js& */ "./resources/js/views/app/call/filter.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _filter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _filter_vue_vue_type_template_id_0ee5172c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _filter_vue_vue_type_template_id_0ee5172c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/app/call/filter.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/app/call/filter.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./resources/js/views/app/call/filter.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_filter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./filter.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/call/filter.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_filter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/app/call/filter.vue?vue&type=template&id=0ee5172c&":
/*!*******************************************************************************!*\
  !*** ./resources/js/views/app/call/filter.vue?vue&type=template&id=0ee5172c& ***!
  \*******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_filter_vue_vue_type_template_id_0ee5172c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./filter.vue?vue&type=template&id=0ee5172c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/call/filter.vue?vue&type=template&id=0ee5172c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_filter_vue_vue_type_template_id_0ee5172c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_filter_vue_vue_type_template_id_0ee5172c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/app/call/index.vue":
/*!***********************************************!*\
  !*** ./resources/js/views/app/call/index.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index_vue_vue_type_template_id_3e9ff9ce___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=3e9ff9ce& */ "./resources/js/views/app/call/index.vue?vue&type=template&id=3e9ff9ce&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/views/app/call/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_3e9ff9ce___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_3e9ff9ce___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/app/call/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/app/call/index.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/views/app/call/index.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/call/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/app/call/index.vue?vue&type=template&id=3e9ff9ce&":
/*!******************************************************************************!*\
  !*** ./resources/js/views/app/call/index.vue?vue&type=template&id=3e9ff9ce& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_3e9ff9ce___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=3e9ff9ce& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/call/index.vue?vue&type=template&id=3e9ff9ce&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_3e9ff9ce___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_3e9ff9ce___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);
//# sourceMappingURL=index.js.map?id=de79d69dc4e730123a60