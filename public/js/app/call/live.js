(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/app/call/live"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/core/components/AnimatedNumber.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/core/components/AnimatedNumber.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "animated-number",
  props: {
    number: {
      type: Number,
      "default": 0
    },
    delay: {
      type: Number,
      "default": 20
    }
  },
  data: function data() {
    return {
      displayNumber: 0,
      interval: false,
      initialInterval: false
    };
  },
  methods: {
    changeNumberFn: function changeNumberFn() {
      if (this.displayNumber != this.number) {
        var change = (this.number - this.displayNumber) / 10;
        change = change >= 0 ? Math.ceil(change) : Math.floor(change);
        this.displayNumber = this.displayNumber + change;
      }
    }
  },
  watch: {
    number: function number() {
      clearInterval(this.interval);

      if (this.number == this.displayNumber) {
        return;
      }

      this.interval = window.setInterval(this.changeNumberFn, this.delay);
    }
  },
  mounted: function mounted() {
    // this.displayNumber = this.number ? this.number : 0
    this.displayNumber = 0;
    clearInterval(this.initialInterval);

    if (this.number == this.displayNumber) {
      return;
    }

    this.initialInterval = window.setInterval(this.changeNumberFn, this.delay);
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/call/live.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/app/call/live.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _mixins_call__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @mixins/call */ "./resources/js/mixins/call.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": _mixins_call__WEBPACK_IMPORTED_MODULE_0__["default"],
  components: {}
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/call/live.vue?vue&type=style&index=0&lang=scss&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--9-2!./node_modules/sass-loader/dist/cjs.js??ref--9-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/app/call/live.vue?vue&type=style&index=0&lang=scss& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Muli:300,400,600,700,800,900);", ""]);

// module
exports.push([module.i, ".call-page {\n  width: 100vw;\n  height: 100vh;\n  max-height: 100vh;\n  padding: 0;\n  margin: 0;\n  display: flex;\n  flex-direction: column;\n  align-items: stretch;\n}\n.call-page .video-list {\n  display: flex;\n  flex-grow: 1;\n  justify-content: space-around;\n  align-items: center;\n  flex-wrap: wrap;\n  padding-bottom: 3rem;\n}\n.call-page .call-footer {\n  width: 100%;\n  display: flex;\n  justify-content: space-around;\n  height: 60px;\n  align-items: stretch;\n}\n.call-page .call-footer .logo-wrapper {\n  display: flex;\n  align-items: center;\n  padding-left: 15px;\n}\n.call-page .call-footer .call-actions {\n  text-align: right;\n  display: none;\n}\n.call-page .call-footer .call-actions > .separator {\n  width: 2px;\n  border-left: 2px solid rgba(0, 0, 0, 0.1);\n  background: rgba(0, 0, 0, 0.05);\n}\n.call-page .call-footer .call-actions > .btn {\n  box-shadow: none;\n  font-size: 1rem;\n  margin-right: 0;\n}\n.call-page .call-footer .call-actions > .btn:hover {\n  box-shadow: none;\n  background: rgba(0, 0, 0, 0.3);\n}\n.call-page .call-footer .call-actions > .btn:active {\n  background: rgba(0, 0, 0, 0.6);\n  box-shadow: inset 0 0 10px 0px rgba(0, 0, 0, 0.1);\n}\n.call-page .call-footer .call-actions > .btn.disabled-text {\n  color: #f5365c;\n}\n.call-page .call-footer .call-actions > .btn.disabled-text:hover {\n  color: #f5365c;\n}\n.call-page .call-footer .call-actions > .btn.enabled-text {\n  color: #2dce89;\n}\n.call-page .call-footer .call-actions > .btn.enabled-text:hover {\n  color: #2dce89;\n}\n[data-page-background-color=primary] {\n  background-color: #581b98;\n  color: #dee2e9;\n}\n[data-footer-background-color=primary] .call-footer {\n  background-color: #581b98;\n  color: #dee2e9;\n}\n[data-page-background-color=light-primary] {\n  background-color: #7e27d9;\n  color: #dee2e9;\n}\n[data-footer-background-color=light-primary] .call-footer {\n  background-color: #7e27d9;\n  color: #dee2e9;\n}\n[data-page-background-color=dark-primary] {\n  background-color: #320f57;\n  color: #dee2e9;\n}\n[data-footer-background-color=dark-primary] .call-footer {\n  background-color: #320f57;\n  color: #dee2e9;\n}\n[data-page-background-color=secondary] {\n  background-color: #e00c51;\n  color: #6c7580;\n}\n[data-footer-background-color=secondary] .call-footer {\n  background-color: #e00c51;\n  color: #6c7580;\n}\n[data-page-background-color=dark-secondary] {\n  background-color: #970837;\n  color: #6c7580;\n}\n[data-footer-background-color=dark-secondary] .call-footer {\n  background-color: #970837;\n  color: #6c7580;\n}\n[data-page-background-color=success] {\n  background-color: #2dce89;\n  color: #dee2e9;\n}\n[data-footer-background-color=success] .call-footer {\n  background-color: #2dce89;\n  color: #dee2e9;\n}\n[data-page-background-color=info] {\n  background-color: #11cdef;\n  color: #dee2e9;\n}\n[data-footer-background-color=info] .call-footer {\n  background-color: #11cdef;\n  color: #dee2e9;\n}\n[data-page-background-color=warning] {\n  background-color: #fb6340;\n  color: #dee2e9;\n}\n[data-footer-background-color=warning] .call-footer {\n  background-color: #fb6340;\n  color: #dee2e9;\n}\n[data-page-background-color=danger] {\n  background-color: #f5365c;\n  color: #dee2e9;\n}\n[data-footer-background-color=danger] .call-footer {\n  background-color: #f5365c;\n  color: #dee2e9;\n}\n[data-page-background-color=white] {\n  background-color: #fff;\n  color: #6c7580;\n}\n[data-footer-background-color=white] .call-footer {\n  background-color: #fff;\n  color: #6c7580;\n}\n[data-page-background-color=whitish] {\n  background-color: #fafbfd;\n  color: #6c7580;\n}\n[data-footer-background-color=whitish] .call-footer {\n  background-color: #fafbfd;\n  color: #6c7580;\n}\n[data-page-background-color=light] {\n  background-color: #dee2e9;\n  color: #6c7580;\n}\n[data-footer-background-color=light] .call-footer {\n  background-color: #dee2e9;\n  color: #6c7580;\n}\n[data-page-background-color=dark] {\n  background-color: #6c7580;\n  color: #dee2e9;\n}\n[data-footer-background-color=dark] .call-footer {\n  background-color: #6c7580;\n  color: #dee2e9;\n}\n[data-page-background-color=gray-darker] {\n  background-color: #495060;\n  color: #dee2e9;\n}\n[data-footer-background-color=gray-darker] .call-footer {\n  background-color: #495060;\n  color: #dee2e9;\n}\n[data-page-background-color=gray-darkest] {\n  background-color: #212532;\n  color: #dee2e9;\n}\n[data-footer-background-color=gray-darkest] .call-footer {\n  background-color: #212532;\n  color: #dee2e9;\n}\n[data-page-background-color=black] {\n  background-color: #000;\n  color: #dee2e9;\n}\n[data-footer-background-color=black] .call-footer {\n  background-color: #000;\n  color: #dee2e9;\n}\n[data-page-background-color=default-color] {\n  background-color: #0a346d;\n  color: #dee2e9;\n}\n[data-footer-background-color=default-color] .call-footer {\n  background-color: #0a346d;\n  color: #dee2e9;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/call/live.vue?vue&type=style&index=0&lang=scss&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--9-2!./node_modules/sass-loader/dist/cjs.js??ref--9-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/app/call/live.vue?vue&type=style&index=0&lang=scss& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--9-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--9-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./live.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/call/live.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/core/components/AnimatedNumber.vue?vue&type=template&id=96304fda&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/core/components/AnimatedNumber.vue?vue&type=template&id=96304fda& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("span", [_vm._v(_vm._s(_vm.displayNumber))])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/call/live.vue?vue&type=template&id=15f1b9a0&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/app/call/live.vue?vue&type=template&id=15f1b9a0& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "call-page",
      attrs: {
        "data-page-background-color":
          _vm.uiConfigs.pageContainerBackgroundColor,
        "data-footer-background-color": _vm.uiConfigs.leftSidebarColor
      }
    },
    [
      !_vm.duplicateTab
        ? [
            _c(
              "div",
              {
                ref: "videoListEle",
                class: [
                  "video-list",
                  { "has-no-video": !_vm.hasVideos },
                  {
                    "agenda-comments-visible":
                      _vm.pageConfigs.showAgenda || _vm.pageConfigs.showComments
                  }
                ]
              },
              [
                _vm.hasVideos
                  ? _vm._l(_vm.videoList, function(item, itemIndex) {
                      return _c(
                        "div",
                        {
                          key: "" + item.uuid + item.id,
                          class: [
                            "video-item",
                            { maximized: item.maximized },
                            { local: item.local },
                            { screen: item.screen },
                            {
                              "fullscreen-item":
                                item.id === _vm.fullScreenItemId
                            },
                            { "audio-muted": item.audioMuted },
                            {
                              "video-muted": item.videoMuted || !item.hasVideo
                            },
                            { "no-stream": !item.status }
                          ],
                          on: {
                            dblclick: function($event) {
                              return _vm.changeFocus(item)
                            }
                          }
                        },
                        [
                          _c("div", { staticClass: "video-wrapper" }, [
                            _c("video", {
                              ref: "videos",
                              refInFor: true,
                              class: [
                                { "no-poster": item.status && !item.videoMuted }
                              ],
                              attrs: {
                                autoplay: "",
                                playsinline: "",
                                id: item.id,
                                poster: item.status
                                  ? null
                                  : "/images/video/no-stream.jpg"
                              },
                              domProps: { muted: item.muted }
                            })
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "caption" },
                            [
                              item.status
                                ? _c(
                                    "h6",
                                    [
                                      _vm._m(0, true),
                                      _vm._v(" "),
                                      item.local
                                        ? [
                                            _vm._v(
                                              "\n                                " +
                                                _vm._s(
                                                  item.screen
                                                    ? _vm.$t(
                                                        "meeting.your_screen"
                                                      )
                                                    : _vm.$t("meeting.you")
                                                ) +
                                                "\n                            "
                                            )
                                          ]
                                        : [
                                            _vm._v(
                                              "\n                                " +
                                                _vm._s(
                                                  item.screen
                                                    ? "" +
                                                        item.name +
                                                        _vm.$t("meeting.screen")
                                                    : item.name
                                                ) +
                                                "\n                            "
                                            )
                                          ]
                                    ],
                                    2
                                  )
                                : _c("h6", [
                                    _vm._m(1, true),
                                    _vm._v(
                                      "\n\n                            " +
                                        _vm._s(
                                          item.local
                                            ? item.localScreen
                                              ? _vm.$t("meeting.your_screen")
                                              : _vm.$t("meeting.you")
                                            : item.name
                                        ) +
                                        "\n\n                            "
                                    ),
                                    _c(
                                      "span",
                                      { staticClass: "bracketed text-danger" },
                                      [_vm._v(_vm._s(_vm.$t("meeting.left")))]
                                    )
                                  ]),
                              _vm._v(" "),
                              !item.maximized
                                ? _c(
                                    "base-dropdown",
                                    {
                                      staticClass: "video-item-actions",
                                      attrs: {
                                        tag: "div",
                                        direction: "up",
                                        "menu-classes":
                                          "animated faster menuFadeInUpSmall",
                                        position: "right",
                                        design: "gray-darker"
                                      }
                                    },
                                    [
                                      _c(
                                        "base-button",
                                        {
                                          attrs: {
                                            slot: "title",
                                            type: "button",
                                            design: "dark-primary",
                                            size: "sm",
                                            "data-toggle": "dropdown",
                                            role: "button"
                                          },
                                          slot: "title"
                                        },
                                        [
                                          _c("i", {
                                            staticClass: "fas fa-ellipsis-v"
                                          })
                                        ]
                                      ),
                                      _vm._v(" "),
                                      !item.maximized
                                        ? _c(
                                            "a",
                                            {
                                              staticClass: "dropdown-item",
                                              on: {
                                                click: function($event) {
                                                  return _vm.changeFocus(item)
                                                }
                                              }
                                            },
                                            [_vm._v("Focus this!")]
                                          )
                                        : _vm._e()
                                    ],
                                    1
                                  )
                                : _vm._e()
                            ],
                            1
                          ),
                          _vm._v(" "),
                          !item.local
                            ? _c(
                                "div",
                                { staticClass: "custom-controls-wrapper" },
                                [
                                  _c(
                                    "div",
                                    { staticClass: "custom-controls" },
                                    [
                                      item.audioMuted && !item.videoMuted
                                        ? _c(
                                            "div",
                                            { staticClass: "status-icon" },
                                            [
                                              _c("i", {
                                                staticClass:
                                                  "fas fa-microphone-slash disabled-text"
                                              })
                                            ]
                                          )
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _c(
                                        "button",
                                        {
                                          on: {
                                            click: function($event) {
                                              return _vm.toggleRemoteAudio(
                                                item,
                                                itemIndex
                                              )
                                            }
                                          }
                                        },
                                        [
                                          _c("i", {
                                            class: [
                                              "fas",
                                              {
                                                "fa-volume-up enabled-text": !item.muted
                                              },
                                              {
                                                "fa-volume-off disabled-text":
                                                  item.muted
                                              }
                                            ]
                                          })
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "button",
                                        {
                                          on: {
                                            click: function($event) {
                                              return _vm.toggleFullScreen(
                                                item,
                                                itemIndex
                                              )
                                            }
                                          }
                                        },
                                        [
                                          _c("i", {
                                            class: [
                                              "fas",
                                              {
                                                "fa-expand": !_vm.fullScreenInOn
                                              },
                                              {
                                                "fa-compress":
                                                  _vm.fullScreenInOn
                                              }
                                            ]
                                          })
                                        ]
                                      )
                                    ]
                                  )
                                ]
                              )
                            : _vm._e()
                        ]
                      )
                    })
                  : _vm._e()
              ],
              2
            ),
            _vm._v(" "),
            _c("div", { staticClass: "call-footer" }, [
              _c(
                "div",
                { staticClass: "logo-wrapper" },
                [
                  _c("app-logo", {
                    attrs: {
                      place: _vm.pageConfigs.isDark ? "dark" : "light",
                      size: "sm"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "call-actions" },
                [
                  _vm.entity && _vm.entity.status && _vm.videoList.length
                    ? [
                        _vm.localVideo
                          ? [
                              _vm.pageConfigs.showEnableVideoBtn
                                ? [
                                    _vm.pageConfigs.enableVideo
                                      ? _c(
                                          "button",
                                          {
                                            directives: [
                                              {
                                                name: "b-tooltip",
                                                rawName:
                                                  "v-b-tooltip.hover.d500",
                                                modifiers: {
                                                  hover: true,
                                                  d500: true
                                                }
                                              }
                                            ],
                                            staticClass:
                                              "btn action enabled-text",
                                            attrs: {
                                              type: "button",
                                              title: _vm.$t(
                                                "meeting.mute_video"
                                              )
                                            },
                                            on: { click: _vm.toggleVideo }
                                          },
                                          [
                                            _c("i", {
                                              staticClass: "fas fa-video"
                                            })
                                          ]
                                        )
                                      : _c(
                                          "button",
                                          {
                                            directives: [
                                              {
                                                name: "b-tooltip",
                                                rawName:
                                                  "v-b-tooltip.hover.d500",
                                                modifiers: {
                                                  hover: true,
                                                  d500: true
                                                }
                                              }
                                            ],
                                            staticClass:
                                              "btn action disabled-text",
                                            attrs: {
                                              type: "button",
                                              title: _vm.$t(
                                                "meeting.unmute_video"
                                              )
                                            },
                                            on: { click: _vm.toggleVideo }
                                          },
                                          [
                                            _c("i", {
                                              staticClass: "fas fa-video-slash"
                                            })
                                          ]
                                        )
                                  ]
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.pageConfigs.showEnableAudioBtn
                                ? [
                                    _vm.pageConfigs.enableAudio
                                      ? _c(
                                          "button",
                                          {
                                            directives: [
                                              {
                                                name: "b-tooltip",
                                                rawName:
                                                  "v-b-tooltip.hover.d500",
                                                modifiers: {
                                                  hover: true,
                                                  d500: true
                                                }
                                              }
                                            ],
                                            staticClass:
                                              "btn action enabled-text",
                                            attrs: {
                                              type: "button",
                                              title: _vm.$t(
                                                "meeting.mute_audio"
                                              )
                                            },
                                            on: { click: _vm.toggleAudio }
                                          },
                                          [
                                            _c("i", {
                                              staticClass: "fas fa-microphone"
                                            })
                                          ]
                                        )
                                      : _c(
                                          "button",
                                          {
                                            directives: [
                                              {
                                                name: "b-tooltip",
                                                rawName:
                                                  "v-b-tooltip.hover.d500",
                                                modifiers: {
                                                  hover: true,
                                                  d500: true
                                                }
                                              }
                                            ],
                                            staticClass:
                                              "btn action disabled-text",
                                            attrs: {
                                              type: "button",
                                              title: _vm.$t(
                                                "meeting.unmute_audio"
                                              )
                                            },
                                            on: { click: _vm.toggleAudio }
                                          },
                                          [
                                            _c("i", {
                                              staticClass:
                                                "fas fa-microphone-slash"
                                            })
                                          ]
                                        )
                                  ]
                                : _vm._e()
                            ]
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.pageConfigs.showShareScreenBtn
                          ? [
                              _vm.localScreenStreamid
                                ? _c(
                                    "button",
                                    {
                                      directives: [
                                        {
                                          name: "b-tooltip",
                                          rawName: "v-b-tooltip.hover.d500",
                                          modifiers: { hover: true, d500: true }
                                        }
                                      ],
                                      staticClass: "btn action",
                                      attrs: {
                                        type: "button",
                                        title: _vm.$t(
                                          "meeting.stop_sharing_screen"
                                        )
                                      },
                                      on: { click: _vm.stopSharingScreen }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fas fa-minus-square"
                                      })
                                    ]
                                  )
                                : _c(
                                    "button",
                                    {
                                      directives: [
                                        {
                                          name: "b-tooltip",
                                          rawName: "v-b-tooltip.hover.d500",
                                          modifiers: { hover: true, d500: true }
                                        }
                                      ],
                                      staticClass: "btn action",
                                      attrs: {
                                        type: "button",
                                        title: _vm.$t("meeting.share_screen")
                                      },
                                      on: { click: _vm.shareScreen }
                                    },
                                    [_c("i", { staticClass: "fas fa-desktop" })]
                                  )
                            ]
                          : _vm._e(),
                        _vm._v(" "),
                        _c("div", { staticClass: "separator" }),
                        _vm._v(" "),
                        _vm.entity.canModerate
                          ? _c(
                              "button",
                              {
                                directives: [
                                  {
                                    name: "b-tooltip",
                                    rawName: "v-b-tooltip.hover.d500",
                                    modifiers: { hover: true, d500: true }
                                  }
                                ],
                                staticClass: "btn action",
                                attrs: {
                                  type: "button",
                                  title: _vm.$t("meeting.end_meeting")
                                },
                                on: { click: _vm.endMeeting }
                              },
                              [
                                _c("i", {
                                  staticClass: "fas fa-calendar-times"
                                })
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _c(
                          "button",
                          {
                            directives: [
                              {
                                name: "b-tooltip",
                                rawName: "v-b-tooltip.hover.d500",
                                modifiers: { hover: true, d500: true }
                              }
                            ],
                            staticClass: "btn action",
                            attrs: {
                              type: "button",
                              title: _vm.$t("meeting.leave_meeting")
                            },
                            on: { click: _vm.getOffline }
                          },
                          [_c("i", { staticClass: "fas fa-sign-out-alt" })]
                        )
                      ]
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.entity &&
                  _vm.entity.config &&
                  _vm.entity.config.enableComments
                    ? _c(
                        "button",
                        {
                          directives: [
                            {
                              name: "b-tooltip",
                              rawName: "v-b-tooltip.hover.d500",
                              modifiers: { hover: true, d500: true }
                            }
                          ],
                          class: ["btn action", { pulse: _vm.newComments }],
                          attrs: {
                            type: "button",
                            title: _vm.$t(
                              _vm.pageConfigs.showComments
                                ? "global.hide"
                                : "global.show",
                              { attribute: _vm.$t("comment.comments") }
                            )
                          },
                          on: { click: _vm.toggleComments }
                        },
                        [
                          _c("i", {
                            class: [
                              {
                                "fas fa-comment-slash":
                                  _vm.pageConfigs.showComments
                              },
                              {
                                "far fa-comment": !_vm.pageConfigs.showComments
                              }
                            ]
                          })
                        ]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _c(
                    "button",
                    {
                      directives: [
                        {
                          name: "b-tooltip",
                          rawName: "v-b-tooltip.hover.d500",
                          modifiers: { hover: true, d500: true }
                        }
                      ],
                      staticClass: "btn action",
                      attrs: {
                        type: "button",
                        title: _vm.$t(
                          _vm.pageConfigs.showAgenda
                            ? "global.hide"
                            : "global.show",
                          { attribute: _vm.$t("general.agenda") }
                        )
                      },
                      on: { click: _vm.toggleAgenda }
                    },
                    [_c("i", { staticClass: "fas fa-quote-left" })]
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "separator" }),
                  _vm._v(" "),
                  _c(
                    "button",
                    {
                      directives: [
                        {
                          name: "b-tooltip",
                          rawName: "v-b-tooltip.hover.d500",
                          modifiers: { hover: true, d500: true }
                        }
                      ],
                      staticClass: "btn action",
                      attrs: {
                        type: "button",
                        title: _vm.$t("global.switch", {
                          attribute: _vm.pageConfigs.isDark
                            ? _vm.$t("config.ui.light")
                            : _vm.$t("config.ui.dark")
                        })
                      },
                      on: { click: _vm.toggleDark }
                    },
                    [_c("i", { staticClass: "fas fa-adjust" })]
                  )
                ],
                2
              )
            ])
          ]
        : _c(
            "div",
            {
              staticClass:
                "duplicate-tab d-flex justify-content-center align-items-center min-height-90vh"
            },
            [_c("h2", [_vm._v(_vm._s(_vm.$t("meeting.duplicate_tab")))])]
          )
    ],
    2
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "text-xs mr-1 text-success" }, [
      _c("i", { staticClass: "fas fa-circle" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "text-xs mr-1 text-danger" }, [
      _c("i", { staticClass: "fas fa-circle" })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/core/components/AnimatedNumber.vue":
/*!*********************************************************!*\
  !*** ./resources/js/core/components/AnimatedNumber.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AnimatedNumber_vue_vue_type_template_id_96304fda___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AnimatedNumber.vue?vue&type=template&id=96304fda& */ "./resources/js/core/components/AnimatedNumber.vue?vue&type=template&id=96304fda&");
/* harmony import */ var _AnimatedNumber_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AnimatedNumber.vue?vue&type=script&lang=js& */ "./resources/js/core/components/AnimatedNumber.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _AnimatedNumber_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AnimatedNumber_vue_vue_type_template_id_96304fda___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AnimatedNumber_vue_vue_type_template_id_96304fda___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/core/components/AnimatedNumber.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/core/components/AnimatedNumber.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/core/components/AnimatedNumber.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AnimatedNumber_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./AnimatedNumber.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/core/components/AnimatedNumber.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AnimatedNumber_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/core/components/AnimatedNumber.vue?vue&type=template&id=96304fda&":
/*!****************************************************************************************!*\
  !*** ./resources/js/core/components/AnimatedNumber.vue?vue&type=template&id=96304fda& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AnimatedNumber_vue_vue_type_template_id_96304fda___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./AnimatedNumber.vue?vue&type=template&id=96304fda& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/core/components/AnimatedNumber.vue?vue&type=template&id=96304fda&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AnimatedNumber_vue_vue_type_template_id_96304fda___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AnimatedNumber_vue_vue_type_template_id_96304fda___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/core/plugins/detect-duplicate-tab.js":
/*!***********************************************************!*\
  !*** ./resources/js/core/plugins/detect-duplicate-tab.js ***!
  \***********************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! js-cookie */ "./node_modules/js-cookie/src/js.cookie.js");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! uuid */ "./node_modules/uuid/dist/esm-browser/index.js");



var DuplicateWindow = function DuplicateWindow() {
  var localStorageTimeout = 5 * 1000; // 15,000 milliseconds = 15 seconds.

  var localStorageResetInterval = 1 / 2 * 1000; // 10,000 milliseconds = 10 seconds.

  var localStorageTabKey = 'my-application-browser-tab';
  var sessionStorageGuidKey = 'browser-tab-guid';
  var ItemType = {
    Session: 1,
    Local: 2
  };

  function getItem(itemtype) {
    var val = "";

    switch (itemtype) {
      case ItemType.Session:
        val = window.name;
        break;

      case ItemType.Local:
        val = decodeURIComponent(js_cookie__WEBPACK_IMPORTED_MODULE_0___default.a.get(localStorageTabKey));
        if (val == undefined || val == 'undefined') val = "";
        break;
    }

    return val;
  }

  function setItem(itemtype, val) {
    switch (itemtype) {
      case ItemType.Session:
        window.name = val;
        break;

      case ItemType.Local:
        js_cookie__WEBPACK_IMPORTED_MODULE_0___default.a.set(localStorageTabKey, val || "");
        break;
    }
  }

  function testIfDuplicate() {
    //console.log("In testTab")
    var sessionGuid = getItem(ItemType.Session) || Object(uuid__WEBPACK_IMPORTED_MODULE_1__["v4"])();
    setItem(ItemType.Session, sessionGuid);
    var val = getItem(ItemType.Local);
    var tabObj = (val == "" ? null : JSON.parse(val)) || null; // If no or stale tab object, our session is the winner.  If the guid matches, ours is still the winner

    if (tabObj === null || tabObj.timestamp < new Date().getTime() - localStorageTimeout || tabObj.guid === sessionGuid) {
      var setTabObj = function setTabObj() {
        //console.log("In setTabObj")
        var newTabObj = {
          guid: sessionGuid,
          timestamp: new Date().getTime()
        };
        setItem(ItemType.Local, JSON.stringify(newTabObj));
      };

      setTabObj();
      setInterval(setTabObj, localStorageResetInterval); //every x interval refresh timestamp in cookie

      return false;
    } else {
      // An active tab is already open that does not match our session guid.
      return true;
    }
  }

  window.isDuplicate = function () {
    var duplicate = testIfDuplicate(); //console.log("Is Duplicate: "+ duplicate)

    return duplicate;
  };

  window.addEventListener('beforeunload', function () {
    if (testIfDuplicate() == false) {
      setItem(ItemType.Local, "");
    }
  });
};

DuplicateWindow();

/***/ }),

/***/ "./resources/js/mixins/call.js":
/*!*************************************!*\
  !*** ./resources/js/mixins/call.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _core_components_AnimatedNumber__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @core/components/AnimatedNumber */ "./resources/js/core/components/AnimatedNumber.vue");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! socket.io-client */ "./node_modules/socket.io-client/lib/index.js");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(socket_io_client__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var rtcmulticonnection__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rtcmulticonnection */ "./node_modules/rtcmulticonnection/dist/RTCMultiConnection.js");
/* harmony import */ var rtcmulticonnection__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(rtcmulticonnection__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _js_echo_setup__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @js/echo-setup */ "./resources/js/echo-setup.js");
/* harmony import */ var _core_utils_media__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @core/utils/media */ "./resources/js/core/utils/media.js");
/* harmony import */ var _core_plugins_detect_duplicate_tab__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @core/plugins/detect-duplicate-tab */ "./resources/js/core/plugins/detect-duplicate-tab.js");
/* harmony import */ var _core_configs_sweet_alert__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @core/configs/sweet-alert */ "./resources/js/core/configs/sweet-alert.js");
/* harmony import */ var _components_AppLogo__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @components/AppLogo */ "./resources/js/components/AppLogo.vue");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




 // import 'adapterjs'






window.io = socket_io_client__WEBPACK_IMPORTED_MODULE_2___default.a;
/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    AnimatedNumber: _core_components_AnimatedNumber__WEBPACK_IMPORTED_MODULE_1__["default"],
    RTCMultiConnection: rtcmulticonnection__WEBPACK_IMPORTED_MODULE_3___default.a,
    AppLogo: _components_AppLogo__WEBPACK_IMPORTED_MODULE_8__["default"]
  },
  data: function data() {
    return {
      uuid: null,
      entity: null,
      isLoading: false,
      prevRoute: null,
      channel: null,
      countdownInterval: null,
      getOnlineTimer: null,
      fullScreenItemId: null,
      meetingRoomId: null,
      initUrl: 'meetings',
      fallBackRoute: 'appMeetingList',
      newComments: false,
      roomIdAlive: false,
      showFlipClock: true,
      duplicateTab: false,
      fullScreenInOn: false,
      membersLive: [],
      videoList: [],
      socketURL: 'https://socket.scriptmint.com:9001/',
      rtcmConnection: null,
      localStream: null,
      localScreenStreamid: null,
      videoConstraints: {},
      pageConfigs: {
        isDark: true,
        showAgenda: true,
        showComments: false,
        enableAudio: true,
        enableVideo: true,
        autoplay: true,
        showEnableAudioBtn: true,
        showEnableVideoBtn: true,
        showShareScreenBtn: true
      },
      meetingRulesHost: {
        session: {
          audio: false,
          video: false,
          screen: false,
          oneway: false
        },
        mediaConstraints: {
          audio: false,
          video: false,
          screen: false
        },
        mandatory: {
          OfferToReceiveAudio: false,
          OfferToReceiveVideo: false
        }
      },
      meetingRulesGuest: {
        session: {
          audio: false,
          video: false,
          screen: false,
          oneway: false
        },
        mediaConstraints: {
          audio: false,
          video: false,
          screen: false
        },
        mandatory: {
          OfferToReceiveAudio: false,
          OfferToReceiveVideo: false
        }
      },
      snoozeOpts: [{
        uuid: 5,
        name: 5,
        type: 'm'
      }, {
        uuid: 10,
        name: 10,
        type: 'm'
      }, {
        uuid: 15,
        name: 15,
        type: 'm'
      }, {
        uuid: 30,
        name: 30,
        type: 'm'
      }, {
        uuid: 60,
        name: 1,
        type: 'h'
      }]
    };
  },
  computed: _objectSpread(_objectSpread(_objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('config', ['vars', 'configs', 'uiConfigs'])), Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('user', {
    'userUuid': 'uuid'
  })), Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('user', ['profile', 'username', 'hasPermission'])), {}, {
    user: function user() {
      return {
        uuid: this.userUuid,
        username: this.username,
        name: this.profile.name
      };
    },
    hasVideos: function hasVideos() {
      return this.videoList && this.videoList.length;
    },
    liveMembersCount: function liveMembersCount() {
      return this.membersLive.length - 1;
    },
    startDateTimeIsFuture: function startDateTimeIsFuture() {
      var isInFuture = this.entity && this.showFlipClock && this.isStartDateTimeInFuture();

      if (isInFuture) {
        this.startCountDown();
      } else {
        if (this.countdownInterval) {
          clearInterval(this.countdownInterval);
        }
      }

      return isInFuture;
    }
  }),
  watch: {
    liveMembersCount: function liveMembersCount(newVal, oldVal) {
      if (!window.isLiveMeetingDestroyed && newVal !== oldVal) {
        this.meetingRoomCreated(this.entity);
      }
    }
  },
  methods: _objectSpread(_objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])('config', ['SetUiConfig'])), Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])('common', ['Init', 'Get', 'Custom', 'GetPreRequisite'])), {}, {
    // query / countdown / timer methods
    isStartDateTimeInFuture: function isStartDateTimeInFuture() {
      return this.entity && this.entity.startDateTime && moment(this.entity.startDateTime, this.vars.serverDateTimeFormat).isAfter(moment());
    },
    startCountDown: function startCountDown() {
      var _this = this;

      if (this.countdownInterval) {
        clearInterval(this.countdownInterval);
      }

      this.countdownInterval = window.setInterval(function () {
        if (_this.isStartDateTimeInFuture()) {
          _this.showFlipClock = true;
        } else {
          if (_this.showFlipClock && !document.hasFocus()) {
            Object(_core_utils_media__WEBPACK_IMPORTED_MODULE_5__["playIncomingMessage"])();
          }

          _this.showFlipClock = false;
        }
      }, 1000);
    },
    startGetOnlineTimer: function startGetOnlineTimer() {
      var _this2 = this;

      if (this.getOnlineTimer) {
        clearTimeout(this.getOnlineTimer);
      }

      if (this.videoList.length) {
        return;
      }

      var alertContent = {
        title: $t('misc.refresh_confirm.title'),
        text: $t('misc.refresh_confirm.text'),
        showCancelButton: true,
        confirmButtonText: $t('misc.refresh_confirm.confirm_btn'),
        cancelButtonText: $t('misc.refresh_confirm.cancel_btn')
      };
      this.getOnlineTimer = setTimeout(function () {
        swtAlert.fire(alertContent).then(function (result) {
          if (result.value) {
            window.location.reload();
          } else {
            _this2.startGetOnlineTimer();
          }
        });
      }, 15000);
    },
    // toggle methods
    toggleAudio: function toggleAudio() {
      var _this3 = this;

      var found = this.$refs.videos.find(function (video) {
        return video.id === _this3.localStream.id;
      });
      var videoIndex = this.videoList.findIndex(function (video) {
        return video.id === _this3.localStream.id;
      });

      if (found && found.srcObject) {
        var stream = found.srcObject;
        var tracks = stream.getAudioTracks();
        tracks.forEach(function (track) {
          if (_this3.pageConfigs.enableAudio) {
            _this3.pageConfigs.enableAudio = false;
            track.enabled = false;
            _this3.videoList[videoIndex].audioMuted = true;

            _this3.rtcmConnection.socket.emit('remoteMutedUnmuted', {
              audioEnabled: false,
              streamid: stream.streamid
            });
          } else {
            _this3.pageConfigs.enableAudio = true;
            track.enabled = true;
            _this3.videoList[videoIndex].audioMuted = true;

            _this3.rtcmConnection.socket.emit('remoteMutedUnmuted', {
              audioEnabled: true,
              streamid: stream.streamid
            });
          }
        });
        this.rtcmConnection.extra.audioMuted = !this.pageConfigs.enableAudio;
        this.rtcmConnection.updateExtraData();
      } else {
        this.localStream = null;
      }
    },
    toggleVideo: function toggleVideo() {
      var _this4 = this;

      var found = this.$refs.videos.find(function (video) {
        return video.id === _this4.localStream.id;
      });
      var videoIndex = this.videoList.findIndex(function (video) {
        return video.id === _this4.localStream.id;
      });

      if (found && found.srcObject) {
        var stream = found.srcObject;
        var tracks = stream.getVideoTracks();
        tracks.forEach(function (track) {
          if (_this4.pageConfigs.enableVideo) {
            _this4.pageConfigs.enableVideo = false;
            track.enabled = false;
            _this4.videoList[videoIndex].videoMuted = true;

            _this4.rtcmConnection.socket.emit('remoteMutedUnmuted', {
              videoEnabled: false,
              streamid: stream.streamid
            });
          } else {
            _this4.pageConfigs.enableVideo = true;
            track.enabled = true;
            _this4.videoList[videoIndex].videoMuted = false;

            _this4.rtcmConnection.socket.emit('remoteMutedUnmuted', {
              videoEnabled: true,
              streamid: stream.streamid
            });
          }
        });
        this.rtcmConnection.extra.videoMuted = !this.pageConfigs.enableVideo;
        this.rtcmConnection.updateExtraData();
      } else {
        this.localStream = null;
      }
    },
    toggleRemoteAudio: function toggleRemoteAudio(videoItem, itemIndex) {
      if (videoItem.muted) {
        this.videoList[itemIndex].muted = false;
        this.$refs.videos[itemIndex].muted = false;
      } else {
        this.videoList[itemIndex].muted = true;
        this.$refs.videos[itemIndex].muted = true;
      } // if(videoItem.streamUserId) {
      //     const streamByUserId = this.rtcmConnection.streamEvents.selectFirst({ userid: videoItem.streamUserId }).stream
      //     if(videoItem.muted) {
      //         streamByUserId.unmute('audio')
      //     } else {
      //         streamByUserId.mute('audio')
      //     }
      // }

    },
    toggleFullScreen: function toggleFullScreen(videoItem, itemIndex) {
      var _this5 = this;

      var targetParentEl = this.$refs['videoListEle'];
      this.fullScreenItemId = videoItem.id;
      this.$fullscreen.toggle(targetParentEl, {
        wrap: false,
        callback: function callback(fullscreen) {
          _this5.fullScreenInOn = fullscreen;
        }
      });
    },
    toggleComments: function toggleComments() {
      this.newComments = false;
      this.pageConfigs.showComments = !this.pageConfigs.showComments;

      if (!this.pageConfigs.showComments && !this.hasVideos) {
        this.pageConfigs.showAgenda = true;
      } else {
        this.pageConfigs.showAgenda = false;
      }
    },
    toggleAgenda: function toggleAgenda() {
      this.pageConfigs.showAgenda = !this.pageConfigs.showAgenda;

      if (!this.pageConfigs.showAgenda && !this.hasVideos) {
        this.pageConfigs.showComments = true;
      } else {
        this.pageConfigs.showComments = false;
      }
    },
    toggleDark: function toggleDark() {
      this.pageConfigs.isDark = !this.pageConfigs.isDark;
    },
    changeFocus: function changeFocus(item) {
      this.videoList.forEach(function (v) {
        v.maximized = v.id === item.id;
      });
    },
    // event callback methods
    beforeUnload: function beforeUnload(event) {
      if (this.localStream) {
        this.channel.whisper('RemovedStream', this.localStream);
      }
    },
    // channel event callback methods
    afterJoiningChannel: function afterJoiningChannel(members) {
      this.membersLive = members;
    },
    newMemberJoining: function newMemberJoining(member) {
      this.membersLive.push(member);
    },
    memberLeaving: function memberLeaving(member) {
      this.membersLive = this.membersLive.filter(function (u) {
        return u.uuid !== member.uuid;
      });
    },
    meetingStatusChanged: function meetingStatusChanged(e) {
      if (e.uuid === this.entity.uuid) {
        if (this.entity.status === e.status) {
          return;
        }

        this.entity.status = e.status;
        this.entity.startDateTime = e.startDateTime;
        var meetingStatus = e.status;

        if (meetingStatus === 'scheduled' && e.delayed) {
          meetingStatus = 'delayed';
        }

        var statusUpdateMessages = {
          'live': 'meeting.is_live_now',
          'delayed': 'meeting.meeting_delayed',
          'cancelled': 'meeting.meeting_cancelled',
          'ended': 'meeting.meeting_ended'
        };
        this.$toasted.success(this.$t(statusUpdateMessages[meetingStatus]), this.$toastConfig);
      }
    },
    gotNewComment: function gotNewComment() {
      if (!this.pageConfigs.showComments || this.fullScreenInOn) {
        this.newComments = true;
        Object(_core_utils_media__WEBPACK_IMPORTED_MODULE_5__["playIncomingMessage"])();
      }
    },
    meetingRoomCreated: function meetingRoomCreated(e) {
      var _this6 = this;

      this.entity.roomId = e.roomId;
      this.initMediaAndRtcmConnection();
      this.rtcmConnection.checkPresence(this.entity.roomId, function (isRoomExist, roomid) {
        _this6.roomIdAlive = !!isRoomExist;
      });
    },
    streamRemoved: function streamRemoved(e) {
      this.rtcmConnection.removeStream(e.id);
      this.rtcmOnStreamEnded(e);
    },
    meetingEnded: function meetingEnded(e) {
      this.closeConnectionAndStream();
      this.getInitialData();
    },
    // channel action methods
    joinChannel: function joinChannel() {
      this.channel = window.Echo.join("Meeting.".concat(this.uuid));
      this.channel.here(this.afterJoiningChannel).joining(this.newMemberJoining).leaving(this.memberLeaving).listen('MeetingStatusChanged', this.meetingStatusChanged).listen('NewComment', this.gotNewComment).listenForWhisper('MeetingRoomCreated', this.meetingRoomCreated).listenForWhisper('RemovedStream', this.streamRemoved).listenForWhisper('MeetingEnded', this.meetingEnded);
    },
    // rtcm event callback methods
    rtcmOnStream: function rtcmOnStream(stream) {
      var _this7 = this;

      this.logEvent('On Stream: ', stream);
      var found = this.videoList.find(function (video) {
        return video.id === stream.streamid;
      });
      var streamInstance = stream.stream.idInstance ? JSON.parse(stream.stream.idInstance) : stream.stream;

      if (streamInstance.isScreen && stream.type === 'local') {
        this.localScreenStreamid = stream.streamid;
        var tracks = stream.stream.getTracks();
        tracks.forEach(function (track) {
          track.addEventListener('ended', _this7.stopSharingScreen);
        });
      }

      if (this.videoList.length > 1) {
        this.recheckLiveParticipants(null);
      }

      if (found === undefined) {
        var video = _objectSpread(_objectSpread({
          id: stream.streamid,
          streamUserId: stream.userid,
          muted: stream.type === 'local'
        }, stream.extra), {}, {
          maximized: stream.type === 'local' && !streamInstance.isScreen,
          local: stream.type === 'local',
          screen: this.localScreenStreamid === stream.streamid,
          status: true,
          hasAudio: streamInstance.isAudio || streamInstance.audio,
          hasVideo: streamInstance.isVideo || streamInstance.video || streamInstance.isScreen || streamInstance.screen
        });

        if (streamInstance.isScreen) {
          video.videoMuted = false;
        } // let foundUserIndex = this.videoList.findIndex(item => item.uuid === stream.extra.uuid)
        // if (foundUserIndex >= 0) {
        //     const foundUserVideo = this.videoList[foundUserIndex]
        //     if (!foundUserVideo.status) {
        //         let newList = this.videoList.map(item => item.uuid !== foundUserVideo.uuid)
        //         this.videoList = newList
        //     }
        // }


        this.videoList.push(video);

        if (stream.type === 'local' && this.localScreenStreamid !== stream.streamid) {
          this.localStream = video;
        }
      }

      this.autoSetVideoMaximized();
      setTimeout(function () {
        for (var i = 0, len = _this7.$refs.videos.length; i < len; i++) {
          if (_this7.$refs.videos[i].id === stream.streamid) {
            _this7.$refs.videos[i].srcObject = stream.stream;
            break;
          }
        } // this.rtcmConnection.streamEvents.selectAll({
        //     isScreen: true
        // }).forEach(function(screenEvent) {
        //     this.videoList.forEach((item, index) => {
        //         if (item.id !== screenEvent.stream.streamid) {
        //             this.videoList[index].screen = true
        //         } else {
        //             this.videoList[index].screen = false
        //         }
        //     })
        // })

      }, 500);
      this.isLoading = false;
    },
    rtcmOnStreamEnded: function rtcmOnStreamEnded(stream) {
      this.recheckLiveParticipants(stream);
      this.autoSetVideoMaximized();
    },
    rtcmOnMediaError: function rtcmOnMediaError(error) {
      this.isLoading = false;

      if (this.getOnlineTimer) {
        clearTimeout(this.getOnlineTimer);
      }

      var msg = Object(_core_utils_media__WEBPACK_IMPORTED_MODULE_5__["showMediaPermissionError"])(error);
      this.meetingAction('leave', {
        error: {
          name: error.name,
          title: msg.title
        }
      }, {
        alert: false
      });
    },
    rtcmOnMute: function rtcmOnMute(stream) {
      var videoIndex = this.videoList.findIndex(function (v) {
        return v.id === stream.streamid;
      });
      var videoEle = this.$refs.videos.find(function (video) {
        return video.id === stream.streamid;
      });

      if (stream.muteType === 'video') {
        this.videoList[videoIndex].videoMuted = true;
      } else if (stream.muteType === 'audio') {
        this.videoList[videoIndex].muted = true;
      } else {
        this.videoList[videoIndex].videoMuted = true;
        this.videoList[videoIndex].muted = true;
        videoEle.srcObject = null;
      }
    },
    rtcmOnUnmute: function rtcmOnUnmute(stream) {
      var videoIndex = this.videoList.findIndex(function (v) {
        return v.id === stream.streamid;
      });
      var videoEle = this.$refs.videos.find(function (video) {
        return video.id === stream.streamid;
      });

      if (stream.unmuteType === 'video') {
        this.videoList[videoIndex].videoMuted = false;
      } else if (stream.unmuteType === 'audio') {
        this.videoList[videoIndex].muted = false;
      } else {
        this.videoList[videoIndex].videoMuted = false;
        this.videoList[videoIndex].muted = false;
        videoEle.srcObject = stream;
      }
    },
    rtcmOnRemoteMuteUnmute: function rtcmOnRemoteMuteUnmute(data) {
      var videoIndex = this.videoList.findIndex(function (video) {
        return video.id === data.streamid;
      });
      var stream = this.rtcmConnection.streamEvents[data.streamid].stream;

      if (data.hasOwnProperty('audioEnabled')) {
        if (data.audioEnabled) {
          this.videoList[videoIndex].audioMuted = false;
        } else {
          this.videoList[videoIndex].audioMuted = true;
        }
      } else if (data.hasOwnProperty('videoEnabled')) {
        // const videoEle = this.$refs.videos.find(video => {
        //     return video.id === data.streamid
        // })
        if (data.videoEnabled) {
          this.videoList[videoIndex].videoMuted = false;
        } else {
          this.videoList[videoIndex].videoMuted = true;
        }
      }
    },
    rtcmOnUserIdAlreadyTaken: function rtcmOnUserIdAlreadyTaken(useridAlreadyTaken, yourNewUserId) {
      this.rtcmConnection.userid = yourNewUserId;
    },
    // rtc action methods
    initMediaAndRtcmConnection: function initMediaAndRtcmConnection() {
      this.rtcmConnection = new rtcmulticonnection__WEBPACK_IMPORTED_MODULE_3___default.a();
      this.rtcmConnection.socketURL = this.socketURL;
      this.rtcmConnection.autoCreateMediaElement = false; // this.rtcmConnection.autoCloseEntireSession = true // set this line to close room as soon as room creator leaves

      this.rtcmConnection.enableLogs = true; // STAR_FIX_VIDEO_AUTO_PAUSE_ISSUES
      // via: https://github.com/muaz-khan/RTCMultiConnection/issues/778#issuecomment-524853468

      var bitrates = 512;
      var resolutions = 'Ultra-HD';
      var videoConstraints = {};

      if (resolutions == 'HD') {
        this.videoConstraints = {
          width: {
            ideal: 1280
          },
          height: {
            ideal: 720
          },
          frameRate: 30
        };
      }

      if (resolutions == 'Ultra-HD') {
        this.videoConstraints = {
          width: {
            ideal: 1920
          },
          height: {
            ideal: 1080
          },
          frameRate: 30
        };
      }

      this.rtcmConnection.session = _objectSpread({}, this.meetingRulesHost.session);
      this.rtcmConnection.sdpConstraints.mandatory = _objectSpread({}, this.meetingRulesHost.mandatory);
      this.rtcmConnection.mediaConstraints = {
        video: this.meetingRulesHost.mediaConstraints.video ? this.videoConstraints : false,
        audio: this.meetingRulesHost.mediaConstraints.audio,
        screen: this.meetingRulesHost.mediaConstraints.screen
      };
      var CodecsHandler = this.rtcmConnection.CodecsHandler;

      this.rtcmConnection.processSdp = function (sdp) {
        var codecs = 'vp8';

        if (codecs.length) {
          sdp = CodecsHandler.preferCodec(sdp, codecs.toLowerCase());
        }

        if (resolutions == 'HD') {
          sdp = CodecsHandler.setApplicationSpecificBandwidth(sdp, {
            audio: 128,
            video: bitrates,
            screen: bitrates
          });
          sdp = CodecsHandler.setVideoBitrates(sdp, {
            min: bitrates * 8 * 1024,
            max: bitrates * 8 * 1024
          });
        }

        if (resolutions == 'Ultra-HD') {
          sdp = CodecsHandler.setApplicationSpecificBandwidth(sdp, {
            audio: 128,
            video: bitrates,
            screen: bitrates
          });
          sdp = CodecsHandler.setVideoBitrates(sdp, {
            min: bitrates * 8 * 1024,
            max: bitrates * 8 * 1024
          });
        }

        return sdp;
      }; // END_FIX_VIDEO_AUTO_PAUSE_ISSUES


      this.rtcmConnection.iceServers = [];
      this.rtcmConnection.iceServers.push({
        urls: 'stun:st.kodemint.in:5349'
      });
      this.rtcmConnection.iceServers.push({
        urls: 'turn:st.kodemint.in:5349',
        credential: 'guest',
        username: 'password'
      });
      this.rtcmConnection.onstream = this.rtcmOnStream;
      this.rtcmConnection.onstreamended = this.rtcmOnStreamEnded;
      this.rtcmConnection.onmute = this.rtcmOnMute;
      this.rtcmConnection.onunmute = this.rtcmOnUnmute;
      this.rtcmConnection.onMediaError = this.rtcmOnMediaError;
      this.rtcmConnection.onUserIdAlreadyTaken = this.rtcmOnUserIdAlreadyTaken;
      this.rtcmConnection.setCustomSocketEvent('remoteMutedUnmuted');
    },
    closeConnectionAndStream: function closeConnectionAndStream() {
      var _this8 = this;

      if (this.rtcmConnection) {
        this.rtcmConnection.attachStreams.forEach(function (localStream) {
          localStream.stop();
        });
        this.rtcmConnection.removeStream(this.localStream.id);
        this.channel.whisper('RemovedStream', this.localStream);
        this.rtcmConnection.getAllParticipants().forEach(function (pid) {
          _this8.rtcmConnection.disconnectWith(pid);
        });
        this.rtcmConnection.leave();
        this.rtcmConnection.closeSocket();
        this.rtcmConnection = null;
      }

      this.videoList = [];
    },
    recheckLiveParticipants: function recheckLiveParticipants(stream) {
      var _this9 = this;

      var newList = [];
      var membersWhoLeft = [];
      var liveParticipants = this.rtcmConnection ? this.rtcmConnection.getAllParticipants() : [];
      this.videoList.forEach(function (item, index) {
        var userIndex = liveParticipants.findIndex(function (m) {
          return m === item.streamUserId;
        });

        if ((!stream || stream && item.id !== stream.streamid) && (item.local || !item.local && userIndex !== -1)) {
          newList.push(item);
        } else {
          item.status = false;
          newList.push(item);
        }
      });
      this.videoList = newList;
      setTimeout(function () {
        _this9.videoList = _this9.videoList.filter(function (v) {
          return v.status;
        });

        _this9.autoSetVideoMaximized();
      }, 3000);
    },
    autoSetVideoMaximized: function autoSetVideoMaximized() {
      if (this.videoList.length > 1) {
        var maximizedRemoteVideoIndex = this.videoList.findIndex(function (v) {
          return !v.local && v.maximized && v.status;
        });

        if (maximizedRemoteVideoIndex === -1) {
          var remoteVideoIndex = this.videoList.findIndex(function (v) {
            return !v.local;
          });

          if (remoteVideoIndex !== -1) {
            this.videoList = this.videoList.map(function (v, index) {
              v.maximized = false;

              if (index === remoteVideoIndex) {
                v.maximized = true;
              }

              return v;
            });
          }
        }
      } else if (this.videoList.length) {
        this.videoList[0].maximized = true;
      }
    },
    openRoom: function openRoom(meetingRoomId) {
      var _this10 = this;

      this.rtcmConnection.session = _objectSpread({}, this.meetingRulesHost.session);
      this.rtcmConnection.mediaConstraints = _objectSpread({}, this.meetingRulesHost.mediaConstraints);
      this.rtcmConnection.sdpConstraints.mandatory = _objectSpread({}, this.meetingRulesHost.mandatory);
      this.rtcmConnection.open(meetingRoomId, function (isRoomOpened, roomid, error) {
        _this10.rtcmConnection.socket.on('remoteMutedUnmuted', _this10.rtcmOnRemoteMuteUnmute);

        _this10.logEvent('Room Opened: ', roomid);

        _this10.isLoading = false;

        if (error) {
          formUtil.handleErrors(error);
        } else if (isRoomOpened === true) {
          if (_this10.getOnlineTimer) {
            clearTimeout(_this10.getOnlineTimer);
          }

          _this10.updatePageConfigs(true);

          _this10.channel.whisper('MeetingRoomCreated', {
            roomId: meetingRoomId
          });

          _this10.$toasted.success(_this10.$t('meeting.meeting_created'), _this10.$toastConfig);
        }
      });
    },
    joinRoom: function joinRoom(meetingRoomId) {
      var _this11 = this;

      this.rtcmConnection.session = _objectSpread({}, this.meetingRulesGuest.session);
      this.rtcmConnection.mediaConstraints = _objectSpread({}, this.meetingRulesGuest.mediaConstraints);
      this.rtcmConnection.sdpConstraints.mandatory = _objectSpread({}, this.meetingRulesGuest.mandatory);
      this.rtcmConnection.join(meetingRoomId, function (isJoined, roomid, error) {
        _this11.rtcmConnection.socket.on('remoteMutedUnmuted', _this11.rtcmOnRemoteMuteUnmute);

        _this11.logEvent('Room Joined: ', roomid);

        _this11.isLoading = false;

        if (isJoined === false || error) {
          formUtil.handleErrors(error);
        } else {
          if (_this11.getOnlineTimer) {
            clearTimeout(_this11.getOnlineTimer);
          }

          _this11.updatePageConfigs();

          _this11.$toasted.success(_this11.$t('meeting.meeting_joined'), _this11.$toastConfig);
        }
      });
    },
    shareScreen: function shareScreen() {
      this.isLoading = true;
      this.rtcmConnection.addStream({
        screen: true
      });
    },
    stopSharingScreen: function stopSharingScreen() {
      var _this12 = this;

      this.isLoading = true;
      var found = this.$refs.videos.find(function (video) {
        return video.id === _this12.localScreenStreamid;
      });

      if (found && found.srcObject) {
        var tracks = found.srcObject.getTracks();
        tracks.forEach(function (track) {
          track.removeEventListener('ended', _this12.stopSharingScreen);
          track.enabled = false;
          track.stop();
        });
        this.rtcmConnection.removeStream(this.localScreenStreamid);
        this.channel.whisper('RemovedStream', found.srcObject); // this.videoList = this.videoList.filter(video => video.id !== this.localScreenStreamid)

        this.localScreenStreamid = null;
        this.isLoading = false;
      } else {
        this.localScreenStreamid = null;
        this.isLoading = false;
      }
    },
    getOnline: function getOnline() {
      var _this13 = this;

      this.isLoading = true;
      var meetingStatusEalier = this.entity.status;
      this.startGetOnlineTimer();
      this.initMediaAndRtcmConnection();
      this.Custom({
        url: "/".concat(this.initUrl, "/").concat(this.uuid, "/join"),
        method: 'post'
      }).then(function (response) {
        _this13.meetingRoomId = response.meeting.roomId;
        _this13.entity = response.meeting;
        _this13.rtcmConnection.extra = {
          username: _this13.user.username,
          name: _this13.user.name,
          uuid: _this13.user.uuid,
          image: _this13.profile.image,
          audioMuted: !_this13.pageConfigs.enableAudio,
          videoMuted: !_this13.pageConfigs.enableVideo
        };

        _this13.rtcmConnection.checkPresence(_this13.meetingRoomId, function (isRoomExist, roomid) {
          if (isRoomExist === true) {
            _this13.joinRoom(_this13.meetingRoomId);
          } else {
            if (_this13.entity.canModerate) {
              _this13.openRoom(_this13.meetingRoomId);
            } else {
              _this13.isLoading = false;

              if (_this13.getOnlineTimer) {
                clearTimeout(_this13.getOnlineTimer);
              }

              _this13.$toasted.error(_this13.$t('meeting.room_not_found'), _this13.$toastConfig.error);
            }
          }
        });
      })["catch"](function (error) {
        _this13.isLoading = false;
        clearTimeout(_this13.getOnlineTimer);
        formUtil.handleErrors(error);
      });
    },
    getOffline: function getOffline() {
      var _this14 = this;

      this.isLoading = true;
      this.meetingAction('leave', null, {
        alert: 'confirm',
        callback: function callback(e) {
          _this14.closeConnectionAndStream();
        }
      });
    },
    // meeting action methods
    meetingAction: function meetingAction(action) {
      var _this15 = this;

      var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var opts = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {
        alert: 'confirm'
      };
      var defaultOpts = {
        alert: 'confirm'
      };
      opts = Object.assign({}, defaultOpts, opts);

      var callApi = function callApi(dataToSend) {
        _this15.isLoading = true;
        data = dataToSend ? dataToSend : data;

        _this15.Custom({
          url: "/".concat(_this15.initUrl, "/").concat(_this15.uuid, "/").concat(action),
          method: 'post',
          data: data
        }).then(function (response) {
          _this15.entity = response.meeting;

          _this15.$toasted.success(response.message, _this15.$toastConfig);

          if (opts.callback) {
            opts.callback(response);
          }

          _this15.isLoading = false;
        })["catch"](function (error) {
          _this15.isLoading = false;
          formUtil.handleErrors(error);
        });
      };

      if (!action) {
        this.isLoading = false;
        return;
      }

      if (opts.alert === 'confirm' || opts.alert === true) {
        formUtil.confirmAction().then(function (result) {
          if (result.value) {
            callApi();
          } else {
            _this15.isLoading = false;
            result.dismiss === _core_configs_sweet_alert__WEBPACK_IMPORTED_MODULE_7__["default"].DismissReason.cancel;
          }
        });
      } else if (opts.alert === 'input') {
        swtAlert.fire({
          title: opts.title,
          input: 'text',
          inputPlaceholder: opts.inputPlaceholder,
          showCancelButton: true,
          confirmButtonText: 'Proceed!',
          cancelButtonText: 'Go Back!'
        }).then(function (result) {
          if (result.value) {
            var toSend = {};
            toSend[opts.fieldName] = result.value;
            callApi(toSend);
          } else {
            _this15.isLoading = false;
            result.dismiss === _core_configs_sweet_alert__WEBPACK_IMPORTED_MODULE_7__["default"].DismissReason.cancel;
          }
        });
      } else {
        callApi();
      }
    },
    leaveMeeting: function leaveMeeting() {
      this.meetingAction('leave');
    },
    endMeeting: function endMeeting() {
      var _this16 = this;

      this.isLoading = true;
      this.meetingAction('end', null, {
        callback: function callback(e) {
          _this16.closeConnectionAndStream();

          _this16.channel.whisper('MeetingEnded', {
            status: e.meeting.status
          });
        }
      });
    },
    // page methods
    getInitialData: function getInitialData() {
      var _this17 = this;

      this.isLoading = true;
      return this.Get({
        uuid: this.uuid
      }).then(function (response) {
        _this17.entity = response;
        Object(_core_utils_media__WEBPACK_IMPORTED_MODULE_5__["initMedia"])();

        if (response.roomId) {
          setTimeout(function () {
            _this17.initMediaAndRtcmConnection();

            _this17.rtcmConnection.checkPresence(response.roomId, function (isRoomExist, roomid) {
              _this17.roomIdAlive = !!isRoomExist;
            });
          }, 1000);
        }

        _this17.joinChannel();

        _this17.isLoading = false;
        return response;
      })["catch"](function (error) {
        _this17.isLoading = false;
        formUtil.handleErrors(error);

        _this17.$router.push({
          name: _this17.fallBackRoute
        });

        return error;
      });
    },
    logEvent: function logEvent(msg, args) {
      var type = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'log';

      if (this.rtcmConnection.enableLogs) {
        if (type === 'log') {
          console.log(msg, args);
        } else if (type === 'error') {
          console.error(msg, args);
        } else if (type === 'debug') {
          console.debug(msg, args);
        }
      }
    },
    destroyPage: function destroyPage() {
      if (this.countdownInterval) {
        clearInterval(this.countdownInterval);
      }

      this.SetUiConfig({
        pageHeaderShow: true,
        pageFooterShow: true
      });
      this.closeConnectionAndStream();

      if (window.Echo) {
        if (this.channel) {
          this.channel.stopListening('MeetingStatusChanged');
          this.channel.stopListening('NewComment');
        }

        window.Echo.leave("Meeting.".concat(this.uuid));
      }
    },
    doInit: function doInit() {
      this.Init({
        url: this.initUrl
      });

      if (window.isDuplicate()) {
        this.duplicateTab = true;
      }

      this.getInitialData();
    }
  }),
  mounted: function mounted() {
    if (this.$route.params.uuid) {
      this.uuid = this.$route.params.uuid;
    }

    this.doInit();
    window.addEventListener('beforeunload', this.beforeUnload);
  },
  created: function created() {
    this.SetUiConfig({
      pageHeaderShow: false,
      pageFooterShow: false
    }); // detect 2G and alert

    if (navigator.connection && navigator.connection.type === 'cellular' && navigator.connection.downlinkMax <= 0.115) {
      alert('2G is not supported. Please use a better internet service.');
    }
  },
  beforeDestroy: function beforeDestroy() {
    if (!window.isPageDestroyed) {
      this.isLoading = true;
      this.destroyPage();

      if (this.entity && this.entity.status === 'live') {
        this.Custom({
          url: "/".concat(this.initUrl, "/").concat(this.uuid, "/leave"),
          method: 'post'
        });
      }

      window.isPageDestroyed = true;
    }
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    if (!to.params.uuid) {
      next(from);
    } else {
      next(function (vm) {
        vm.prevRoute = from;
      });
    }
  },
  beforeRouteLeave: function beforeRouteLeave(to, from, next) {
    this.isLoading = true;
    this.destroyPage();
    window.removeEventListener('beforeunload', this.beforeUnload);

    if (this.entity && this.entity.status === 'live') {
      this.Custom({
        url: "/".concat(this.initUrl, "/").concat(this.uuid, "/leave"),
        method: 'post'
      }).then(function (response) {
        window.isPageDestroyed = true;
        next();
      })["catch"](function (error) {
        window.isPageDestroyed = true;
        next();
      });
    } else {
      window.isPageDestroyed = true;
      next();
    }
  }
});

/***/ }),

/***/ "./resources/js/views/app/call/live.vue":
/*!**********************************************!*\
  !*** ./resources/js/views/app/call/live.vue ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _live_vue_vue_type_template_id_15f1b9a0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./live.vue?vue&type=template&id=15f1b9a0& */ "./resources/js/views/app/call/live.vue?vue&type=template&id=15f1b9a0&");
/* harmony import */ var _live_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./live.vue?vue&type=script&lang=js& */ "./resources/js/views/app/call/live.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _live_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./live.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/views/app/call/live.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _live_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _live_vue_vue_type_template_id_15f1b9a0___WEBPACK_IMPORTED_MODULE_0__["render"],
  _live_vue_vue_type_template_id_15f1b9a0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/app/call/live.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/app/call/live.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/js/views/app/call/live.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_live_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./live.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/call/live.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_live_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/app/call/live.vue?vue&type=style&index=0&lang=scss&":
/*!********************************************************************************!*\
  !*** ./resources/js/views/app/call/live.vue?vue&type=style&index=0&lang=scss& ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_live_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--9-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--9-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./live.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/call/live.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_live_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_live_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_live_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_live_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_dist_cjs_js_ref_9_3_node_modules_vue_loader_lib_index_js_vue_loader_options_live_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/views/app/call/live.vue?vue&type=template&id=15f1b9a0&":
/*!*****************************************************************************!*\
  !*** ./resources/js/views/app/call/live.vue?vue&type=template&id=15f1b9a0& ***!
  \*****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_live_vue_vue_type_template_id_15f1b9a0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./live.vue?vue&type=template&id=15f1b9a0& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/app/call/live.vue?vue&type=template&id=15f1b9a0&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_live_vue_vue_type_template_id_15f1b9a0___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_live_vue_vue_type_template_id_15f1b9a0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);
//# sourceMappingURL=live.js.map?id=7378b9d898718b47fe4b