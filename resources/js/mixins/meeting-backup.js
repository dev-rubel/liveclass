let ebml = document.createElement('script')
ebml.setAttribute('src', '/js/EBML.js')
document.head.appendChild(ebml)

import { mapGetters, mapActions } from 'vuex'

import AnimatedNumber from '@core/components/AnimatedNumber'
import io from 'socket.io-client'
import RTCMultiConnection from 'rtcmulticonnection'
import * as RecordRTC from 'recordrtc'
import 'adapterjs'
import screenfull from 'screenfull'

import { setupPusher } from '@js/echo-setup'
import { initMedia, showMediaPermissionError, playIncomingMessage, playScreenshot, share } from '@core/utils/media'
import { calculateTimeDuration } from '@core/utils'
import { clearStore } from '@core/utils/auth'
import * as MomentTz from '@core/filters/momentz'

import '@core/plugins/detect-duplicate-tab'
import Swal from '@core/configs/sweet-alert'

import AppLogo from '@components/AppLogo'

window.io = io

export default {
    components: {
        AnimatedNumber,
        RTCMultiConnection,
        AppLogo,
    },
    data() {
        return {
            uuid: null,
            entity: null,
            isLoading: false,
            prevRoute: null,
            channel: null,

            fullScreenItemId: null,
            meetingRoomId: null,
            initUrl: 'meetings',
            fallBackRoute: 'appMeetingList',
            newMessages: false,
            roomIdAlive: false,
            showFlipClock: true,
            duplicateTab: false,
            fullScreenInOn: false,
            footerAutoHide: false,
            membersLive: [],
            videoList: [],
            socketURL: 'aHR0cHM6Ly9zaWduYWwua29kZW1pbnQuaW46OTAwMS8=',
            // socketURL: 'aHR0cDovL2xvY2FsaG9zdDo5MDAxLw==',
            rtcmConnection: null,
            localVideo: null,
            localScreenStreamid: null,
            videoConstraints: {},
            pageConfigs: {
                streamGrid: false,
                hasAgenda: true,
                hasChat: true,
                showAgenda: true,
                enableAudio: true,
                enableVideo: true,
                autoplay: true,
                showEnableAudioBtn: true,
                showEnableVideoBtn: true,
                showShareScreenBtn: true,
                showRecordingBtn: false,
                showHandBtn: false,
                enableRecording: false,
                isHandUp: false,
                recording: false,
                layout: 'fullscreen',
            },
            meetingRulesHost: {
                session: {
                    audio: false,
                    video: false,
                    screen: false,
                    oneway: false,
                },
                mediaConstraints: {
                    audio: false,
                    video: false,
                    screen: false,
                },
                mandatory: {
                    OfferToReceiveAudio: false,
                    OfferToReceiveVideo: false,
                },
            },
            meetingRulesGuest: {
                session: {
                    audio: false,
                    video: false,
                    screen: false,
                    oneway: false,
                },
                mediaConstraints: {
                    audio: false,
                    video: false,
                    screen: false,
                },
                mandatory: {
                    OfferToReceiveAudio: false,
                    OfferToReceiveVideo: false,
                },
            },
            snoozeOpts: [
                { uuid: 5, name: 5, type: 'm' },
                { uuid: 10, name: 10, type: 'm' },
                { uuid: 15, name: 15, type: 'm' },
                { uuid: 30, name: 30, type: 'm' },
                { uuid: 60, name: 1, type: 'h' },
            ],
            recorded: false,
            recordingDuration: null,
        }
    },
    computed: {
        ...mapGetters('config', [
            'vars',
            'configs',
            'uiConfigs',
        ]),
        ...mapGetters('user', {
            'userUuid': 'uuid'
        }),
        ...mapGetters('user', [
            'profile',
            'username',
            'hasPermission',
        ]),
        user() {
            return {
                uuid: this.userUuid,
                username: this.username,
                name: this.profile.name,
            }
        },
        hasVideos() {
            return this.videoList && this.videoList.length > 0 ? this.videoList.length : 0
        },
        hasVideosClasses() {
            if (!this.hasVideos) {
                return 'has-no-video'
            }

            return 'has-videos ' + (this.hasVideos > 3 ? 'has-gt-3-videos' : (this.hasVideos > 1 ? 'has-2-3-videos' : 'has-1-video'))
        },
        liveMembersCount() {
            return this.membersLive.length - 1
        },
        startDateTimeIsFuture() {
            const isInFuture = this.entity && this.showFlipClock && this.isStartDateTimeInFuture()
            if (isInFuture) {
                this.startCountDown()
            } else {
                if (window.countdownInterval) {
                    clearInterval(window.countdownInterval)
                }
            }
            return isInFuture
        },
    },
    watch: {
        liveMembersCount(newVal, oldVal) {
            if (!window.isLiveMeetingDestroyed && newVal !== oldVal) {
                this.meetingRoomCreated(this.entity)
            }
        },
    },
    methods: {
        ...mapActions('config', [
            'SetUiConfig',
        ]),
        ...mapActions('common', [
            'Init',
            'Get',
            'Custom',
            'GetPreRequisite',
        ]),

        shareURL() {
            if(!this.entity) {
                return 
            }

            this.$gaEvent('engagement', 'shareURL', 'Shown')

            let url = window.location.protocol+'//'+window.location.hostname+(window.location.port ? ':'+window.location.port: '')

            url = url + (this.entity.identifier ? `/m/${this.entity.identifier}` : `/app/live/meetings/${this.entity.uuid}`)

            const alertTitle = this.entity.identifier ? `${$t('meeting.meeting_code')}: <strong class="ml-2"> ${this.entity.identifier}</strong>` : null

            share({
                title: $t('meeting.user_inviting_msg', {attribute: this.user.name }),
                alertTitle: alertTitle,
                alertHtml:
                    `${$t('misc.share_alert.text')}<br>` +
                    `<small class='text-muted'>${url}</small><br>`,
                url: url
            }, () => {
                this.$gaEvent('engagement', 'shareURL', 'Copied')

                this.$toasted.success($t('meeting.url_has_been_copied'), this.$toastConfig)
            })
        },

        // query / countdown / timer methods
        isStartDateTimeInFuture() {
            return this.entity && this.entity.startDateTime && moment(this.entity.startDateTime, this.vars.serverDateTimeFormat)
                .isAfter(moment())
        },
        startCountDown() {
            if (window.countdownInterval) {
                clearInterval(window.countdownInterval)
            }

            window.countdownInterval = window.setInterval(() => {
                if (this.isStartDateTimeInFuture()) {
                    this.showFlipClock = true
                } else {
                    if (this.showFlipClock && !document.hasFocus()) {
                        playIncomingMessage()
                    }
                    this.showFlipClock = false
                }
            }, 1000)
        },
        startGetOnlineTimer() {
            if (window.getOnlineTimer) {
                clearTimeout(window.getOnlineTimer)
            }
            if (this.videoList.length) {
                return
            }
            const alertContent = {
                title: $t('misc.refresh_confirm.title'),
                text: $t('misc.refresh_confirm.text'),
                showCancelButton: true,
                confirmButtonText: $t('misc.refresh_confirm.confirm_btn'),
                cancelButtonText: $t('misc.refresh_confirm.cancel_btn')
            }
            window.getOnlineTimer = setTimeout(() => {
                swtAlert.fire(alertContent)
                    .then((result) => {
                        if (result.value) {
                            window.location.reload()
                        } else {
                            this.startGetOnlineTimer()
                        }
                    })
            }, 15000)
        },

        startRecording() {
            this.$gaEvent('engagement', 'startRecording')

            this.pageConfigs.recording = true
            this.recorded = false
            let recorderInstance = this.rtcmConnection.recorder
            recorderInstance.startRecording()

            const recordingStartedAt = new Date().getTime()

            if (window.recordingDurationInterval) {
                this.recordingDuration = null
                clearInterval(window.recordingDurationInterval)
            }

            window.recordingDurationInterval = window.setInterval(() => {
                this.recordingDuration = calculateTimeDuration((new Date().getTime() - recordingStartedAt) / 1000)
            }, 1000)

            const internalRecorder = recorderInstance.getInternalRecorder()

            this.rtcmConnection.streamEvents.selectAll().forEach((streamEvent) => {
                if(streamEvent.type !== 'local') {
                    internalRecorder.addStreams([streamEvent.stream])
                }
            })
        },
        stopRecording() {
            this.$gaEvent('engagement', 'stopRecording')

            this.pageConfigs.recording = false
            this.recorded = true
            let recorderInstance = this.rtcmConnection.recorder
            
            if(!recorderInstance) {
                return alert('No recorder found!')
            }

            recorderInstance.stopRecording(() => {
                if (window.recordingDurationInterval) {
                    this.recordingDuration = null
                    clearInterval(window.recordingDurationInterval)
                }

                RecordRTC.getSeekableBlob(recorderInstance.getBlob(), (seekableBlob) => {
                    const recordedVideo = URL.createObjectURL(seekableBlob)
                    const downloadLinkBtn = document.createElement("a")
                    recorderInstance.destroy()
                    recorderInstance = null

                    RecordRTC.invokeSaveAsDialog(seekableBlob, "meeting_record_" + moment().format(this.vars.serverDateTimeFormat) + ".webm");

                    downloadLinkBtn.style.display = "none"
                    downloadLinkBtn.href = recordedVideo
                    downloadLinkBtn.download = "meeting_record_" + moment().format(this.vars.serverDateTimeFormat) + ".webm"
                    document.body.appendChild(downloadLinkBtn)
                    // downloadLinkBtn.click()

                    setTimeout(() => { 
                        document.body.removeChild(downloadLinkBtn)
                        window.URL.revokeObjectURL(downloadLinkBtn) 
                        this.recorded = false
                    }, 100)
                })
            })
        },

        // toggle methods
        toggleHandUp() {
            this.$gaEvent('engagement', 'toggleHandUp')

            if (window.lowerHandTimer) {
                clearTimeout(window.lowerHandTimer)
            }

            let found = this.$refs.videos.find(video => {
                return video.id === this.localVideo.id
            })
            const videoIndex = this.videoList.findIndex(video => {
                return video.id === this.localVideo.id
            })

            if (found && found.srcObject) {
                const stream = found.srcObject

                if (this.pageConfigs.isHandUp) {
                    this.pageConfigs.isHandUp = false
                    this.videoList[videoIndex].isHandUp = false
                    this.rtcmConnection.socket.emit('remoteHandToggled', {
                        isHandUp: false,
                        streamid: stream.streamid
                    })
                } else {
                    this.pageConfigs.isHandUp = true
                    this.videoList[videoIndex].isHandUp = true
                    this.rtcmConnection.socket.emit('remoteHandToggled', {
                        isHandUp: true,
                        streamid: stream.streamid
                    })

                    window.lowerHandTimer = setTimeout(() => {
                        this.toggleHandUp()
                    }, 30000)
                }

                this.rtcmConnection.extra.isHandUp = this.pageConfigs.isHandUp
                this.rtcmConnection.updateExtraData()
            } else {
                this.localVideo = null
            }
        },

        toggleAudio() {
            let found = this.$refs.videos.find(video => {
                return video.id === this.localVideo.id
            })
            const videoIndex = this.videoList.findIndex(video => {
                return video.id === this.localVideo.id
            })

            if (found && found.srcObject) {
                const stream = found.srcObject
                const tracks = stream.getAudioTracks()
                tracks.forEach(track => {
                    if (this.pageConfigs.enableAudio) {
                        this.pageConfigs.enableAudio = false
                        track.enabled = false
                        this.videoList[videoIndex].audioMuted = true
                        this.rtcmConnection.socket.emit('remoteMutedUnmuted', {
                            audioEnabled: false,
                            streamid: stream.streamid
                        })
                    } else {
                        this.pageConfigs.enableAudio = true
                        track.enabled = true
                        this.videoList[videoIndex].audioMuted = true
                        this.rtcmConnection.socket.emit('remoteMutedUnmuted', {
                            audioEnabled: true,
                            streamid: stream.streamid
                        })
                    }
                })

                this.rtcmConnection.extra.audioMuted = !this.pageConfigs.enableAudio
                this.rtcmConnection.updateExtraData()
            } else {
                this.localVideo = null
            }
        },
        toggleVideo() {
            let found = this.$refs.videos.find(video => {
                return video.id === this.localVideo.id
            })
            const videoIndex = this.videoList.findIndex(video => {
                return video.id === this.localVideo.id
            })

            if (found && found.srcObject) {
                const stream = found.srcObject
                const tracks = stream.getVideoTracks()
                tracks.forEach(track => {
                    if (this.pageConfigs.enableVideo) {
                        this.pageConfigs.enableVideo = false
                        track.enabled = false
                        this.videoList[videoIndex].videoMuted = true
                        this.rtcmConnection.socket.emit('remoteMutedUnmuted', {
                            videoEnabled: false,
                            streamid: stream.streamid
                        })
                    } else {
                        this.pageConfigs.enableVideo = true
                        track.enabled = true
                        this.videoList[videoIndex].videoMuted = false
                        this.rtcmConnection.socket.emit('remoteMutedUnmuted', {
                            videoEnabled: true,
                            streamid: stream.streamid
                        })
                    }
                })

                this.rtcmConnection.extra.videoMuted = !this.pageConfigs.enableVideo
                this.rtcmConnection.updateExtraData()
            } else {
                this.localVideo = null
            }
        },
        toggleRemoteAudio(videoItem, itemIndex) {
            if (videoItem.muted) {
                this.videoList[itemIndex].muted = false
                this.$refs.videos[itemIndex].muted = false
            } else {
                this.videoList[itemIndex].muted = true
                this.$refs.videos[itemIndex].muted = true
            }

            // if(videoItem.streamUserId) {
            //     const streamByUserId = this.rtcmConnection.streamEvents.selectFirst({ userid: videoItem.streamUserId }).stream

            //     if(videoItem.muted) {
            //         streamByUserId.unmute('audio')
            //     } else {
            //         streamByUserId.mute('audio')
            //     }
            // }
        },
        toggleEleFullScreen(videoItem, itemIndex) {
            const targetParentEl = this.$refs['videoListEle']
            this.fullScreenItemId = videoItem.id
            this.$fullscreen.toggle(targetParentEl, {
                wrap: false,
                callback: (fullscreen) => {
                    this.fullScreenInOn = fullscreen
                }
            })
        },
        toggleFullScreen(to) {
            if(screenfull.isEnabled) {
                if(to) {
                    screenfull.request()
                } else {
                    screenfull.exit()
                }
            }
        },
        toggleFooterAutoHide() {
            this.$gaEvent('engagement', 'toggleFooterAutoHide')
            this.footerAutoHide = !this.footerAutoHide
        },
        toggleStreamGrid() {
            this.$gaEvent('engagement', 'toggleStreamGrid')
            this.pageConfigs.streamGrid = !this.pageConfigs.streamGrid
        },
        toggleLayout(layout) {
            this.pageConfigs.layout = layout
        },
        toggleAgenda() {
            this.$gaEvent('engagement', 'toggleAgenda')
            this.pageConfigs.showAgenda = !this.pageConfigs.showAgenda
        },
        changeFocus(item) {
            this.videoList.forEach(v => {
                v.maximized = v.id === item.id
            })
        },

        // event callback methods
        beforeUnload(event) {
            if (this.localVideo) {
                window.meetingChannel.whisper('RemovedStream', this.localVideo)
            }
        },

        // channel event callback methods
        afterJoiningChannel(members) {
            this.membersLive = members
        },
        newMemberJoining(member) {
            this.membersLive.push(member)
        },
        memberLeaving(member) {
            this.membersLive = this.membersLive.filter(u => (u.uuid !== member.uuid))
        },
        meetingStatusChanged(e) {
            if (e.uuid === this.entity.uuid) {

                if (this.entity.status === e.status && !e.delayed) {
                    return
                }

                this.entity.status = e.status
                this.entity.startDateTime = e.startDateTime
                let meetingStatus = e.status

                if (meetingStatus === 'scheduled' && e.delayed) {
                    meetingStatus = 'delayed'
                }

                const statusUpdateMessages = {
                    'live': 'meeting.is_live_now',
                    'delayed': 'meeting.meeting_delayed',
                    'cancelled': 'meeting.meeting_cancelled',
                    'ended': 'meeting.meeting_ended',
                }

                this.$toasted.success($t(statusUpdateMessages[meetingStatus]), this.$toastConfig)
            }
        },
        gotNewMessage() {
            if(!(this.configs.chat && this.configs.chat.enabled)) {
                return
            }
            
            if (!this.pageConfigs.showChat || this.fullScreenInOn) {
                this.newMessages = true
                playIncomingMessage()
            }
        },
        meetingRoomCreated(e) {
            this.entity.roomId = e.roomId
            this.initMediaAndRtcmConnection()
            this.rtcmConnection.checkPresence(this.entity.roomId, (isRoomExist, roomid) => {
                this.roomIdAlive = !!isRoomExist
            })
        },
        banAttendee(e) {
            let found = this.$refs.videos.find(video => {
                return video.id === e.id
            })

            if (found && found.srcObject) {
                const stream = found.srcObject
                this.rtcmConnection.removeStream(stream.streamid)
            }

            if(e.uuid === this.userUuid) {
                this.closeConnectionAndStream()
                this.getInitialData()
            }

            this.$toasted.info($t('meeting.ban_notification'), this.$toastConfig.info)
        },
        streamRemoved(e) {
            if(this.rtcmConnection && e.id) {
                this.rtcmConnection.removeStream(e.id)
            }
            this.rtcmOnStreamEnded(e)
        },
        meetingEnded(e) {
            this.closeConnectionAndStream()
            this.getInitialData()
        },

        // channel action methods
        joinChannel() {
            window.meetingChannel = window.Echo.join(`Meeting.${this.uuid}`)
            window.meetingChannel.here(this.afterJoiningChannel)
                .joining(this.newMemberJoining)
                .leaving(this.memberLeaving)
                .listen('MeetingStatusChanged', this.meetingStatusChanged)
                .listen('NewMessage', this.gotNewMessage)
                .listenForWhisper('MeetingRoomCreated', this.meetingRoomCreated)
                .listenForWhisper('RemovedStream', this.streamRemoved)
                .listenForWhisper('BanAttendee', this.banAttendee)
                .listenForWhisper('MeetingEnded', this.meetingEnded)
        },

        // rtcm event callback methods
        rtcmOnStream(stream) {
            this.logEvent('On Stream: ', stream)
            let found = this.videoList.find(video => {
                return video.id === stream.streamid
            })

            let streamInstance = stream.stream.idInstance ? JSON.parse(stream.stream.idInstance) : stream.stream
            if (streamInstance.isScreen && stream.type === 'local') {
                this.localScreenStreamid = stream.streamid

                let tracks = stream.stream.getTracks()
                tracks.forEach(track => {
                    track.addEventListener('ended', this.stopSharingScreen)
                })
            }

            if (this.videoList.length > 1) {
                this.recheckLiveParticipants(null)
            }

            if (found === undefined) {
                let video = {
                    id: stream.streamid,
                    streamUserId: stream.userid,
                    muted: stream.type === 'local',
                    ...stream.extra,
                    extra: stream.extra,
                    maximized: stream.type === 'local' && !streamInstance.isScreen,
                    local: stream.type === 'local',
                    screen: this.localScreenStreamid === stream.streamid,
                    status: true,
                    hasAudio: streamInstance.isAudio || streamInstance.audio,
                    hasVideo: streamInstance.isVideo || streamInstance.video || streamInstance.isScreen || streamInstance.screen,
                }

                if (streamInstance.isScreen) {
                    video.videoMuted = false
                }

                // let foundUserIndex = this.videoList.findIndex(item => item.uuid === stream.extra.uuid)

                // if (foundUserIndex >= 0) {
                //     const foundUserVideo = this.videoList[foundUserIndex]
                //     if (!foundUserVideo.status) {
                //         let newList = this.videoList.map(item => item.uuid !== foundUserVideo.uuid)
                //         this.videoList = newList
                //     }
                // }

                this.videoList.push(video)

                if (stream.type === 'local' && this.localScreenStreamid !== stream.streamid) {
                    this.localVideo = video
                }
            }

            this.autoSetVideoMaximized()



            if(this.pageConfigs.enableRecording) {
                let recorderInstance = this.rtcmConnection.recorder

                if(!recorderInstance) {
                    recorderInstance = RecordRTC([stream.stream], {
                        type: 'video',
                        mimeType: "video/webm",
                        timeSlice: 1000, // pass this parameter 
                        disableLogs: false,
                    })

                    this.rtcmConnection.recorder = recorderInstance
                } else {
                    const internalRecorder = recorderInstance.getInternalRecorder()
                    if(this.pageConfigs.recording && internalRecorder) {
                        internalRecorder.addStreams([stream.stream])
                    }
                }

                if(!this.rtcmConnection.recorder.streams) {
                    this.rtcmConnection.recorder.streams = []
                }

                this.rtcmConnection.recorder.streams.push(stream.stream)
            }

            setTimeout(() => {
                for (let i = 0, len = this.$refs.videos.length; i < len; i++) {
                    if (this.$refs.videos[i].id === stream.streamid) {
                        this.$refs.videos[i].srcObject = stream.stream
                        break
                    }
                }

                // this.rtcmConnection.streamEvents.selectAll({
                //     isScreen: true
                // }).forEach(function(screenEvent) {
                //     this.videoList.forEach((item, index) => {
                //         if (item.id !== screenEvent.stream.streamid) {
                //             this.videoList[index].screen = true
                //         } else {
                //             this.videoList[index].screen = false
                //         }
                //     })
                // })
            }, 500)

            this.isLoading = false
        },
        rtcmOnStreamEnded(stream) {
            this.recheckLiveParticipants(stream)
            this.autoSetVideoMaximized()
        },
        rtcmOnMediaError(error) {
            this.isLoading = false
            if (window.getOnlineTimer) {
                clearTimeout(window.getOnlineTimer)
            }
            const msg = showMediaPermissionError(error)
            this.meetingAction('leave', { error: { name: error.name, title: msg.title } }, { alert: false })
        },
        rtcmOnMute(stream) {
            const videoIndex = this.videoList.findIndex(v => v.id === stream.streamid)
            const videoEle = this.$refs.videos.find(video => {
                return video.id === stream.streamid
            })

            if (stream.muteType === 'video') {
                this.videoList[videoIndex].videoMuted = true
            } else if (stream.muteType === 'audio') {
                this.videoList[videoIndex].muted = true
            } else {
                this.videoList[videoIndex].videoMuted = true
                this.videoList[videoIndex].muted = true
                videoEle.srcObject = null
            }
        },
        rtcmOnUnmute(stream) {
            const videoIndex = this.videoList.findIndex(v => v.id === stream.streamid)
            const videoEle = this.$refs.videos.find(video => {
                return video.id === stream.streamid
            })

            if (stream.unmuteType === 'video') {
                this.videoList[videoIndex].videoMuted = false
            } else if (stream.unmuteType === 'audio') {
                this.videoList[videoIndex].muted = false
            } else {
                this.videoList[videoIndex].videoMuted = false
                this.videoList[videoIndex].muted = false
                videoEle.srcObject = stream
            }
        },
        rtcmOnRemoteMuteUnmute(data) {
            const videoIndex = this.videoList.findIndex(video => {
                return video.id === data.streamid
            })

            const stream = this.rtcmConnection.streamEvents[data.streamid].stream

            if (data.hasOwnProperty('audioEnabled')) {
                if (data.audioEnabled) {
                    this.videoList[videoIndex].audioMuted = false
                } else {
                    this.videoList[videoIndex].audioMuted = true
                }
            } else if (data.hasOwnProperty('videoEnabled')) {
                // const videoEle = this.$refs.videos.find(video => {
                //     return video.id === data.streamid
                // })

                if (data.videoEnabled) {
                    this.videoList[videoIndex].videoMuted = false
                } else {
                    this.videoList[videoIndex].videoMuted = true
                }
            }
        },
        rtcmOnRemoteHandToggled(data) {
            if(! (data && data.streamid && this.rtcmConnection.streamEvents[data.streamid])) {
                return
            }
            const videoIndex = this.videoList.findIndex(video => {
                return video.id === data.streamid
            })

            const stream = this.rtcmConnection.streamEvents[data.streamid].stream

            if (data.hasOwnProperty('isHandUp')) {
                if (data.isHandUp) {
                    this.videoList[videoIndex].isHandUp = true
                    this.$toasted.info($t('meeting.handup_notification', {attribute: this.videoList[videoIndex].name }), this.$toastConfig.info)
                } else {
                    this.videoList[videoIndex].isHandUp = false
                }
            }
        },
        rtcmOnUserIdAlreadyTaken(useridAlreadyTaken, yourNewUserId) {
            this.rtcmConnection.userid = yourNewUserId
        },

        // rtc action methods
        initMediaAndRtcmConnection() {
            if (!this.rtcmConnection) {
                this.rtcmConnection = new RTCMultiConnection()
                
                this.rtcmConnection.socketURL = window.atob(this.socketURL)
                if(this.configs.signal && this.configs.signal.url) {
                    this.rtcmConnection.socketURL = this.configs.signal.url
                }

                this.rtcmConnection.autoCreateMediaElement = false
                // this.rtcmConnection.autoCloseEntireSession = true // set this line to close room as soon as room creator leaves
                this.rtcmConnection.enableLogs = false

                // STAR_FIX_VIDEO_AUTO_PAUSE_ISSUES
                // via: https://github.com/muaz-khan/RTCMultiConnection/issues/778#issuecomment-524853468
                var bitrates = 512
                var resolutions = 'Ultra-HD'
                var videoConstraints = {}

                if (resolutions == 'HD') {
                    this.videoConstraints = {
                        width: {
                            ideal: 1280
                        },
                        height: {
                            ideal: 720
                        },
                        frameRate: 30
                    }
                }

                if (resolutions == 'Ultra-HD') {
                    this.videoConstraints = {
                        width: {
                            ideal: 1920
                        },
                        height: {
                            ideal: 1080
                        },
                        frameRate: 30
                    }
                }
                
                var CodecsHandler = this.rtcmConnection.CodecsHandler

                this.rtcmConnection.processSdp = function(sdp) {
                    var codecs = 'vp8'

                    if (codecs.length) {
                        sdp = CodecsHandler.preferCodec(sdp, codecs.toLowerCase())
                    }

                    if (resolutions == 'HD') {
                        sdp = CodecsHandler.setApplicationSpecificBandwidth(sdp, {
                            audio: 128,
                            video: bitrates,
                            screen: bitrates
                        })

                        sdp = CodecsHandler.setVideoBitrates(sdp, {
                            min: bitrates * 8 * 1024,
                            max: bitrates * 8 * 1024,
                        })
                    }

                    if (resolutions == 'Ultra-HD') {
                        sdp = CodecsHandler.setApplicationSpecificBandwidth(sdp, {
                            audio: 128,
                            video: bitrates,
                            screen: bitrates
                        })

                        sdp = CodecsHandler.setVideoBitrates(sdp, {
                            min: bitrates * 8 * 1024,
                            max: bitrates * 8 * 1024,
                        })
                    }

                    return sdp
                }
                // END_FIX_VIDEO_AUTO_PAUSE_ISSUES

                // this.rtcmConnection.candidates = {
                //     turn: true,
                //     stun: false,
                //     host: false
                // }

                this.rtcmConnection.onstream = this.rtcmOnStream
                this.rtcmConnection.onstreamended = this.rtcmOnStreamEnded
                this.rtcmConnection.onmute = this.rtcmOnMute
                this.rtcmConnection.onunmute = this.rtcmOnUnmute
                this.rtcmConnection.onMediaError = this.rtcmOnMediaError
                this.rtcmConnection.onUserIdAlreadyTaken = this.rtcmOnUserIdAlreadyTaken

                this.rtcmConnection.setCustomSocketEvent('remoteMutedUnmuted')
                this.rtcmConnection.setCustomSocketEvent('remoteHandToggled')
            }
            
            this.rtcmConnection.iceServers = [
                {
                    urls: [
                        "stun.l.google.com:19302",
                        "stun1.l.google.com:19302",
                        "stun2.l.google.com:19302",
                        "stun3.l.google.com:19302",
                        "stun4.l.google.com:19302"
                    ]
                }
            ]

            if(this.configs.iceServers.length) {
                this.rtcmConnection.iceServers = [ ...this.configs.iceServers ]
            }

            this.rtcmConnection.session = {
                ...this.meetingRulesHost.session
            }

            this.rtcmConnection.sdpConstraints.mandatory = {
                ...this.meetingRulesHost.mandatory
            }

            this.rtcmConnection.mediaConstraints = {
                video: this.meetingRulesHost.mediaConstraints.video ? this.videoConstraints : false,
                audio: this.meetingRulesHost.mediaConstraints.audio,
                screen: this.meetingRulesHost.mediaConstraints.screen,
            }

        },
        closeConnectionAndStream() {
            if (this.rtcmConnection) {
                if (this.localVideo) {
                    this.rtcmConnection.removeStream(this.localVideo.id)
                    window.meetingChannel.whisper('RemovedStream', this.localVideo)
                }

                this.rtcmConnection.attachStreams.forEach((localStream) => {
                    localStream.stop()
                })

                this.rtcmConnection.getAllParticipants()
                    .forEach((pid) => {
                        this.rtcmConnection.disconnectWith(pid)
                    })

                this.rtcmConnection.leave()
                this.rtcmConnection.closeSocket()
                this.localVideo = null
                this.localScreenStreamid = null
                this.rtcmConnection.recorder = null
                this.rtcmConnection = null
            }
            this.videoList = []
        },
        recheckLiveParticipants(stream) {
            let newList = []
            let membersWhoLeft = []
            const liveParticipants = this.rtcmConnection ? this.rtcmConnection.getAllParticipants() : []

            this.videoList.forEach((item, index) => {
                const userIndex = liveParticipants.findIndex(m => m === item.streamUserId)
                if ((!stream || (stream && item.id !== stream.streamid)) && (item.local || (!item.local && userIndex !== -1))) {
                    newList.push(item)
                } else {
                    item.status = false
                    newList.push(item)
                }
            })

            this.videoList = newList

            setTimeout(() => {
                this.videoList = this.videoList.filter(v => v.status)
                this.autoSetVideoMaximized()
            }, 3000)
        },
        autoSetVideoMaximized() {
            if (this.videoList.length > 1) {
                const maximizedRemoteVideoIndex = this.videoList.findIndex(v => !v.local && v.maximized && v.status)
                if (maximizedRemoteVideoIndex === -1) {
                    const remoteVideoIndex = this.videoList.findIndex(v => !v.local)
                    if (remoteVideoIndex !== -1) {
                        this.videoList = this.videoList.map((v, index) => {
                            v.maximized = false
                            if (index === remoteVideoIndex) {
                                v.maximized = true
                            }
                            return v
                        })
                    }
                }
            } else if (this.videoList.length) {
                this.videoList[0].maximized = true
            }
        },
        openRoom(meetingRoomId) {
            this.rtcmConnection.session = {
                ...this.meetingRulesHost.session
            }

            this.rtcmConnection.mediaConstraints = {
                ...this.meetingRulesHost.mediaConstraints
            }

            this.rtcmConnection.sdpConstraints.mandatory = {
                ...this.meetingRulesHost.mandatory
            }

            this.rtcmConnection.open(meetingRoomId, (isRoomOpened, roomid, error) => {
                this.rtcmConnection.socket.on('remoteMutedUnmuted', this.rtcmOnRemoteMuteUnmute)
                this.rtcmConnection.socket.on('remoteHandToggled', this.rtcmOnRemoteHandToggled)
                this.logEvent('Room Opened: ', roomid)
                this.isLoading = false

                if (error) {
                    formUtil.handleErrors(error)
                } else if (isRoomOpened === true) {

                    if (window.getOnlineTimer) {
                        clearTimeout(window.getOnlineTimer)
                    }

                    this.updatePageConfigs(true)

                    window.meetingChannel.whisper('MeetingRoomCreated', {
                        roomId: meetingRoomId
                    })
                    this.$toasted.success($t('meeting.meeting_created'), this.$toastConfig)
                }
            })
        },
        joinRoom(meetingRoomId) {
            this.rtcmConnection.session = {
                ...this.meetingRulesGuest.session
            }

            this.rtcmConnection.mediaConstraints = {
                ...this.meetingRulesGuest.mediaConstraints
            }

            this.rtcmConnection.sdpConstraints.mandatory = {
                ...this.meetingRulesGuest.mandatory
            }

            this.rtcmConnection.join(meetingRoomId, (isJoined, roomid, error) => {
                this.rtcmConnection.socket.on('remoteMutedUnmuted', this.rtcmOnRemoteMuteUnmute)
                this.rtcmConnection.socket.on('remoteHandToggled', this.rtcmOnRemoteHandToggled)
                this.logEvent('Room Joined: ', roomid)
                this.isLoading = false

                if (isJoined === false || error) {
                    formUtil.handleErrors(error)
                } else {
                    if (window.getOnlineTimer) {
                        clearTimeout(window.getOnlineTimer)
                    }

                    this.updatePageConfigs()

                    this.$toasted.success($t('meeting.meeting_joined'), this.$toastConfig)
                }
            })
        },
        shareScreen() {
            this.isLoading = true

            this.rtcmConnection.addStream({
                screen: true,
            })
        },
        stopSharingScreen() {
            this.isLoading = true

            let found = this.$refs.videos.find(video => {
                return video.id === this.localScreenStreamid
            })

            if (found && found.srcObject) {
                let tracks = found.srcObject.getTracks()
                tracks.forEach(track => {
                    track.removeEventListener('ended', this.stopSharingScreen)
                    track.enabled = false
                    track.stop()
                })
                this.rtcmConnection.removeStream(this.localScreenStreamid)
                window.meetingChannel.whisper('RemovedStream', found.srcObject)

                // this.videoList = this.videoList.filter(video => video.id !== this.localScreenStreamid)
                this.localScreenStreamid = null
                this.isLoading = false
            } else {
                this.localScreenStreamid = null
                this.isLoading = false
            }
        },
        getOnline() {
            this.isLoading = true
            this.pageConfigs.showAgenda = false
            const meetingStatusEalier = this.entity.status

            this.$gaEvent('engagement', 'meeting_getOnline')

            this.startGetOnlineTimer()

            this.initMediaAndRtcmConnection()

            this.Custom({
                    url: `/${this.initUrl}/${this.uuid}/join`,
                    method: 'post',
                })
                .then(response => {
                    this.meetingRoomId = response.meeting.roomId
                    this.entity = response.meeting
                    this.updateMeetingTimezone()
                    this.rtcmConnection.extra = {
                        username: this.user.username,
                        name: this.user.name,
                        uuid: this.user.uuid,
                        image: this.profile.image,
                        audioMuted: !this.pageConfigs.enableAudio,
                        videoMuted: !this.pageConfigs.enableVideo,
                        isHandUp: this.pageConfigs.isHandUp,
                    }

                    this.rtcmConnection.checkPresence(this.meetingRoomId, (isRoomExist, roomid) => {
                        if (isRoomExist === true) {
                            this.joinRoom(this.meetingRoomId)
                        } else {
                            if (this.entity.canModerate) {
                                this.openRoom(this.meetingRoomId)
                            } else {
                                this.isLoading = false
                                if (window.getOnlineTimer) {
                                    clearTimeout(window.getOnlineTimer)
                                }
                                this.$toasted.error($t('meeting.room_not_found'), this.$toastConfig.error)
                            }
                        }
                    })
                })
                .catch(error => {
                    this.isLoading = false
                    clearTimeout(window.getOnlineTimer)
                    formUtil.handleErrors(error)
                })
        },
        kickRemoteUser(item, itemIndex) {
            formUtil.confirmAction()
                .then((result) => {
                    if (result.value) {
                        let found = this.$refs.videos.find(video => {
                            return video.id === item.id
                        })

                        if (found && found.srcObject) {
                            const stream = found.srcObject

                            this.rtcmConnection.removeStream(stream.streamid)
                            window.meetingChannel.whisper('BanAttendee', item)

                            this.Custom({
                                    url: `/${this.initUrl}/${this.uuid}/invitees/${item.extra.uuid}/block`,
                                    method: 'post',
                                })
                                .then(response => {
                                    this.$toasted.success(response.message, this.$toastConfig)
                                })
                                .catch(error => {
                                    formUtil.handleErrors(error)
                                })
                        } 
                    } else {
                        result.dismiss === Swal.DismissReason.cancel
                    }
                })
                
        },
        getOffline() {
            this.$gaEvent('engagement', 'meeting_getOffline')
            this.isLoading = true
            this.meetingAction('leave', null, {
                alert: 'confirm',
                callback: (e) => {
                    this.pageConfigs.showAgenda = true
                    this.closeConnectionAndStream()
                }
            })
        },

        // meeting action methods
        updateMeetingTimezone() {
            this.entity.startDateTime = MomentTz.momentDateTimeTz(this.entity.startDateTime, this.vars.serverDateTimeFormat)
            if(this.entity.plannedStartDateTime) {
                this.entity.plannedStartDateTime = MomentTz.momentDateTimeTz(this.entity.plannedStartDateTime, this.vars.serverDateTimeFormat)
            }
        },
        meetingAction(action, data = {}, opts = { alert: 'confirm' }) {
            const defaultOpts = { alert: 'confirm' }
            opts = Object.assign({}, defaultOpts, opts)

            const callApi = (dataToSend) => {
                this.isLoading = true
                data = dataToSend ? dataToSend : data

                this.Custom({
                        url: `/${this.initUrl}/${this.uuid}/${action}`,
                        method: 'post',
                        data
                    })
                    .then(response => {
                        this.entity = response.meeting
                        this.updateMeetingTimezone()
                        this.$toasted.success(response.message, this.$toastConfig)
                        if (opts.callback) {
                            opts.callback(response)
                        }
                        this.isLoading = false
                    })
                    .catch(error => {
                        this.isLoading = false
                        formUtil.handleErrors(error)
                    })
            }

            if (!action) {
                this.isLoading = false
                return
            }

            if (opts.alert === 'confirm' || opts.alert === true) {
                formUtil.confirmAction()
                    .then((result) => {
                        if (result.value) {
                            callApi()
                        } else {
                            this.isLoading = false
                            result.dismiss === Swal.DismissReason.cancel
                        }
                    })
            } else if (opts.alert === 'input') {
                swtAlert.fire({
                        title: opts.title,
                        input: 'text',
                        inputPlaceholder: opts.inputPlaceholder,
                        showCancelButton: true,
                        confirmButtonText: 'Proceed!',
                        cancelButtonText: 'Go Back!'
                    })
                    .then((result) => {
                        if (result.value) {
                            let toSend = {}
                            toSend[opts.fieldName] = result.value
                            callApi(toSend)
                        } else {
                            this.isLoading = false
                            result.dismiss === Swal.DismissReason.cancel
                        }
                    })
            } else {
                callApi()
            }
        },
        snooze(period) {
            this.meetingAction('snooze', { period })
        },
        leaveMeeting() {
            this.meetingAction('leave')
        },
        cancelMeeting() {
            this.meetingAction('cancel', null, { alert: 'input', title: $t('meeting.reason_for_cancellation'), inputPlaceholder: $t('meeting.reason_for_cancellation'), fieldName: 'cancellationReason' })
        },
        endMeeting() {
            this.isLoading = true
            this.meetingAction('end', null, {
                callback: (e) => {
                    this.pageConfigs.showAgenda = true
                    this.closeConnectionAndStream()
                    window.meetingChannel.whisper('MeetingEnded', { status: e.meeting.status })
                }
            })
        },

        // page methods
        updatePageConfigs(isInitiator = false) {
            let configOptions = {}

            if (this.entity.type.uuid === 'video_conference') {
                configOptions = {
                    enableAudio: true,
                    enableVideo: true,
                    showEnableAudioBtn: true,
                    showEnableVideoBtn: true,
                    showShareScreenBtn: true,
                    showRecordingBtn: false,
                    showHandBtn: true,
                }
            } else if (this.entity.type.uuid === 'audio_conference') {
                configOptions = {
                    enableAudio: true,
                    enableVideo: true,
                    showEnableAudioBtn: true,
                    showEnableVideoBtn: false,
                    showShareScreenBtn: true,
                    showRecordingBtn: false,
                    showHandBtn: true,
                }
            } else if (this.entity.type.uuid === 'webinar') {
                configOptions = isInitiator ? {
                    enableAudio: true,
                    enableVideo: true,
                    showEnableAudioBtn: true,
                    showEnableVideoBtn: true,
                    showShareScreenBtn: true,
                    showRecordingBtn: true,
                    showHandBtn: false,
                } : {
                    enableAudio: true,
                    enableVideo: true,
                    showEnableAudioBtn: false,
                    showEnableVideoBtn: false,
                    showShareScreenBtn: false,
                    showRecordingBtn: false,
                    showHandBtn: true,
                }
            } else if (this.entity.type.uuid === 'live_class') {
                configOptions = isInitiator ? {
                    enableAudio: true,
                    enableVideo: true,
                    showEnableAudioBtn: true,
                    showEnableVideoBtn: true,
                    showShareScreenBtn: true,
                    showRecordingBtn: true,
                    showHandBtn: false,
                } : {
                    enableAudio: true,
                    enableVideo: true,
                    showEnableAudioBtn: true,
                    showEnableVideoBtn: false,
                    showShareScreenBtn: false,
                    showRecordingBtn: false,
                    showHandBtn: true,
                }
            } else if (this.entity.type.uuid === 'podcast') {
                configOptions = isInitiator ? {
                    enableAudio: true,
                    enableVideo: false,
                    showEnableAudioBtn: true,
                    showEnableVideoBtn: false,
                    showShareScreenBtn: false,
                    showRecordingBtn: true,
                    showHandBtn: false,
                } : {
                    enableAudio: true,
                    enableVideo: false,
                    showEnableAudioBtn: false,
                    showEnableVideoBtn: false,
                    showShareScreenBtn: false,
                    showRecordingBtn: false,
                    showHandBtn: false,
                }
            }

            configOptions.showRecordingBtn = isInitiator || this.entity.canModerate ? true : false
            configOptions.showHandBtn = true

            configOptions.objForEach((value, key) => this.pageConfigs[key] = value)


            this.rtcmConnection.extra.audioMuted = !this.pageConfigs.enableAudio
            this.rtcmConnection.extra.videoMuted = !this.pageConfigs.enableVideo
            this.rtcmConnection.extra.isHandUp = this.pageConfigs.isHandUp
            this.rtcmConnection.updateExtraData()
        },
        updateMeetingRules() {
            this.pageConfigs.enableRecording = true
            if (this.entity.type.uuid === 'video_conference') {
                this.meetingRulesHost = {
                    session: {
                        audio: true,
                        video: true,
                        screen: false,
                        oneway: false,
                    },
                    mediaConstraints: {
                        audio: true,
                        video: this.videoConstraints,
                        screen: false,
                    },
                    mandatory: {
                        OfferToReceiveAudio: true,
                        OfferToReceiveVideo: true,
                    },
                }
                this.meetingRulesGuest = {
                    session: {
                        audio: true,
                        video: true,
                        screen: false,
                        oneway: false,
                    },
                    mediaConstraints: {
                        audio: true,
                        video: this.videoConstraints,
                        screen: false,
                    },
                    mandatory: {
                        OfferToReceiveAudio: true,
                        OfferToReceiveVideo: true,
                    },
                }
            } else if (this.entity.type.uuid === 'audio_conference') {
                this.meetingRulesHost = {
                    session: {
                        audio: true,
                        video: false,
                        screen: false,
                        oneway: false,
                    },
                    mediaConstraints: {
                        audio: true,
                        video: false,
                        screen: false,
                    },
                    mandatory: {
                        OfferToReceiveAudio: true,
                        OfferToReceiveVideo: true,
                    },
                }
                this.meetingRulesGuest = {
                    session: {
                        audio: true,
                        video: false,
                        screen: false,
                        oneway: false,
                    },
                    mediaConstraints: {
                        audio: true,
                        video: false,
                        screen: false,
                    },
                    mandatory: {
                        OfferToReceiveAudio: true,
                        OfferToReceiveVideo: true,
                    },
                }
            } else if (this.entity.type.uuid === 'webinar') {
                this.meetingRulesHost = {
                    session: {
                        audio: true,
                        video: true,
                        screen: false,
                        oneway: true,
                    },
                    mediaConstraints: {
                        audio: true,
                        video: this.videoConstraints,
                        screen: false,
                    },
                    mandatory: {
                        OfferToReceiveAudio: false,
                        OfferToReceiveVideo: false,
                    },
                }
                this.meetingRulesGuest = {
                    session: {
                        audio: true,
                        video: true,
                        screen: false,
                        oneway: true,
                    },
                    mediaConstraints: {
                        audio: true,
                        video: false,
                        screen: false,
                    },
                    mandatory: {
                        OfferToReceiveAudio: true,
                        OfferToReceiveVideo: true,
                    },
                }
            } else if (this.entity.type.uuid === 'live_class') {
                this.meetingRulesHost = {
                    session: {
                        audio: true,
                        video: true,
                        screen: false,
                        oneway: false,
                    },
                    mediaConstraints: {
                        audio: true,
                        video: this.videoConstraints,
                        screen: false,
                    },
                    mandatory: {
                        OfferToReceiveAudio: true,
                        OfferToReceiveVideo: false,
                    },
                }
                this.meetingRulesGuest = {
                    session: {
                        audio: true,
                        video: false,
                        screen: false,
                        oneway: false,
                    },
                    mediaConstraints: {
                        audio: true,
                        video: false,
                        screen: false,
                    },
                    mandatory: {
                        OfferToReceiveAudio: true,
                        OfferToReceiveVideo: true,
                    },
                }
            } else if (this.entity.type.uuid === 'podcast') {
                this.meetingRulesHost = {
                    session: {
                        audio: true,
                        video: false,
                        screen: false,
                        oneway: true,
                    },
                    mediaConstraints: {
                        audio: true,
                        video: false,
                        screen: false,
                    },
                    mandatory: {
                        OfferToReceiveAudio: false,
                        OfferToReceiveVideo: false,
                    },
                }
                this.meetingRulesGuest = {
                    session: {
                        audio: true,
                        video: false,
                        screen: false,
                        oneway: true,
                    },
                    mediaConstraints: {
                        audio: false,
                        video: false,
                        screen: false,
                    },
                    mandatory: {
                        OfferToReceiveAudio: true,
                        OfferToReceiveVideo: false,
                    },
                }
            }
        },
        getInitialData() {
            this.isLoading = true

            if(!window.Echo) {
                this.$toasted.error($t('config.pusher.credential_required'), this.$toastConfig.error)
                this.$router.push({ name: this.fallBackRoute })
                return
            }

            return this.Get({ uuid: this.uuid })
                .then(response => {
                    if(response.isInstantMeeting) {
                        this.pageConfigs.hasAgenda = false
                    }

                    this.entity = response
                    this.updateMeetingTimezone()
                    this.updateMeetingRules()

                    initMedia()

                    if (response.roomId && response.status === 'live') {
                        setTimeout(() => {
                            this.initMediaAndRtcmConnection()
                            this.rtcmConnection.checkPresence(response.roomId, (isRoomExist, roomid) => {
                                this.roomIdAlive = !!isRoomExist
                            })
                        }, 1000)
                    }

                    this.joinChannel()

                    if(response.isInstantMeeting && !response.isBlocked && response.status === 'live') {
                        this.getOnline()
                    } else {
                        this.isLoading = false
                    }

                    return response
                })
                .catch(error => {
                    this.isLoading = false
                    formUtil.handleErrors(error)
                    this.$router.push({ name: this.fallBackRoute })
                    return error
                })
        },
        logEvent(msg, args, type = 'log') {
            if (this.rtcmConnection.enableLogs) {
                if (type === 'log') {
                    console.log(msg, args)
                } else if (type === 'error') {
                    console.error(msg, args)
                } else if (type === 'debug') {
                    console.debug(msg, args)
                }
            }
        },
        destroyPage() {
            if (window.countdownInterval) {
                clearInterval(window.countdownInterval)
            }

            this.SetUiConfig({ pageHeaderShow: true, pageFooterShow: true })

            this.closeConnectionAndStream()

            if (window.Echo) {
                if (window.meetingChannel) {
                    window.meetingChannel.stopListening('MeetingStatusChanged')
                    window.meetingChannel.stopListening('NewMessage')
                }
                window.Echo.leave(`Meeting.${this.uuid}`)
            }
            
            if(screenfull.isEnabled) {
                screenfull.off('change')
                screenfull.off('error')
            }
        },
        doInit() {
            this.Init({ url: this.initUrl })

            if (window.isDuplicate()) {
                this.duplicateTab = true
            }

            this.getInitialData()
        },
    },
    mounted() {
        if (this.$route.params.uuid) {
            this.uuid = this.$route.params.uuid
        }

        if (screenfull.isEnabled) {
            screenfull.on('change', () => {
                this.SetUiConfig({ fullScreen: screenfull.isFullscreen })
            })
            screenfull.on('error', event => {
                console.error('Failed to enable fullscreen', event);
            })

            this.SetUiConfig({ fullScreen: false })
        }

        this.doInit()
        window.addEventListener('beforeunload', this.beforeUnload)
    },
    created() {
        this.SetUiConfig({ pageHeaderShow: false, pageFooterShow: false })

        // detect 2G and alert
        if (navigator.connection &&
            navigator.connection.type === 'cellular' &&
            navigator.connection.downlinkMax <= 0.115) {
            alert('2G is not supported. Please use a better internet service.')
        }
    },
    beforeDestroy() {
        if (!window.isMeetingPageDestroyed) {
            this.isLoading = true

            this.destroyPage()

            if (this.entity && this.entity.status === 'live') {
                this.Custom({
                    url: `/${this.initUrl}/${this.uuid}/leave`,
                    method: 'post',
                })
            }

            window.isMeetingPageDestroyed = true
        }
    },
    beforeRouteEnter(to, from, next) {
        if (!to.params.uuid) {
            next({ name: from === 'appMeetingView' ? from : 'AppMeetingList' })
        } else {
            next(vm => {
                vm.prevRoute = from
            })
        }
    },
    beforeRouteLeave(to, from, next) {
        this.isLoading = true

        this.destroyPage()
        window.removeEventListener('beforeunload', this.beforeUnload)

        if (this.entity && this.entity.status === 'live') {
            this.Custom({
                    url: `/${this.initUrl}/${this.uuid}/leave`,
                    method: 'post',
                })
                .then(response => {
                    window.isMeetingPageDestroyed = true
                    next()
                })
                .catch(error => {
                    window.isMeetingPageDestroyed = true
                    next()
                })
        } else {
            window.isMeetingPageDestroyed = true
            next()
        }
    },
    destroyed() {
    }
}
